const https = require("https")
const fs = require("fs")

require("dotenv").config()

const options = {
  hostname: process.env.API_HOST,
  path: "/v2/redirects",
  method: "GET",
  headers: {
    "API-Token": process.env.API_TOKEN,
  },
}

https.get(options, (res) => {
  let body = ""

  res.on("data", function (d) {
    body += d
  })

  res.on("end", function () {
    const parsed = JSON.parse(body)
    if (
      !parsed.hasOwnProperty("data") &&
      parsed.data.hasOwnProperty("redirects_301")
    ) {
      return
    }

    const redirectData = parsed.data["redirects_301"]
    const redirects = JSON.stringify(
      redirectData.map((r) => {
        switch (r.type) {
          case "collections":
            return {
              _from: `/collection/${r["from"]}`,
              to: `/collection/${r["to"]}`,
            }
          case "entities":
            return { _from: `/entity/${r["from"]}`, to: `/entity/${r["to"]}` }
          default:
            return { _from: r["from"], to: r["to"] }
        }
      })
    )

    fs.writeFileSync(process.env.REDIRECT_JSON_FILE_PATH, redirects)
  })
})
