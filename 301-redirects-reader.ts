const fs = require("fs")

const getRedirects = () => {
  return JSON.parse(fs.readFileSync(process.env["REDIRECT_JSON_FILE_PATH"]))
}

export default getRedirects
