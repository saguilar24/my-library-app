export const environment = {
  production: true,
  appUrl: "https://library.adaptavist.com",
  apiUrl: "https://library-api.adaptavist.com",
}
