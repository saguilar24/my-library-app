export const environment = {
  production: false,
  appUrl: "http://localhost:4200",
  apiUrl: "http://localhost",
  indexName: "library-content",
  applicationID: "GR3XPFYRU9",
  searchOnlyApiKey: "857402c13e7d6824d1c9f29180e07ca5",
  graphcmsEndpoint:
    "https://api-eu-central-1.graphcms.com/v2/ckqw85jopz6wk01xi5o6mecm5/master",
  graphcmsToken:
    "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImdjbXMtbWFpbi1wcm9kdWN0aW9uIn0.eyJ2ZXJzaW9uIjozLCJpYXQiOjE2MjkyNzI5OTks" +
    "ImF1ZCI6WyJodHRwczovL2FwaS1ldS1jZW50cmFsLTEuZ3JhcGhjbXMuY29tL3YyL2NrcXc4NWpvcHo2d2swMXhpNW82bWVjbTUvbWFzdGVyIiwiaHR0cHM6Ly9tYW5" +
    "hZ2VtZW50LW5leHQuZ3JhcGhjbXMuY29tIl0sImlzcyI6Imh0dHBzOi8vbWFuYWdlbWVudC5ncmFwaGNtcy5jb20vIiwic3ViIjoiNjhmYzczMzQtNDhmMS00MjliLW" +
    "E4YjEtZDllYmQ5NTE3NjM1IiwianRpIjoiY2tzaDcyc2Z1MHhuczAxeGxibzFnNmNuZSJ9.IhXgfcrB5rxhih-6VgEyFbp8UvnTMaDfSFc9cYW7AnO2UZAQs9A3QJV_" +
    "kEfv8vsjXqXWiyPJNV_Cni3CA84oXIQtfS6U4mteNzEbtDZjkpk_wEcK0D2cCT_2WHpzCBdy_pgJ66ho1Sc5FWRBfOcPVpmsBMfmqzszrO205esvfBD560cwX_eawGX" +
    "IZrzmY7x0fnxsSVZe7aBVzEAbXvbiiFsazG69dBF2xFR-MHC3TS6mZCWBYOhyX0lzFwc__1vsJ9ofcBF5AMBlQljZuAK-0X0xNZq5pY4KusEzeEIhxfvZnGjQObckSv" +
    "6-xxsjmcI6nxVOOx4N2LXd9ZB8VI7Mgs8zJwKS3HcsC1XJhrb7KI9UPa9mkrBibWaNDz07i1sohHy9xoCIBa92rqgoixl7GaH8GA-42Kp1HVqXJHCP5zLIJ9G357Lg3" +
    "f6gwcZ_5KEeZabag56XaUlBUNTxtkxXNmP-X-wLP7TxG3KjBvXPqnPbulsLjLc8RBdzV7bs5i2R8g0kvcdlBdutnSa4ahRfL6VQSh9dJ0w1Tkicyu1Nnq7KZ8wimw1t" +
    "--tKhzZZtOSruoqY9mMpfzS95nIRtm1nESLwVn_p2TgRFVq-aLY0p9m7H68X-0pMyw-r2JeKWv-RmGAuOSCj5Z69iQ-XT8zSIGRo8y5YTBUgzBJaapQ_R_8",
}
