export const environment = {
  production: false,
  appUrl: "https://librarytest.adaptavist.com",
  apiUrl: "https://library-apistage.adaptavist.com",
}
