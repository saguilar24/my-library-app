import {Component, Inject, OnInit, Optional} from "@angular/core"
import { ApplicationService } from "./services/application/application.service"
import { ProfileService } from "./services/profile/profile.service"
import {makeStateKey, Meta, TransferState} from "@angular/platform-browser"
import { environment } from "../environments/environment"
import algoliasearch from "algoliasearch"
import { history } from 'instantsearch.js/es/lib/routers';
import {createSSRSearchClient} from "angular-instantsearch";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ssrRouter} from "./ssrRouter";
import {REQUEST} from "@nguniversal/express-engine/tokens";
import { simple } from 'instantsearch.js/es/lib/stateMappings'


@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  config: any;

  constructor(
    public profile: ProfileService,
    public appService: ApplicationService,
    private meta: Meta,
    private httpClient: HttpClient,
    private transferState: TransferState,
    @Optional() @Inject(REQUEST) protected request: Request
  ) {
    this.config = {
      indexName: environment.indexName,
      searchClient: createSSRSearchClient({
        appId: environment.applicationID,
        apiKey: environment.searchOnlyApiKey,
        makeStateKey,
        HttpHeaders,
        transferState: this.transferState,
        httpClient: this.httpClient,
      }),
      routing: {
        /*router: history({
          getLocation() {
            if (typeof window === 'undefined') {
              const url = environment.appUrl; // retrieved from the server context
              return new URL(url) as unknown as Location;
            }

            return window.location;
          },
        })*/
        router: ssrRouter(() => {
          if (this.request) {
            // request is only defined on the server side
            return this.request.url;
          }
          return window.location.pathname + window.location.search;
        }),
        stateMapping: simple(),

      }
    }

  }

  ngOnInit(): void {
    // Only primeSegmentAnalytics within client browser
    if (this.appService.isRunningInClientBrowser()) {
      this.appService.primeSegmentAnalytics()
    }
    if (environment.production) {
      this.meta.addTag({
        "http-Equiv": "Content-Security-Policy",
        content:
          "default-src 'none'; script-src 'self' 'unsafe-eval' 'unsafe-inline' unpkg.com cdn.segment.com static.hotjar.com script.hotjar.com; style-src 'self' 'unsafe-inline' cdnjs.cloudflare.com fonts.googleapis.com fast.fonts.net; font-src 'self' fonts.gstatic.com fast.fonts.net; img-src 'self' library-api.adaptavist.com; connect-src 'self' library-api.adaptavist.com adaptavist-id.eu.auth0.com cdn.segment.com api.segment.io in.hotjar.com; frame-src vars.hotjar.com adaptavist-id.eu.auth0.com;",
      })
    }
  }

  getYear(): number {
    return new Date().getFullYear()
  }
}
