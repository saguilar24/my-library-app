import { NgModule } from "@angular/core"
import { AppShellNoRenderDirective } from "./directives/app-shell-no-render.directive"

@NgModule({
  imports: [],
  declarations: [AppShellNoRenderDirective],
  exports: [AppShellNoRenderDirective],
})
export class NoRenderModule {}
