import { EntityModel } from "../models/entity.model"
import {
  makeAlgoliaLinkData,
  makeGraphCmsLinkData,
} from "./category-transformers"
import { CardModel } from "../models/card.model"
import { RankingInfo } from "@algolia/client-search"

// This method is not necessary in the new infrastructure because the values are obtained from GraphCMS and not from the API.

/*export const entityToCardTransformer = (e: EntityModel): CardModel => {
  return {
    name: e.name,
    id: e.id,
    description: e.description,
    slug: e.slug,
    categories: categoryTransformer(e),
    partners: e.partners,
    app_versions: e.app_versions,
    updated_at: e.updated_at,
    created_at: e.created_at,
    type: e.type_name,
  }
}

// This method is not necessary in the new infrastructure because the values are obtained from GraphCMS and not from the API.
export const elasticSearchEntityToCardTransformer = (e): CardModel => ({
  name: e.name,
  id: e.id,
  description: e.description,
  slug: e.slug,
  categories: categoryTransformer(e),
  partners: e.partners,
  app_versions: e.app_versions,
  updated_at: e.updated_at,
  created_at: e.created_at,
  type: e.type,
})

// This method is not necessary in the new infrastructure because the values are obtained from GraphCMS and not from the API.
export const singleEntityToCardTransformer = (e): CardModel => ({
  name: e.name,
  id: e.id,
  description: e.description,
  slug: e.slug,
  categories: categoryTransformer(e),
  partners: e.partners,
  app_versions: e.app_versions,
  updated_at: e.updated_at,
  created_at: e.created_at,
  type: e.type.name.toLowerCase(),
})*/

export const entityToCardTransformerFromGraphCms = (e: { name: any; scriptId: any; appVs: any[]; createdDate: any; type: any }): CardModel => ({
  name: e.name,
  slug: e.scriptId,
  categories: makeGraphCmsLinkData(e.appVs),
  created_at: e.createdDate,
  type: e.type,
})


export const entityToCardTransformerFromAlgolia = (e: { objectID?: string; _highlightResult?: {} | undefined; _snippetResult?: {} | undefined; _rankingInfo?: RankingInfo | undefined; _distinctSeqID?: number | undefined; script_name?: any; script_id?: any; created_at?: any; type?: any; app_pretty_names?: any[]; app_names?: { [x: string]: string } }): CardModel => ({
  name: e.script_name,
  scriptId: e.script_id,
  categories: makeAlgoliaLinkData(e),
  created_at: e.created_at,
  type: e.type,
})
