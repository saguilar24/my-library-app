import {
  getAppVersion,
  getPartners,
  makeGraphCmsLinkData,
} from "./category-transformers"
import { CardCollectionModel } from "../models/card-collection.model"
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime"
import {LinkModel} from "../models/link.model";

dayjs.extend(relativeTime)

// This method is not necessary in the new infrastructure because the values are obtained from GraphCMS and not from the API.

/*export const collectionToCardCollectionTransformer = (
  collection: CollectionModel,
  withCategories = true
): CardCollectionModel => ({
  name: collection.name,
  id: collection.id,
  description: collection.description,
  meta_description: collection.meta_description,
  slug: collection.slug,
  categories: withCategories ? categoryTransformer(collection) : null,
  partners: collection.partners,
  app_versions: collection.app_versions,
  created_at: collection.created_at,
  image: {
    src: collection.image, // Todo: Create fallback default image
    alt: collection.name, // Todo: return and pick up alt tag
  },
  creator: "Adaptavist",
  type: "collection", // Todo: pull this through dynamically
})*/

export const collectionFromGraphCmsToCardCollectionTransformer = (
  collection: { name: any; id: any; slug: any; scripts: string | any[]; originalCreationDate: any; icon: { url: any } }
): { image: { src: any; alt: any }; creator: string; name: any; created_at: any; id: any; categories: LinkModel[] | null; type: string; slug: any } => ({
  name: collection.name,
  id: collection.id,
  slug: collection.slug,
  categories:
    collection.scripts.length !== 0
      ? makeGraphCmsLinkData(collection.scripts[0].appVs)
      : null,
  created_at: dayjs(collection.originalCreationDate).fromNow(),
  image: {
    src: collection.icon.url,
    alt: collection.icon.url,
  },
  creator: "Adaptavist",
  type: "collection",
})
