import { CollectionModel } from "../models/collection.model"
import { LinkModel } from "../models/link.model"
import { EntityModel } from "../models/entity.model"
import { slugify } from "../utilities/slugify"
import { PartnerModel } from "../models/partner.model"
import { AppVersionsModel } from "../models/app-versions.model"
import {RankingInfo} from "@algolia/client-search";

/*export const categoryTransformer = (
  model: EntityModel | CollectionModel
): LinkModel[] => {

  return ["app_versions"].reduce(
    (arr, category) => [
      ...arr,
      ...model[category].map((ec) => makeLinkData(category, ec)),
    ],
    []
  )
}

const makeLinkData = (category, entityCategory) => {
  return {
    link: "/search",
    text: entityCategory.pretty_name
      ? entityCategory.pretty_name.charAt(0).toUpperCase() +
        entityCategory.pretty_name.slice(1)
      : entityCategory.name.charAt(0).toUpperCase() +
        entityCategory.name.slice(1),
    params: { [category]: slugify(entityCategory.name), page: 1 },
  }
}*/

export const makeGraphCmsLinkData = (apps: any[]): LinkModel[] => {
  const links: LinkModel[] = []
  apps.forEach((data) => {
    links.push({
      link: "/search",
      text: data.app.prettyName
        ? data.app.prettyName.charAt(0).toUpperCase() +
          data.app.prettyName.slice(1)
        : data.app.name.charAt(0).toUpperCase() + data.app.name.slice(1),
      params: { ["app_versions"]: slugify(data.app.name), page: 1 },
    })
  })
  return links
}

export const makeAlgoliaLinkData = (script: { objectID?: string; _highlightResult?: {} | undefined; _snippetResult?: {} | undefined; _rankingInfo?: RankingInfo | undefined; _distinctSeqID?: number | undefined; script_name?: any; script_id?: any; created_at?: any; type?: any; app_pretty_names?: any[]; app_names?: { [p: string]: string } }): LinkModel[] => {
  const links: LinkModel[] = []
  // @ts-ignore
  if (script.app_pretty_names.length > 0) {
    // @ts-ignore
    script.app_pretty_names.forEach((prettyName, index) => {
      links.push({
        link: "/search",
        text: prettyName ? prettyName : script.app_names[index],
        params: { ["app_versions"]: slugify(script.app_names[index]), page: 1 },
      })
    })
  }
  return links
}

export const getPartners = (partners: any[]): PartnerModel[] => {
  const p: PartnerModel[] = []

  partners.forEach((data) => {
    p.push({
      icon: data.partnerLogo.url,
      name: data.partnerName,
      product_name: data.partnerProductName,
      product_url: data.partnerProductURL,
      url: data.partnerURL,
    })
  })
  return p
}

export const getAppVersion = (appVersions: any[]): AppVersionsModel[] => {
  const v: AppVersionsModel[] = []

  appVersions.forEach((data) => {
    v.push({
      company_name: data.app.companyName,
      file: undefined,
      icon: data.app.icon.url,
      id: data.app.id,
      name: data.app.name,
      pretty_name: data.app.prettyName,
      product_url: data.app.productURL,
      removeFile: false,
      shortcode: data.app.shortcode,
      url: data.app.companyURL,
      version_from: 0,
      version_to: 0,
    })
  })
  return v
}
