import { inject, TestBed } from "@angular/core/testing"
import { RouterTestingModule } from "@angular/router/testing"

import { AuthService } from "../services/auth/auth.service"
import { AuthAndVerifiedGuard } from "./auth-and-verified.guard"

describe("AuthAndVerifiedGuard", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [AuthService, AuthAndVerifiedGuard],
    })
  })

  it("should ...", inject(
    [AuthAndVerifiedGuard],
    (guard: AuthAndVerifiedGuard) => {
      expect(guard).toBeTruthy()
    }
  ))
})
