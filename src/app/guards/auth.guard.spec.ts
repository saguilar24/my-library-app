import { inject, TestBed } from "@angular/core/testing"
import { RouterTestingModule } from "@angular/router/testing"

import { AuthService } from "../services/auth/auth.service"
import { AuthGuard } from "./auth.guard"

describe("AuthGuard", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [AuthService, AuthGuard],
    })
  })

  it("should ...", inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy()
  }))
})
