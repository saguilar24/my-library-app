import { Injectable } from "@angular/core"
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
} from "@angular/router"
import { Observable } from "rxjs"
import { AuthService } from "../services/auth/auth.service"
import { ProfileService } from "../services/profile/profile.service"

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private profile: ProfileService, private auth: AuthService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.profile.isAuthenticatedAndVerified() && this.profile.isAdmin()) {
      return true
    } else {
      this.auth.login()
      return false
    }
  }
}
