import { Injectable } from "@angular/core"
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
} from "@angular/router"
import { Observable } from "rxjs"
import { AuthService } from "../services/auth/auth.service"
import { ProfileService } from "../services/profile/profile.service"

@Injectable()
export class AuthAndVerifiedGuard implements CanActivate {
  constructor(private auth: AuthService, private profile: ProfileService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.profile.isAuthenticatedAndVerified()) {
      return true
    } else {
      this.auth.login()
      return false
    }
  }
}
