import { inject, TestBed } from "@angular/core/testing"
import { RouterTestingModule } from "@angular/router/testing"

import { AuthService } from "../services/auth/auth.service"
import { AdminGuard } from "./admin.guard"

describe("AdminGuard", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [AuthService, AdminGuard],
    })
  })

  it("should ...", inject([AdminGuard], (guard: AdminGuard) => {
    expect(guard).toBeTruthy()
  }))
})
