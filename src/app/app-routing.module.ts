import { NgModule } from "@angular/core"
import { RouterModule, Routes } from "@angular/router"
import { HomePageComponent } from "./components/pages/home-page/home-page.component"
import { HelpPageComponent } from "./components/pages/help-page/help-page.component"
import { TermsPageComponent } from "./components/pages/terms-page/terms-page.component"
import { CollectionPageComponent } from "./components/pages/collection-page/collection-page.component"
import { CollectionsPageComponent } from "./components/pages/collections-page/collections-page.component"
import { ManageAccountPageComponent } from "./components/pages/manage-account-page/manage-account-page.component"
import { ProfilePageComponent } from "./components/pages/profile-page/profile-page.component"
import { ProfileCodePageComponent } from "./components/pages/profile-code-page/profile-code-page.component"
import { EntityPageComponent } from "./components/pages/entity-page/entity-page.component"
import { AuthGuard } from "./guards/auth.guard"
import { AuthAndVerifiedGuard } from "./guards/auth-and-verified.guard"
import { CallbackPageComponent } from "./components/pages/callback-page/callback-page.component"
import { ErrorUnauthorisedPageComponent } from "./components/pages/error-unauthorised-page/error-unauthorised-page.component"
import { ErrorPageComponent } from "./components/pages/error-page/error-page.component"
import { ErrorNotFoundPageComponent } from "./components/pages/error-not-found-page/error-not-found-page.component"
import { SavedSearchPageComponent } from "./components/pages/saved-search-page/saved-search-page.component"
import { WatchedEntitiesPageComponent } from "./components/pages/watched-entities-page/watched-entities-page.component"
import { SearchPageComponent } from "./components/pages/search-page/search-page.component"

const routes: Routes = [
  { path: "", component: HomePageComponent },
  { path: "search", component: SearchPageComponent },
  { path: "help", component: HelpPageComponent },
  { path: "terms-and-conditions", component: TermsPageComponent },
  {
    path: "manage-account",
    canActivate: [AuthGuard],
    component: ManageAccountPageComponent,
  },
  {
    path: "saved-search",
    canActivate: [AuthGuard],
    component: SavedSearchPageComponent,
  },
  {
    path: "watched-entities",
    canActivate: [AuthGuard],
    component: WatchedEntitiesPageComponent,
  },
  {
    path: "profile/:uuid",
    canActivate: [AuthGuard],
    component: ProfilePageComponent,
  },
  {
    path: "profile",
    canActivate: [AuthGuard],
    component: ProfilePageComponent,
  },
  {
    path: "code",
    canActivate: [AuthGuard],
    component: ProfileCodePageComponent,
  },
  {
    path: "code/:uuid",
    canActivate: [AuthGuard],
    component: ProfileCodePageComponent,
  },
  { path: "entity/:slug", component: EntityPageComponent },
  { path: "collection/:slug", component: CollectionPageComponent },
  { path: "collections/newest", component: CollectionsPageComponent },
  { path: "collections/trending", component: CollectionsPageComponent },
  { path: "callback", component: CallbackPageComponent },
  { path: "error-401", component: ErrorUnauthorisedPageComponent },
  { path: "error-500", component: ErrorPageComponent },
  { path: "**", component: ErrorNotFoundPageComponent },
]

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,

      {
        onSameUrlNavigation: "reload",
        enableTracing: false,
        relativeLinkResolution: "legacy",
      }
    ),
  ],
  exports: [RouterModule],
  providers: [AuthGuard, AuthAndVerifiedGuard],
})
export class AppRoutingModule {}
