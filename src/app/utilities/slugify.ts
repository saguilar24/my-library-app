export function slugify(name: string) {
  return name.replace(/\s+/g, "-").toLowerCase()
}
