export default function arrayToPrettyList(
  arr,
  separator = ", ",
  finalSeparator = null
) {
  let result = ""
  for (let i = 0; i < arr.length; i++) {
    result += arr[i]

    if (i < arr.length - 2) {
      result += separator
    } else if (i === arr.length - 2) {
      result += finalSeparator ? finalSeparator : separator
    }
  }
  return result
}
