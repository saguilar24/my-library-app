import { CardModel } from "../models/card.model"
import { ScriptModel } from "../models/script.model"

const makeDataModel = (data: CardModel | ScriptModel, isReady = false) => ({ data, isReady })

export default makeDataModel
