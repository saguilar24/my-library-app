export default function displayCommaOrAmpersand(i, length) {
  return i >= length - 1 ? "" : length === 2 || i === length - 2 ? " & " : ", "
}
