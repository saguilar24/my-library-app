import { Injectable } from "@angular/core"
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from "@angular/common/http"
import { Observable } from "rxjs/internal/Observable"

import { AuthService } from "./services/auth/auth.service"
import { ProfileService } from "./services/profile/profile.service"
import { ApplicationService } from "./services/application/application.service"

@Injectable()
export class HTTPHeadersInterceptor implements HttpInterceptor {
  constructor(
    private auth: AuthService,
    private profile: ProfileService,
    private appService: ApplicationService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const apiToken = "7d8a48df-011b-4164-aa40-3de0312c0fde"

    // Only attempt to add an `Authorization` header if running within the Client's Browser and the User is authed
    if (
      this.appService.isRunningInClientBrowser() &&
      this.profile.isAuthenticated()
    ) {
      const accessToken = this.profile.accessToken

      if (accessToken) {
        const authedRequestClone = req.clone({
          headers: req.headers
            .set("Authorization", "Bearer " + accessToken)
            .set("API-Token", apiToken),
        })

        return next.handle(authedRequestClone)
      }
    }

    // Always append an `API-Token` header to unlock access to the API endpoints
    const unauthedRequestClone = req.clone({
      headers: req.headers.set("API-Token", apiToken),
    })

    return next.handle(unauthedRequestClone)
  }
}
