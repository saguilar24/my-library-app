import {APP_ID, APP_INITIALIZER, Inject, NgModule, PLATFORM_ID} from '@angular/core';
import {BrowserModule, BrowserTransferStateModule} from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RendererModule, TransferHttpCacheModule } from '@nguniversal/common/clover';
import {ErrorNotFoundPageComponent} from "./components/pages/error-not-found-page/error-not-found-page.component";
import {CallbackPageComponent} from "./components/pages/callback-page/callback-page.component";
import {VerifyAccountMessageBarComponent} from "./components/partials/verify-account-message-bar/verify-account-message-bar.component";
import {ErrorUnauthorisedPageComponent} from "./components/pages/error-unauthorised-page/error-unauthorised-page.component";
import {TermsPageComponent} from "./components/pages/terms-page/terms-page.component";
import {HelpPageComponent} from "./components/pages/help-page/help-page.component";
import {ErrorPageComponent} from "./components/pages/error-page/error-page.component";
import {AppShellRenderDirective} from "./directives/app-shell-render.directive";
import {HeaderBarPartialComponent} from "./components/partials/header-bar-partial/header-bar-partial.component";
import {FeedbackButtonComponent} from "./components/partials/feedback-button/feedback-button.component";
import {UniversalHeaderComponent} from "./components/partials/universal-header/universal-header.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AdminRoutingModule} from "./components/admin/admin-routing.module";
import {LibraryRoutingModule} from "./components/pages/library/library-routing.module";
import {HomePageModule} from "./components/pages/home-page/home-page.module";
import {EntityPageModule} from "./components/pages/entity-page/entity-page.module";
import {NavigationModule} from "./components/lib/navigation/navigation.module";
import {CollectionPageModule} from "./components/pages/collection-page/collection-page.module";
import {CollectionsPageModule} from "./components/pages/collections-page/collections-page.module";
import {LibraryModule} from "./components/pages/library/library.module";
import {ManageAccountPageModule} from "./components/pages/manage-account-page/manage-account-page.module";
import {AdminModule} from "./components/admin/admin.module";
import {TypographyModule} from "./components/lib/typography/typography.module";
import {ProfilePageModule} from "./components/pages/profile-page/profile-page.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SearchPageModule} from "./components/pages/search-page/search-page.module";
import {NoRenderModule} from "./norender.module";
import {APP_BASE_HREF, isPlatformBrowser} from "@angular/common";
import {ApplicationService} from "./services/application/application.service";
import {AuthService} from "./services/auth/auth.service";
import {HTTPHeadersInterceptor} from "./http-headers.interceptor";
import {NgAisInstantSearchModule, NgAisModule} from "angular-instantsearch";
import {AppRoutingModule} from "./app-routing.module";

@NgModule({
  declarations: [
    AppComponent,
    ErrorUnauthorisedPageComponent,
    ErrorNotFoundPageComponent,
    ErrorPageComponent,
    HelpPageComponent,
    TermsPageComponent,
    CallbackPageComponent,
    AppShellRenderDirective,
    HeaderBarPartialComponent,
    VerifyAccountMessageBarComponent,
    FeedbackButtonComponent,
    UniversalHeaderComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'my-library-app'}),
    BrowserAnimationsModule,
    HttpClientModule,
    LibraryRoutingModule,
    AdminRoutingModule,
    AppRoutingModule,
    HomePageModule,
    EntityPageModule,
    NavigationModule,
    CollectionPageModule,
    CollectionsPageModule,
    LibraryModule,
    ManageAccountPageModule,
    AdminModule,
    TypographyModule,
    ProfilePageModule,
    FormsModule,
    ReactiveFormsModule,
    NoRenderModule,
    BrowserTransferStateModule,
    SearchPageModule,
    NgAisModule.forRoot()
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: "/" },
    ApplicationService,
    AuthService,
    {
      provide: APP_INITIALIZER,
      useFactory: (auth: AuthService) => () => auth.init(),
      deps: [AuthService],
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HTTPHeadersInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string
  ) {
    const platform = isPlatformBrowser(this.platformId)
      ? "in the browser"
      : "on the server"
    // console.log(`Running ${platform} with appId=${appId}`);
  }
}
