import { Component, OnInit } from "@angular/core"
import { ApplicationService } from "../../../services/application/application.service"

import { Meta, Title } from "@angular/platform-browser"
import { AdminKindModel } from "../../../models/admin-kind.model"
import { AdminKindService } from "../../../services/admin/kind/admin.kind.service"

@Component({
  selector: "app-admin-kinds",
  templateUrl: "./admin.kinds.component.html",
  styleUrls: [],
})
export class AdminKindsComponent implements OnInit {
  kinds: AdminKindModel[]
  model: AdminKindModel
  submitType: String = "Save"
  showNew: Boolean = false
  showEdit: Boolean = false
  selectedKind: AdminKindModel

  constructor(
    private applicationService: ApplicationService,
    private kindService: AdminKindService,
    private pageTitle: Title,
    private meta: Meta
  ) {}

  ngOnInit(): void {
    this.applicationService.setAdminTemplate()
    this.getKinds()
    this.setPageMetaData()
  }

  private setPageMetaData(): void {
    this.pageTitle.setTitle("Kinds - Admin - Adaptavist Library")
  }

  onNew() {
    this.model = new AdminKindModel()
    this.submitType = "Save"
    this.showNew = true
  }

  onSave() {
    if (this.submitType === "Save") {
      this.kindService.createKind(this.model).subscribe(
        (kind) => {
          this.kinds.push(kind)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    } else {
      this.kindService.updateKind(this.model.id, this.model).subscribe(
        (kind) => {
          this.selectedKind.name = kind.name
          this.selectedKind.shortcode = kind.shortcode
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
    this.showNew = false
  }

  onEdit(kind: AdminKindModel) {
    this.selectedKind = kind
    this.model = Object.assign(
      new AdminKindModel(),
      this.selectedKind as AdminKindModel
    )
    this.submitType = "Update"
    this.showNew = true
    this.showEdit = true

    this.applicationService.scrollTop()
  }

  onDelete(kind: AdminKindModel) {
    if (confirm("Are you sure you want to delete " + kind.name + "?")) {
      this.kindService.deleteKind(kind.id).subscribe(
        () => {
          this.kinds.splice(this.kinds.indexOf(kind), 1)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
  }

  onCancel() {
    this.showNew = false
  }

  getKinds(): void {
    this.kindService.getKinds().subscribe(
      (kinds) => (this.kinds = kinds),
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }
}
