import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { RouterModule } from "@angular/router"

import { AdminCollectionsComponent } from "./collections/admin.collections.component"
import { AdminPartnersComponent } from "./partners/admin.partners.component"
import { AdminProductsComponent } from "./products/admin.products.component"
import { AdminPlatformsComponent } from "./platforms/admin.platforms.component"
import { AdminLeftNavComponent } from "./partials/left-nav/admin.left-nav.component"
import { AdminAppsComponent } from "./apps/admin.apps.component"
import { AdminKindsComponent } from "./kinds/admin.kinds.component"
import { AdminTagsComponent } from "./tags/admin.tags.component"
import { AdminFeaturesComponent } from "./features/admin.features.component"

import { FormsModule } from "@angular/forms"
import { AdminEntitiesComponent } from "./entities/admin.entities.component"

@NgModule({
  imports: [CommonModule, RouterModule, FormsModule],
  exports: [],
  declarations: [
    AdminCollectionsComponent,
    AdminPartnersComponent,
    AdminEntitiesComponent,
    AdminKindsComponent,
    AdminLeftNavComponent,
    AdminProductsComponent,
    AdminPlatformsComponent,
    AdminTagsComponent,
    AdminAppsComponent,
    AdminFeaturesComponent,
  ],
})
export class AdminModule {}
