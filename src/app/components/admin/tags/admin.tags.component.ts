import { Component, OnInit } from "@angular/core"
import { ApplicationService } from "../../../services/application/application.service"

import { Meta, Title } from "@angular/platform-browser"
import { AdminTagModel } from "../../../models/admin-tag.model"
import { AdminTagService } from "../../../services/admin/tag/admin.tag.service"

@Component({
  selector: "app-admin-tags",
  templateUrl: "./admin.tags.component.html",
  styleUrls: [],
})
export class AdminTagsComponent implements OnInit {
  tags: AdminTagModel[]
  model: AdminTagModel
  submitType: String = "Save"
  showNew: Boolean = false
  showEdit: Boolean = false
  selectedTag: AdminTagModel

  constructor(
    private applicationService: ApplicationService,
    private tagService: AdminTagService,
    private pageTitle: Title,
    private meta: Meta
  ) {}

  ngOnInit(): void {
    this.applicationService.setAdminTemplate()
    this.getTags()
    this.setPageMetaData()
  }

  private setPageMetaData(): void {
    this.pageTitle.setTitle("Tags - Admin - Adaptavist Library")
  }

  onNew() {
    this.model = new AdminTagModel()
    this.submitType = "Save"
    this.showNew = true
  }

  onSave() {
    if (this.submitType === "Save") {
      this.tagService.createTag(this.model).subscribe(
        (tag) => {
          this.tags.push(tag)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    } else {
      this.tagService.updateTag(this.model.id, this.model).subscribe(
        (tag) => {
          this.selectedTag.name = tag.name
          this.selectedTag.shortcode = tag.shortcode
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
    this.showNew = false
  }

  onEdit(tag: AdminTagModel) {
    this.selectedTag = tag
    this.model = Object.assign(
      new AdminTagModel(),
      this.selectedTag as AdminTagModel
    )
    this.submitType = "Update"
    this.showNew = true
    this.showEdit = true

    this.applicationService.scrollTop()
  }

  onDelete(tag: AdminTagModel) {
    if (confirm("Are you sure you want to delete " + tag.name + "?")) {
      this.tagService.deleteTag(tag.id).subscribe(
        () => {
          this.tags.splice(this.tags.indexOf(tag), 1)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
  }

  onCancel() {
    this.showNew = false
  }

  getTags(): void {
    this.tagService.getTags().subscribe(
      (tags) => (this.tags = tags),
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }
}
