import { Component, OnInit } from "@angular/core"
import { ApplicationService } from "../../../services/application/application.service"
import { EntityService } from "../../../services/entity/entity.service"
import { AdminPartnerService } from "../../../services/admin/partner/admin.partner.service"
import { Title } from "@angular/platform-browser"
import { EntityModel } from "../../../models/entity.model"
import { AdminPartnerModel } from "../../../models/admin-partner.model"
import { CollectionService } from "../../../services/collection/collection.service"
import { CollectionModel } from "../../../models/collection.model"

@Component({
  selector: "app-admin-collections",
  templateUrl: "./admin.collections.component.html",
  styleUrls: [],
})
export class AdminCollectionsComponent implements OnInit {
  collections: CollectionModel[]
  entities: EntityModel[]
  partners: AdminPartnerModel[]
  model: CollectionModel
  submitType = "Save"
  showNew = false
  showEdit = false
  showAssignEntities = false
  showAssignPartners = false
  selectedCollection: CollectionModel

  constructor(
    private applicationService: ApplicationService,
    private collectionService: CollectionService,
    private entityService: EntityService,
    private partnerService: AdminPartnerService,
    private pageTitle: Title
  ) {}

  ngOnInit(): void {
    this.getCollectionList()
    this.getEntityList()
    this.getPartners()
    this.applicationService.setAdminTemplate()
    this.setPageMetaData()
  }

  onNew() {
    this.model = new CollectionModel()
    this.submitType = "Save"
    this.showNew = true
    this.showAssignEntities = false
    this.showAssignPartners = false
  }

  onSave() {
    if (this.submitType === "Save") {
      this.collectionService.createCollection(this.model).subscribe(
        (collection) => {
          this.collections.push(collection)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    } else {
      this.collectionService
        .updateCollection(this.model.id, this.model)
        .subscribe(
          (collection) => {
            this.selectedCollection.name = collection.name
            this.selectedCollection.slug = collection.slug
            this.selectedCollection.description = collection.description
            this.selectedCollection.meta_description =
              collection.meta_description
          },
          (error) => {
            console.log(JSON.stringify(error.error))
            // this.applicationService.handleError(error);
          }
        )
    }
    this.showNew = false
  }

  onEdit(collection: CollectionModel) {
    this.selectedCollection = collection
    this.model = new CollectionModel()
    this.model = Object.assign({}, this.selectedCollection)
    this.submitType = "Update"
    this.showAssignEntities = false
    this.showAssignPartners = false
    this.showNew = true
    this.showEdit = true

    this.applicationService.scrollTop()
  }

  onDelete(collection: CollectionModel) {
    if (confirm("Are you sure you want to delete " + collection.name + "?")) {
      this.collectionService.deleteCollection(collection.id).subscribe(
        () => {
          this.collections.splice(this.collections.indexOf(collection), 1)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
  }

  // TODO: (AL-709) The methods of this component are used to manage the current library CMS.
  // With the use of GraphCMS this would no longer be necessary.

  /*onFileChange(event) {
    if (event.target.files.length > 0) {
      this.model.file = event.target.files[0]
    }
  }*/

  onCancel() {
    this.showNew = false
  }

  onAssignEntities(collection: CollectionModel) {
    this.selectedCollection = collection
    this.model = new CollectionModel()
    this.model = Object.assign({}, this.selectedCollection)
    this.showNew = false
    this.showAssignEntities = true
    this.showAssignPartners = false

    this.applicationService.scrollTop()
  }

  onAssignPartners(collection: CollectionModel) {
    this.selectedCollection = collection
    this.model = new CollectionModel()
    this.model = Object.assign({}, this.selectedCollection)

    this.showNew = false
    this.showAssignEntities = false
    this.showAssignPartners = true

    this.applicationService.scrollTop()
  }

  toggleAssignEntity(collection: CollectionModel, entity: EntityModel) {
    if (this.hasEntity(collection, entity)) {
      this.unAssignEntity(collection, entity)
    } else {
      this.assignEntity(collection, entity)
    }
  }

  toggleAssignPartner(collection: CollectionModel, partner: AdminPartnerModel) {
    if (this.hasPartner(collection, partner)) {
      this.unAssignPartner(collection, partner)
    } else {
      this.assignPartner(collection, partner)
    }
  }

  assignEntity(collection: CollectionModel, entity: EntityModel) {
    this.collectionService.assignEntity(collection.id, entity.id).subscribe(
      () => {
        collection.entities.push(entity)
      },
      (error) => {
        console.log(JSON.stringify(error.error))
        // this.applicationService.handleError(error);
      }
    )
  }

  unAssignEntity(collection: CollectionModel, entity: EntityModel) {
    if (
      confirm(
        "Are you sure you want to remove " +
          entity.name +
          " from " +
          collection.name +
          "?"
      )
    ) {
      const index = collection.entities.findIndex(
        (findEntity) => findEntity.id === entity.id
      )

      this.collectionService.unAssignEntity(collection.id, entity.id).subscribe(
        () => {
          collection.entities.splice(index, 1)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          collection.entities.splice(index, 1)
          // this.applicationService.handleError(error);
        }
      )
    }
  }

  assignPartner(collection: CollectionModel, partner: AdminPartnerModel) {
    this.collectionService.assignPartner(collection.id, partner.id).subscribe(
      (c) => {
        collection.partners.push(partner)
        this.selectedCollection.partner = c.partner
      },
      (error) => {
        console.log(JSON.stringify(error.error))
        // this.applicationService.handleError(error);
      }
    )
  }

  unAssignPartner(collection: CollectionModel, partner: AdminPartnerModel) {
    if (
      confirm(
        "Are you sure you want to remove " +
          partner.name +
          " from " +
          collection.name +
          "?"
      )
    ) {
      const index = collection.partners.findIndex(
        (findPartner) => findPartner["id"] === partner.id
      )

      this.collectionService
        .unAssignPartner(collection.id, partner.id)
        .subscribe(
          (c) => {
            collection.partners.splice(index, 1)
            this.selectedCollection.partner = c.partner
          },
          (error) => {
            console.log(JSON.stringify(error.error))
            collection.partners.splice(index, 1)
            // this.applicationService.handleError(error);
          }
        )
    }
  }

  getCollectionList(): void {
    this.collectionService.getCollectionListWithEntitiesAndPartners().subscribe(
      (collections) => (this.collections = collections),
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }

  getEntityList(): void {
    this.entityService.getEntityList(200, "name", "ASC").subscribe(
      (entities) => (this.entities = entities),
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }

  getPartners(): void {
    this.partnerService.getPartners().subscribe(
      (partners) => {
        this.partners = partners
      },
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }

  hasEntity(collection: CollectionModel, entity: EntityModel): boolean {
    return collection.entities.some((obj) => obj.id === entity.id)
  }

  hasPartner(collection: CollectionModel, partner: AdminPartnerModel): boolean {
    return collection.partners.some((obj) => obj["id"] === partner.id)
  }

  private setPageMetaData(): void {
    this.pageTitle.setTitle("Collections - Admin - Adaptavist Library")
  }
}
