import { Component, OnInit } from "@angular/core"
import { ApplicationService } from "../../../services/application/application.service"

import { Meta, Title } from "@angular/platform-browser"
import { AdminPartnerModel } from "../../../models/admin-partner.model"
import { AdminPartnerService } from "../../../services/admin/partner/admin.partner.service"

@Component({
  selector: "app-admin-partners",
  templateUrl: "./admin.partners.component.html",
  styleUrls: [],
})
export class AdminPartnersComponent implements OnInit {
  partners: AdminPartnerModel[]
  model: AdminPartnerModel
  submitType: String = "Save"
  showNew: Boolean = false
  showEdit: Boolean = false
  selectedPartner: AdminPartnerModel

  constructor(
    private applicationService: ApplicationService,
    private partnerService: AdminPartnerService,
    private pageTitle: Title,
    private meta: Meta
  ) {}

  ngOnInit(): void {
    this.applicationService.setAdminTemplate()
    this.getPartners()
    this.setPageMetaData()
  }

  private setPageMetaData(): void {
    this.pageTitle.setTitle("Partners - Admin - Adaptavist Library")
  }

  onNew() {
    this.model = new AdminPartnerModel()
    this.submitType = "Save"
    this.showNew = true
  }

  onSave() {
    if (this.submitType === "Save") {
      this.partnerService.createPartner(this.model).subscribe(
        (partner) => {
          this.partners.push(partner)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    } else {
      this.partnerService.updatePartner(this.model.id, this.model).subscribe(
        (partner) => {
          this.selectedPartner.name = partner.name
          this.selectedPartner.url = partner.url
          this.selectedPartner.product_name = partner.product_name
          this.selectedPartner.product_url = partner.product_url
          this.selectedPartner.icon = partner.icon
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
    this.showNew = false
  }

  onEdit(partner: AdminPartnerModel) {
    this.selectedPartner = partner
    this.model = Object.assign(
      new AdminPartnerModel(),
      this.selectedPartner as AdminPartnerModel
    )
    this.submitType = "Update"
    this.showNew = true
    this.showEdit = true

    this.applicationService.scrollTop()
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.model.file = event.target.files[0]
    }
  }

  onDelete(partner: AdminPartnerModel) {
    if (confirm("Are you sure you want to delete " + partner.name + "?")) {
      this.partnerService.deletePartner(partner.id).subscribe(
        (unused) => {
          this.partners.splice(this.partners.indexOf(partner), 1)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
  }

  onCancel() {
    this.showNew = false
  }

  getPartners(): void {
    this.partnerService.getPartners().subscribe(
      (partners) => (this.partners = partners),
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }
}
