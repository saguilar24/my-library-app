import { Component, OnInit } from "@angular/core"
import { ApplicationService } from "../../../services/application/application.service"

import { Meta, Title } from "@angular/platform-browser"
import { AppModel } from "../../../models/app.model"
import { AdminAppService } from "../../../services/admin/app/admin.app.service"

@Component({
  selector: "app-admin-apps",
  templateUrl: "./admin.apps.component.html",
  styleUrls: [],
})
export class AdminAppsComponent implements OnInit {
  apps: AppModel[]
  model: AppModel
  submitType = "Save"
  showNew = false
  showEdit = false
  selectedApp: AppModel

  constructor(
    private applicationService: ApplicationService,
    private appService: AdminAppService,
    private pageTitle: Title,
    private meta: Meta
  ) {}

  ngOnInit(): void {
    this.applicationService.setAdminTemplate()
    this.getApps()
    this.setPageMetaData()
  }

  private setPageMetaData(): void {
    this.pageTitle.setTitle("Apps - Admin - Adaptavist Library")
  }

  onNew() {
    this.model = new AppModel()
    this.submitType = "Save"
    this.showNew = true
  }

  onSave() {
    if (this.submitType === "Save") {
      this.appService.createApp(this.model).subscribe(
        (app) => {
          this.apps.push(app)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    } else {
      this.appService.updateApp(this.model.id, this.model).subscribe(
        (app) => {
          this.selectedApp.name = app.name
          this.selectedApp.pretty_name = app.pretty_name
          this.selectedApp.shortcode = app.shortcode
          this.selectedApp.url = app.url
          this.selectedApp.product_url = app.product_url
          this.selectedApp.company_name = app.company_name
          this.selectedApp.icon = app.icon
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
    this.showNew = false
  }

  onEdit(app: AppModel) {
    this.selectedApp = app
    this.model = Object.assign(new AppModel(), this.selectedApp as AppModel)
    this.submitType = "Update"
    this.showNew = true
    this.showEdit = true

    this.applicationService.scrollTop()
  }

  onFileChange(event: { target: { files: string | any[] } }) {
    if (event.target.files.length > 0) {
      this.model.file = event.target.files[0]
    }
  }

  onDelete(app: AppModel) {
    if (confirm("Are you sure you want to delete " + app.name + "?")) {
      this.appService.deleteApp(app.id).subscribe(
        () => {
          this.apps.splice(this.apps.indexOf(app), 1)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
  }

  onCancel() {
    this.showNew = false
  }

  getApps(): void {
    this.appService.getApps().subscribe(
      (apps) => (this.apps = apps),
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }
}
