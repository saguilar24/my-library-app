import { Component, OnInit } from "@angular/core"
import { ApplicationService } from "../../../services/application/application.service"

import { Meta, Title } from "@angular/platform-browser"
import { AdminProductModel } from "../../../models/admin-product.model"
import { AdminProductService } from "../../../services/admin/product/admin.product.service"

@Component({
  selector: "app-admin-products",
  templateUrl: "./admin.products.component.html",
  styleUrls: [],
})
export class AdminProductsComponent implements OnInit {
  products: AdminProductModel[]
  model: AdminProductModel
  submitType: String = "Save"
  showNew: Boolean = false
  showEdit: Boolean = false
  selectedProduct: AdminProductModel

  constructor(
    private applicationService: ApplicationService,
    private productService: AdminProductService,
    private pageTitle: Title,
    private meta: Meta
  ) {}

  ngOnInit(): void {
    this.applicationService.setAdminTemplate()
    this.getProducts()
    this.setPageMetaData()
  }

  private setPageMetaData(): void {
    this.pageTitle.setTitle("Products - Admin - Adaptavist Library")
  }

  onNew() {
    this.model = new AdminProductModel()
    this.submitType = "Save"
    this.showNew = true
  }

  onSave() {
    if (this.submitType === "Save") {
      this.productService.createProduct(this.model).subscribe(
        (product) => {
          this.products.push(product)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    } else {
      this.productService.updateProduct(this.model.id, this.model).subscribe(
        (product) => {
          this.selectedProduct.name = product.name
          this.selectedProduct.shortcode = product.shortcode
          this.selectedProduct.icon = product.icon
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
    this.showNew = false
  }

  onEdit(product: AdminProductModel) {
    this.selectedProduct = product
    this.model = Object.assign(
      new AdminProductModel(),
      this.selectedProduct as AdminProductModel
    )
    this.submitType = "Update"
    this.showNew = true
    this.showEdit = true

    this.applicationService.scrollTop()
  }

  onDelete(product: AdminProductModel) {
    if (confirm("Are you sure you want to delete " + product.name + "?")) {
      this.productService.deleteProduct(product.id).subscribe(
        () => {
          this.products.splice(this.products.indexOf(product), 1)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.model.file = event.target.files[0]
    }
  }

  onCancel() {
    this.showNew = false
  }

  getProducts(): void {
    this.productService.getProducts().subscribe(
      (products) => (this.products = products),
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }
}
