import { Component, OnInit } from "@angular/core"
import { ApplicationService } from "../../../services/application/application.service"

import { Meta, Title } from "@angular/platform-browser"
import { AdminFeatureModel } from "../../../models/admin-feature.model"
import { AdminFeatureService } from "../../../services/admin/feature/admin.feature.service"

@Component({
  selector: "app-admin-features",
  templateUrl: "./admin.features.component.html",
  styleUrls: [],
})
export class AdminFeaturesComponent implements OnInit {
  features: AdminFeatureModel[]
  model: AdminFeatureModel
  submitType: String = "Save"
  showNew: Boolean = false
  showEdit: Boolean = false
  selectedFeature: AdminFeatureModel

  constructor(
    private applicationService: ApplicationService,
    private featureService: AdminFeatureService,
    private pageTitle: Title,
    private meta: Meta
  ) {}

  ngOnInit(): void {
    this.applicationService.setAdminTemplate()
    this.getFeatures()
    this.setPageMetaData()
  }

  private setPageMetaData(): void {
    this.pageTitle.setTitle("Features - Admin - Adaptavist Library")
  }

  onNew() {
    this.model = new AdminFeatureModel()
    this.submitType = "Save"
    this.showNew = true
  }

  onSave() {
    if (this.submitType === "Save") {
      this.featureService.createFeature(this.model).subscribe(
        (feature) => {
          this.features.push(feature)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    } else {
      this.featureService.updateFeature(this.model.id, this.model).subscribe(
        (feature) => {
          this.selectedFeature.name = feature.name
          this.selectedFeature.shortcode = feature.shortcode
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
    this.showNew = false
  }

  onEdit(feature: AdminFeatureModel) {
    this.selectedFeature = feature
    this.model = Object.assign(
      new AdminFeatureModel(),
      this.selectedFeature as AdminFeatureModel
    )
    this.submitType = "Update"
    this.showNew = true
    this.showEdit = true

    this.applicationService.scrollTop()
  }

  onDelete(feature: AdminFeatureModel) {
    if (confirm("Are you sure you want to delete " + feature.name + "?")) {
      this.featureService.deleteFeature(feature.id).subscribe(
        () => {
          this.features.splice(this.features.indexOf(feature), 1)
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          // this.applicationService.handleError(error);
        }
      )
    }
  }

  onCancel() {
    this.showNew = false
  }

  getFeatures(): void {
    this.featureService.getFeatures().subscribe(
      (features) => (this.features = features),
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }
}
