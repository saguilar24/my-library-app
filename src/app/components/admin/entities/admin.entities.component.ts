import { Component, OnInit } from "@angular/core"

import { Meta, Title } from "@angular/platform-browser"
import { EntityModel } from "../../../models/entity.model"
import { AdminPartnerModel } from "../../../models/admin-partner.model"
import { ApplicationService } from "../../../services/application/application.service"
import { EntityService } from "../../../services/entity/entity.service"
import { AdminPartnerService } from "../../../services/admin/partner/admin.partner.service"
import { AdminUserModel } from "../../../models/admin-user.model"
import { AdminUserService } from "../../../services/admin/user/admin.user.service"

@Component({
  selector: "app-admin-entities",
  templateUrl: "./admin.entities.component.html",
  styleUrls: [],
})
export class AdminEntitiesComponent implements OnInit {
  entities: EntityModel[]
  model: EntityModel
  partners: AdminPartnerModel[]
  authors: AdminUserModel[]
  selectedEntity: EntityModel
  submitType = "Save"
  showNew = false
  showEdit = false
  showAssignPartners = false
  showAssignAuthor = false

  constructor(
    private applicationService: ApplicationService,
    private entityService: EntityService,
    private pageTitle: Title,
    private partnerService: AdminPartnerService,
    private adminUserService: AdminUserService,
    private meta: Meta
  ) {}

  ngOnInit(): void {
    this.applicationService.setAdminTemplate()
    this.getEntityList()
    this.setPageMetaData()
    this.getPartners()
    this.getVerifiedUsers()
  }

  onSave() {
    this.entityService.updateEntity(this.model.id, this.model).subscribe(
      (entity) => {
        this.selectedEntity.name = entity.name
        this.selectedEntity.slug = entity.slug
        this.selectedEntity.description = entity.description
        this.selectedEntity.meta_description = entity.meta_description
      },
      (error) => {
        console.log(JSON.stringify(error.error))
        // this.applicationService.handleError(error);
      }
    )
  }

  onEdit(entity: EntityModel) {
    this.selectedEntity = entity
    this.model = new EntityModel()
    this.model = Object.assign({}, this.selectedEntity)
    this.submitType = "Update"
    this.showAssignPartners = false
    this.showNew = true
    this.showEdit = true

    this.applicationService.scrollTop()
  }

  onCancel() {
    this.showNew = false
  }

  private setPageMetaData(): void {
    this.pageTitle.setTitle("Entities - Admin - Adaptavist Library")
  }

  getEntityList(): void {
    this.entityService
      .getEntityListWithPartnersAndAuthor(200, "attachment_updated_at", "desc")
      .subscribe(
        (entities) => (this.entities = entities),
        (error) => {
          this.applicationService.handleError(error)
        }
      )
  }

  toggleTopPick(entity: EntityModel, markAsTopPick: boolean) {
    entity.top_pick = markAsTopPick
    this.entityService.updateEntity(entity.id, entity).subscribe(
      (e) => {},
      (error) => {
        console.log(JSON.stringify(error.error))
        // this.applicationService.handleError(error);
      }
    )
  }

  onAssignPartners(entity: EntityModel) {
    this.selectedEntity = entity
    this.model = new EntityModel()
    this.model = Object.assign({}, this.selectedEntity)

    this.showNew = false
    this.showAssignPartners = true
    this.showAssignAuthor = false

    this.applicationService.scrollTop()
  }

  onAssignAuthor(entity: EntityModel) {
    this.selectedEntity = entity
    this.model = new EntityModel()
    this.model = Object.assign({}, this.selectedEntity)

    this.showNew = false
    this.showAssignAuthor = true
    this.showAssignPartners = false

    this.applicationService.scrollTop()
  }

  toggleAssignPartner(entity: EntityModel, partner: AdminPartnerModel) {
    if (this.hasPartner(entity, partner)) {
      this.unAssignPartner(entity, partner)
    } else {
      this.assignPartner(entity, partner)
    }
  }

  toggleAssignAuthor(entity: EntityModel, author: AdminUserModel) {
    this.assignAuthor(entity, author)
  }

  assignPartner(entity: EntityModel, partner: AdminPartnerModel) {
    this.entityService.assignPartner(entity.id, partner.id).subscribe(
      (e) => {
        entity.partners.push(partner)
        this.selectedEntity.partner = e.partner
      },
      (error) => {
        console.log(JSON.stringify(error.error))
        // this.applicationService.handleError(error);
      }
    )
  }

  assignAuthor(entity: EntityModel, author: AdminUserModel) {
    this.entityService.assignAuthor(entity.id, author.id).subscribe(
      (e) => {
        // entity.author
        this.selectedEntity.author = e.author
      },
      (error) => {
        console.log(JSON.stringify(error.error))
      }
    )
  }

  unAssignAuthor(entity: EntityModel) {
    this.entityService.unAssignAuthor(entity.id).subscribe(
      (e) => {
        this.selectedEntity.author = null
      },
      (error) => {
        console.log(JSON.stringify(error.error))
      }
    )
  }

  unAssignPartner(entity: EntityModel, partner: AdminPartnerModel) {
    if (
      confirm(
        "Are you sure you want to remove " +
          partner.name +
          " from " +
          entity.name +
          "?"
      )
    ) {
      const index = entity.partners.findIndex(
        (findPartner) => findPartner["id"] === partner.id
      )

      this.entityService.unAssignPartner(entity.id, partner.id).subscribe(
        (c) => {
          entity.partners.splice(index, 1)
          this.selectedEntity.partner = c.partner
        },
        (error) => {
          console.log(JSON.stringify(error.error))
          entity.partners.splice(index, 1)
          // this.applicationService.handleError(error);
        }
      )
    }
  }

  getPartners(): void {
    this.partnerService.getPartners().subscribe(
      (partners) => {
        this.partners = partners
      },
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }

  hasPartner(entity: EntityModel, partner: AdminPartnerModel): boolean {
    return entity.partners.some((obj) => obj["id"] === partner.id)
  }

  getVerifiedUsers(): void {
    this.adminUserService.getVerifiedUsers().subscribe(
      (verifiedUsers) => {
        this.authors = verifiedUsers
      },
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }

  hasVerifiedUser(entity: EntityModel, author: AdminUserModel): boolean {
    if (entity.author) {
      return entity.author.email === author.email
    }
    return false
  }

  hasUser(entity: EntityModel): boolean {
    if (entity.author) {
      return true
    }
    return false
  }
}
