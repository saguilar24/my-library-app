import { Component, OnInit } from "@angular/core"
import { ApplicationService } from "../../../services/application/application.service"

import { Meta, Title } from "@angular/platform-browser"
import { AdminPlatformModel } from "../../../models/admin-platform.model"
import { AdminPlatformService } from "../../../services/admin/platform/admin.platform.service"

@Component({
  selector: "app-admin-platforms",
  templateUrl: "./admin.platforms.component.html",
  styleUrls: [],
})
export class AdminPlatformsComponent implements OnInit {
  platforms: AdminPlatformModel[]
  model: AdminPlatformModel
  submitType: String = "Save"
  showNew: Boolean = false
  showEdit: Boolean = false
  selectedPlatform: AdminPlatformModel

  constructor(
    private applicationService: ApplicationService,
    private platformService: AdminPlatformService,
    private pageTitle: Title,
    private meta: Meta
  ) {}

  ngOnInit(): void {
    this.applicationService.setAdminTemplate()
    this.getPlatforms()
    this.setPageMetaData()
  }

  private setPageMetaData(): void {
    this.pageTitle.setTitle("Platforms - Admin - Adaptavist Library")
  }

  onSave() {
    this.platformService.updatePlatform(this.model.id, this.model).subscribe(
      (platform) => {
        this.selectedPlatform.name = platform.name
        this.selectedPlatform.icon = platform.icon
      },
      (error) => {
        console.log(JSON.stringify(error.error))
        // this.applicationService.handleError(error);
      }
    )

    this.showNew = false
  }

  onEdit(platform: AdminPlatformModel) {
    this.selectedPlatform = platform
    this.model = Object.assign(
      new AdminPlatformModel(),
      this.selectedPlatform as AdminPlatformModel
    )
    this.submitType = "Update"
    this.showNew = true
    this.showEdit = true

    this.applicationService.scrollTop()
  }

  onCancel() {
    this.showNew = false
  }

  getPlatforms(): void {
    this.platformService.getPlatforms().subscribe(
      (platforms) => (this.platforms = platforms),
      (error) => {
        this.applicationService.handleError(error)
      }
    )
  }
}
