import { NgModule } from "@angular/core"
import { RouterModule, Routes } from "@angular/router"
import { AdminGuard } from "../../guards/admin.guard"
import { AdminAppsComponent } from "./apps/admin.apps.component"
import { AdminCollectionsComponent } from "./collections/admin.collections.component"
import { AdminPartnersComponent } from "./partners/admin.partners.component"
import { AdminKindsComponent } from "./kinds/admin.kinds.component"
import { AdminPlatformsComponent } from "./platforms/admin.platforms.component"
import { AdminProductsComponent } from "./products/admin.products.component"
import { AdminTagsComponent } from "./tags/admin.tags.component"
import { AdminFeaturesComponent } from "./features/admin.features.component"
import { AdminEntitiesComponent } from "./entities/admin.entities.component"

const routes: Routes = [
  { path: "dexad", redirectTo: "/dexad/collections", pathMatch: "full" },
  {
    path: "dexad",
    canActivate: [AdminGuard],
    children: [
      {
        path: "apps",
        component: AdminAppsComponent,
      },
      {
        path: "collections",
        component: AdminCollectionsComponent,
      },
      {
        path: "partners",
        component: AdminPartnersComponent,
      },
      {
        path: "entities",
        component: AdminEntitiesComponent,
      },
      {
        path: "kinds",
        component: AdminKindsComponent,
      },
      {
        path: "platforms",
        component: AdminPlatformsComponent,
      },
      {
        path: "products",
        component: AdminProductsComponent,
      },
      {
        path: "tags",
        component: AdminTagsComponent,
      },
      {
        path: "features",
        component: AdminFeaturesComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: "legacy" })],
  exports: [RouterModule],
  providers: [AdminGuard],
})
export class AdminRoutingModule {}
