import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { ShareIconComponent } from "./share-icon/share-icon.component"

@NgModule({
  imports: [CommonModule],
  exports: [ShareIconComponent],
  declarations: [ShareIconComponent],
})
export class IconsModule {}
