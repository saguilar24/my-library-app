import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { NgAisModule } from "angular-instantsearch"
import { SideBarAndContentComponent } from "./side-bar-and-content.component"
import { MainContentComponent } from "./main-content.component"
import { FilterBarAndContentComponent } from "./filter-bar-and-content.component"
import { SearchSidebarPartialComponent } from "../../partials/search-sidebar-partial/search-sidebar-partial.component"

@NgModule({
  imports: [CommonModule, NgAisModule],
  exports: [
    FilterBarAndContentComponent,
    SideBarAndContentComponent,
    MainContentComponent,
    SearchSidebarPartialComponent,
  ],
  declarations: [
    FilterBarAndContentComponent,
    SideBarAndContentComponent,
    MainContentComponent,
    SearchSidebarPartialComponent,
  ],
})
export class LayoutModule {}
