import { Component } from "@angular/core"

@Component({
  selector: "app-filter-bar-and-content",
  template: `
    <div class="l-page--wrapper">
      <div class="c-filter-content-area">
        <div class="c-filter-content-area--sidebar l-grid-cell">
          <ng-content select="[sidebar]"></ng-content>
        </div>
        <div class="c-filter-content-area--content l-grid-cell">
          <ng-content select="[content]"></ng-content>
        </div>
      </div>
    </div>
  `,
})
export class FilterBarAndContentComponent {}
