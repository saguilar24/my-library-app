import { Component } from "@angular/core"

@Component({
  selector: "app-main-content",
  template: `
    <div class="l-page--wrapper l-padding-top--double-gutter">
      <ng-content select="[content]"></ng-content>
    </div>
  `,
})
export class MainContentComponent {}
