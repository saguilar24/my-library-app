import { Component } from "@angular/core"

@Component({
  selector: "app-side-bar-and-content",
  template: `
    <div class="l-page--wrapper">
      <div class="c-content-area">
        <div class="c-content-area--sidebar">
          <ng-content select="[sidebar]"></ng-content>
        </div>
        <div class="c-content-area--content">
          <ng-content select="[content]"></ng-content>
        </div>
      </div>
    </div>
  `,
})
export class SideBarAndContentComponent {}
