import { Component, Input } from "@angular/core"

@Component({
  selector: "app-text-list-separator",
  template: `<span>{{ displayConnector() }}</span>`,
})
export class TextListSeparatorComponent {
  @Input() index: number
  @Input() length: number

  displayConnector(): string {
    return this.index >= this.length - 1
      ? ""
      : this.length === 2 || this.index === this.length - 2
      ? " & "
      : ", "
  }
}
