import { Component, Input } from "@angular/core"

@Component({
  selector: "app-section-title",
  template: `
    <h2>
      {{ titleBase }}
      <span *ngIf="titleEmphasis" class="c-section-title--emphasis">{{
        titleEmphasis
      }}</span>
    </h2>
  `,
})
export class SectionTitleComponent {
  @Input() titleBase: string
  @Input() titleEmphasis: string
}
