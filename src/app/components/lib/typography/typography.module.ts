import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { RouterModule } from "@angular/router"
import { SectionTitleComponent } from "./section-title.component"
import { TextListSeparatorComponent } from "./text-list-separator.component"
import { VerifiedComponent } from "./verified.component"

@NgModule({
  imports: [CommonModule, RouterModule],
  exports: [
    SectionTitleComponent,
    TextListSeparatorComponent,
    VerifiedComponent,
  ],
  declarations: [
    SectionTitleComponent,
    TextListSeparatorComponent,
    VerifiedComponent,
  ],
})
export class TypographyModule {}
