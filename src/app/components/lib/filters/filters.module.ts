import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { MoreFiltersBlockComponent } from "./more-filters-block.component"
import { FilterButtonComponent } from "./filter-button.component"

@NgModule({
  imports: [CommonModule],
  exports: [MoreFiltersBlockComponent, FilterButtonComponent],
  declarations: [MoreFiltersBlockComponent, FilterButtonComponent],
})
export class FiltersModule {}
