import { Component, Input, OnInit } from "@angular/core"
import { BehaviorSubject } from "rxjs"

@Component({
  selector: "app-more-filters-block",
  template: `
    <button class="c-filter-menu--more" (click)="handleToggle()">
      {{ displayButtonText() }}
    </button>
    <div *ngIf="showMoreFilters.getValue()">
      <ng-content></ng-content>
    </div>
  `,
})
export class MoreFiltersBlockComponent implements OnInit {
  @Input() showMoreFilters: BehaviorSubject<boolean>

  constructor() {}

  ngOnInit() {}

  handleToggle() {
    this.showMoreFilters.next(!this.showMoreFilters.getValue())
  }

  displayButtonText() {
    return this.showMoreFilters.getValue() ? "Close filters" : "+ More filters"
  }
}
