import { Component, Input, OnInit } from "@angular/core"
import { BehaviorSubject } from "rxjs"

@Component({
  selector: "app-filter-button",
  template: `
    <button (click)="handleToggle()" class="c-filter-menu--button">
      {{ !isOpen.getValue() ? "Filters" : "Close" }}
    </button>
  `,
})
export class FilterButtonComponent implements OnInit {
  @Input() isOpen: BehaviorSubject<boolean>

  constructor() {}

  ngOnInit() {}

  handleToggle() {
    this.isOpen.next(!this.isOpen.getValue())
  }
}
