import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { RouterModule } from "@angular/router"
import { DropdownComponent } from "./dropdown.component"
import { RequirementsContainerComponent } from "./requirements-container.component"
import { RequirementsTagComponent } from "./requirements-tag.component"
import { IconsModule } from "../icons/icons.module"

@NgModule({
  imports: [CommonModule, RouterModule, IconsModule],
  exports: [
    DropdownComponent,
    RequirementsContainerComponent,
    RequirementsTagComponent,
  ],
  declarations: [
    DropdownComponent,
    RequirementsContainerComponent,
    RequirementsTagComponent,
  ],
})
export class RequirementsModule {}
