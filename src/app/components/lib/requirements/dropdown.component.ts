import { Component } from "@angular/core"

@Component({
  selector: "app-dropdown",
  template: `
    <select class="c-requirements-dropdown" title="Versions">
      <ng-content></ng-content>
    </select>
  `,
})
export class DropdownComponent {}
