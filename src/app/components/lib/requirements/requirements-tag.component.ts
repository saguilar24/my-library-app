import { Component, Input, OnInit } from "@angular/core"
import { RequirementTagModel } from "../../../models/requirement-tag.model"

@Component({
  selector: "app-requirements-tag",
  template: `
    <p class="c--requirement-text">
      <img
        class="c-button--app-icon"
        src="{{ requirement.icon.src }}"
        alt="{{ requirement.icon.alt }}"
      />
      {{ requirement.text }}
    </p>
  `,
})
export class RequirementsTagComponent implements OnInit {
  @Input() requirement: RequirementTagModel

  constructor() {}

  ngOnInit() {}
}
