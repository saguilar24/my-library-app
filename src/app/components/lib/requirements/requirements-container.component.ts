import { Component, OnInit } from "@angular/core"

@Component({
  selector: "app-requirements-container",
  template: `
    <div class="c-requirements-block">
      <ng-content></ng-content>
    </div>
  `,
})
export class RequirementsContainerComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
