import { Component, Input } from "@angular/core"
import { BehaviorSubject } from "rxjs"
import { animate, style, transition, trigger } from "@angular/animations"

@Component({
  selector: "app-modal",
  template: `
    <div
      @fade
      *ngIf="showModal$.getValue()"
      class="c-modal--overlay"
      (click)="close()"
    ></div>
    <div @fade *ngIf="showModal$.getValue()" class="c-modal--holder">
      <ng-content></ng-content>
      <button
        id="modal-close"
        class="c-modal--close-button c-button c-button--transparent l-padding--none"
        (click)="close()"
      >
        <svg
          width="10px"
          height="10px"
          viewBox="0 0 10 10"
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          xmlns:xlink="http://www.w3.org/1999/xlink"
        >
          <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g
              transform="translate(-330.000000, -269.000000)"
              fill="#ffffff"
              fill-rule="nonzero"
            >
              <path
                d="M338.48629,269.26715 C338.832521,268.91095 339.399459,268.91095 339.74569,269.26715 C340.084776,269.616 340.084776,270.176313 339.745657,270.525197 L336.238339,274.13276 L339.486876,277.474837 C339.779957,277.776358 339.823282,278.241985 339.571926,278.629394 L339.486876,278.73285 C339.187765,279.040573 338.71628,279.087006 338.32833,278.820614 L338.227459,278.732832 L334.997604,275.409615 L331.767731,278.73285 C331.421501,279.08905 330.854562,279.08905 330.508332,278.73285 C330.169246,278.384 330.169246,277.823687 330.508292,277.474879 L333.756035,274.132763 L330.249518,270.525163 C329.956437,270.223642 329.913111,269.758015 330.164468,269.370606 L330.249518,269.26715 C330.548628,268.959427 331.020113,268.912994 331.408064,269.179386 L331.508901,269.267133 L334.997604,272.855911 L338.48629,269.26715 Z"
                id="Path"
              ></path>
            </g>
          </g>
        </svg>
      </button>
    </div>
  `,
  animations: [
    trigger("fade", [
      transition(":enter", [
        style({ opacity: 0 }),
        animate("260ms", style({ opacity: 1 })),
      ]),
      transition(":leave", [animate("180ms", style({ opacity: 0 }))]),
    ]),
  ],
})
export class ModalComponent {
  @Input() private _showModal$: BehaviorSubject<boolean>

  get showModal$(): BehaviorSubject<boolean> {
    return this._showModal$
  }

  close() {
    this._showModal$.next(false)
  }
}
