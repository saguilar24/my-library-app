import { Component, Input } from "@angular/core"
import { CardCollectionModel } from "../../../models/card-collection.model"

@Component({
  selector: "app-card-collection-item",
  template: `
    <div class="c-card-item">
      <p class="c-card-collection--new" *ngIf="isReady && isNew()">NEW</p>
      <div class="c-card-image" [ngClass]="!isReady ? 'u-dummy-image' : ''">
        <img
          *ngIf="isReady"
          alt="{{ card.image.alt }}"
          src="{{ card.image.src }}"
        />
      </div>
      <div class="c-card-collection--text c-card-background--alt-white">
        <div class="c-card-item-text">
          <div
            class="c-card-item--category-title"
            [ngClass]="
              !isReady ? 'u-dummy-text-bar u-dummy-text-bar--short' : ''
            "
          >
            <ng-content select="[categories]"></ng-content>
          </div>
          <div
            [ngClass]="
              !isReady ? 'u-dummy-text-bar u-dummy-text-bar--double' : ''
            "
          ></div>
          <a
            class="c-collection--link c-card-item--card-title"
            *ngIf="isReady"
            [routerLink]="['/collection/', card.slug]"
            [ngClass]="
              !isReady ? 'u-dummy-text-bar u-dummy-text-bar--double' : ''
            "
          >
            {{ card.name }}
          </a>
        </div>
        <ng-container>
          <div
            class="c-card-item--script-text"
            [ngClass]="
              !isReady ? 'u-dummy-text-bar u-dummy-text-bar--link' : ''
            "
          >
            <ng-content select="[viewType]"></ng-content>
          </div>
        </ng-container>
      </div>
    </div>
  `,
})
export class CardCollectionItemComponent {
  @Input() card: CardCollectionModel
  @Input() isReady = false

  isNew() {
    if (this.isReady) {
      if (typeof this.card.created_at === "undefined") {
        return false
      }
      const fortnightAgo = new Date(Date.now() - 12096e5).valueOf()
      const createdAt = Date.parse(this.card.created_at)

      return createdAt > fortnightAgo
    }
  }
}
