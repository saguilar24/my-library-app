import { Component, Input } from "@angular/core"
import { CardModel } from "../../../models/card.model"

@Component({
  selector: "app-card-item",
  template: `
    <div class="c-card-item l-padding--gutter c-card-background--alt-white">
      <p class="c-card-item--new" *ngIf="isReady && isNew()">NEW</p>
      <div class="c-card-item-text">
        <div
          class="c-card-item--category-title"
          [ngClass]="!isReady ? 'u-dummy-text-bar u-dummy-text-bar--short' : ''"
        >
          <ng-content select="[categories]"></ng-content>
        </div>
        <div
          [ngClass]="
            !isReady ? 'u-dummy-text-bar u-dummy-text-bar--double' : ''
          "
        ></div>
        <a
          class="c-card--link c-card-item--card-title"
          *ngIf="isReady"
          [routerLink]="['/entity/', card.slug ? card.slug : card.scriptId]"
          [ngClass]="
            !isReady ? 'u-dummy-text-bar u-dummy-text-bar--double' : ''
          "
        >
          {{ card.name }}
        </a>
      </div>
      <div
        class="c-card-item--script-text"
        [ngClass]="!isReady ? 'u-dummy-text-bar u-dummy-text-bar--link' : ''"
      >
        <ng-content select="[viewType]"></ng-content>
      </div>
    </div>
  `,
})
export class CardItemComponent {
  @Input() card: CardModel
  @Input() isReady = false

  isNew() {
    if (this.isReady) {
      if (typeof this.card.created_at === "undefined") {
        return false
      }
      const fortnightAgo = new Date(Date.now() - 12096e5).valueOf()
      const createdAt = Date.parse(this.card.created_at)

      return createdAt > fortnightAgo
    }
  }
}
