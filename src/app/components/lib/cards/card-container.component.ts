import { Component, Input } from "@angular/core"

@Component({
  selector: "app-card-container",
  template: `
    <div
      [ngClass]="columns"
      class="c-card-container l-display--flex l-grid--gutters l-flex--wrap"
    >
      <ng-content></ng-content>
    </div>
  `,
})
export class CardContainerComponent {
  @Input() columns = "l-grid--1of4"
}
