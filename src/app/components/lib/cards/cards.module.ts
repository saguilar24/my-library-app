import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { CardContainerComponent } from "./card-container.component"
import { CardItemComponent } from "./card-item.component"
import { CardCollectionItemComponent } from "./card-collection-item.component"
import { RouterModule } from "@angular/router"

@NgModule({
  imports: [CommonModule, RouterModule],
  exports: [
    CardContainerComponent,
    CardItemComponent,
    CardCollectionItemComponent,
  ],
  declarations: [
    CardContainerComponent,
    CardItemComponent,
    CardCollectionItemComponent,
  ],
})
export class CardsModule {}
