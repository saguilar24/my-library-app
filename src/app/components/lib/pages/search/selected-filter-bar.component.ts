import { Component, Input, OnInit } from "@angular/core"

@Component({
  selector: "app-selected-filter-bar",
  template: `
    <div class="c-search-filter-bar">
      <div class="c-search-filter-bar--text">
        {{ resultCount }} Results filtered by
        <app-filter-results-tag *ngFor="let tag of selectedTags">{{
          tag
        }}</app-filter-results-tag>
      </div>
    </div>
  `,
})
export class SelectedFilterBarComponent implements OnInit {
  @Input() resultCount
  @Input() selectedTags

  constructor() {}

  ngOnInit() {}
}
