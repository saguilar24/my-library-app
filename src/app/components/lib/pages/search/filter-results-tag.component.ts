import { Component, OnInit } from "@angular/core"

@Component({
  selector: "app-filter-results-tag",
  template: `
    <button class="c-filter-menu--results-tag">
      <ng-content></ng-content>
    </button>
  `,
})
export class FilterResultsTagComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
