import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { SelectedFilterBarComponent } from "./selected-filter-bar.component"
import { FilterResultsTagComponent } from "./filter-results-tag.component"
import { HeroSearchComponent } from "./hero-search.component"

import { ReactiveFormsModule } from "@angular/forms"

@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  exports: [
    FilterResultsTagComponent,
    SelectedFilterBarComponent,
    HeroSearchComponent,
  ],
  declarations: [
    FilterResultsTagComponent,
    SelectedFilterBarComponent,
    HeroSearchComponent,
  ],
})
export class ExploreModule {}
