import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { HeroHomepageComponent } from "./hero-homepage/hero-homepage.component"
import { SearchBoxPartialComponent } from "../../../partials/search-box-partial/search-box-partial.component"
import { ReactiveFormsModule } from "@angular/forms"

@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  exports: [HeroHomepageComponent, SearchBoxPartialComponent],
  declarations: [HeroHomepageComponent, SearchBoxPartialComponent],
})
export class HomeModule {}
