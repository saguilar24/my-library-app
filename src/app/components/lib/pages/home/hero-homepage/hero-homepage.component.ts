import { Component, EventEmitter, Input, Output } from "@angular/core"
import { HomeBannerModel } from "../../../../../models/home-banner.model"
import { FormControl, FormGroup } from "@angular/forms"
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject"

@Component({
  selector: "app-hero-homepage",
  templateUrl: "./hero-homepage.component.html",
})
export class HeroHomepageComponent {
  searchForm = new FormGroup({
    searchTerm: new FormControl(""),
  })

  @Input() searchTerms: BehaviorSubject<string>

  @Output() private _search: EventEmitter<string> = new EventEmitter<string>()

  private _textEntered: Boolean = false
  textEntered(): Boolean {
    return this._textEntered
  }

  get search(): EventEmitter<string> {
    return this._search
  }

  onSubmit() {
    const searchTerm = this.searchForm.get("searchTerm").value
    if (searchTerm !== "") {
      this._search.emit(searchTerm)
    }
  }

  checkSearchTextContent(event) {
    let inputValue = event.target.value
    if (inputValue === "") {
      this._textEntered = false
    } else {
      this._textEntered = true
    }
  }
}
