import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { HeroMyLibraryComponent } from "./hero-my-library.component"

@NgModule({
  imports: [CommonModule],
  exports: [HeroMyLibraryComponent],
  declarations: [HeroMyLibraryComponent],
})
export class MyLibraryModule {}
