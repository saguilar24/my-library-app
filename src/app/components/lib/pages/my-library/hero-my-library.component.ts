import { Component, Input } from "@angular/core"

@Component({
  selector: "app-hero-my-library",
  template: `
    <div class="c-hero c-hero--background">
      <div class="l-page--wrapper">
        <ng-content select="[back]"></ng-content>
        <h1 class="c-hero--title-bar">My Library</h1>
      </div>
    </div>
  `,
})
export class HeroMyLibraryComponent {
  @Input() page: any
}
