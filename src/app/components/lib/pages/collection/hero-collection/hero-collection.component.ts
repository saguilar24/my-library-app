import { Component, EventEmitter, Input, Output } from "@angular/core"
import { CardCollectionModel } from "../../../../../models/card-collection.model"

@Component({
  selector: "app-hero-collection",
  templateUrl: "./hero-collection.component.html",
})
export class HeroCollectionComponent {
  @Input() collection: CardCollectionModel
  @Output() share = new EventEmitter<boolean>()

  private _shared = false

  get shared(): boolean {
    return this._shared
  }

  handleShare() {
    this.share.emit(true)
    this._shared = true
    setTimeout(() => (this._shared = false), 1500)
  }
}
