import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { HeroCollectionComponent } from "./hero-collection/hero-collection.component"
import { IconsModule } from "../../icons/icons.module"
import { HeroPartnersAppsModule } from "../../../partials/hero-partners-apps/hero-partners-apps.module"

@NgModule({
  imports: [CommonModule, IconsModule, HeroPartnersAppsModule],
  exports: [HeroCollectionComponent],
  declarations: [HeroCollectionComponent],
})
export class CollectionModule {}
