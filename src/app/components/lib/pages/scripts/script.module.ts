import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { HeroScriptComponent } from "./hero-script/hero-script.component"
import { IconsModule } from "../../icons/icons.module"
import { HeroPartnersAppsModule } from "../../../partials/hero-partners-apps/hero-partners-apps.module"
import { EntityVotesComponent } from "../../../partials/entity-votes/entity-votes.component"
import { ModalsModule } from "../../modals/modals.module"
import { AnnouncementBannerPartialComponent } from "../../../partials/announcement-banner-partial/announcement-banner-partial.component"

@NgModule({
  imports: [CommonModule, IconsModule, HeroPartnersAppsModule, ModalsModule],
  exports: [HeroScriptComponent, AnnouncementBannerPartialComponent],
  declarations: [
    HeroScriptComponent,
    EntityVotesComponent,
    AnnouncementBannerPartialComponent,
  ],
})
export class ScriptModule {}
