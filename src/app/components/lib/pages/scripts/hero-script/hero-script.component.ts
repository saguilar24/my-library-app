import { Component, EventEmitter, Input, Output } from "@angular/core"
import { ScriptModel } from "../../../../../models/script.model"
import { DataModel } from "../../../../../models/data.model"

@Component({
  selector: "app-hero-script",
  templateUrl: "./hero-script.component.html",
})
export class HeroScriptComponent {
  @Input() script: DataModel<ScriptModel>
  @Output() share = new EventEmitter<boolean>()

  private _shared = false

  get shared(): boolean {
    return this._shared
  }

  handleShare() {
    this.share.emit(true)
    this._shared = true
    setTimeout(() => (this._shared = false), 1500)
  }
}
