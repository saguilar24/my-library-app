import { Component, Input, OnInit } from "@angular/core"
import { BehaviorSubject } from "rxjs"

@Component({
  selector: "app-sliding-drawer",
  template: `
    <div
      class="c-sliding-drawer c-sliding-drawer--left"
      [ngClass]="isOpen.getValue() ? 'is-open' : ''"
    >
      <ng-content></ng-content>
    </div>
  `,
})
export class SlidingDrawerComponent implements OnInit {
  @Input() isOpen: BehaviorSubject<boolean>

  constructor() {}

  ngOnInit() {}
}
