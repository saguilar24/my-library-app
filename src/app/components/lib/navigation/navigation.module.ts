import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { PageHeaderBarComponent } from "./page-header-bar.component"
import { FiltersModule } from "../filters/filters.module"
import { RouterModule } from "@angular/router"
import { SlidingDrawerComponent } from "./sliding-drawer.component"
import { TabMenuItemComponent } from "./tab-menu-item.component"
import { TabMenuListComponent } from "./tab-menu-list.component"
import { PaginationComponent } from "./pagination.component"
import { FooterComponent } from "./footer.component"
import { ModalsModule } from "../modals/modals.module"

@NgModule({
  imports: [CommonModule, RouterModule, FiltersModule, ModalsModule],
  exports: [
    PageHeaderBarComponent,
    SlidingDrawerComponent,
    TabMenuItemComponent,
    TabMenuListComponent,
    PaginationComponent,
    FooterComponent,
  ],
  declarations: [
    PageHeaderBarComponent,
    SlidingDrawerComponent,
    TabMenuItemComponent,
    TabMenuListComponent,
    PaginationComponent,
    FooterComponent,
  ],
})
export class NavigationModule {}
