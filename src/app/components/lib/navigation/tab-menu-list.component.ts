import { Component } from "@angular/core"

@Component({
  selector: "app-tab-menu-list",
  template: `
    <div class="c-tab-menu">
      <ul class="c-tab-menu--list">
        <ng-content></ng-content>
      </ul>
    </div>
  `,
})
export class TabMenuListComponent {}
