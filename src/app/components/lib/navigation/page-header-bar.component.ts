import { Component, OnInit } from "@angular/core"
import { AuthService } from "../../../services/auth/auth.service"
import { ProfileService } from "../../../services/profile/profile.service"

@Component({
  selector: "app-page-header-bar",
  templateUrl: "./page-header-bar.component.html",
})
export class PageHeaderBarComponent implements OnInit {
  isMobileMenuOpen = false

  constructor(public auth: AuthService, public profile: ProfileService) {}

  ngOnInit() {}

  mobileMenuToggle() {
    this.isMobileMenuOpen = !this.isMobileMenuOpen
  }
}
