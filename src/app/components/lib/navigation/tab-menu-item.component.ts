import { Component } from "@angular/core"

@Component({
  selector: "app-tab-menu-item",
  template: `
    <li class="c-tab-menu--item">
      <ng-content></ng-content>
    </li>
  `,
})
export class TabMenuItemComponent {}
