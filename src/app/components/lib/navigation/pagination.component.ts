import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core"
import { BehaviorSubject } from "rxjs"

@Component({
  selector: "app-pagination",
  template: `
    <div class="c-pagination-holder l-display--flex">
      <button
        (click)="firstPage()"
        *ngIf="hasPrevPage()"
        class="c-button c-button--pagination"
        style="uppercase;"
      >
        First
      </button>
      <button
        (click)="prevPage()"
        *ngIf="hasPrevPage()"
        class="c-button c-button--pagination"
      >
        Previous
      </button>
      <button
        (click)="specificPage(button)"
        *ngFor="let button of pageButtonsData"
        [disabled]="button == '...'"
        [ngClass]="button == meta.page ? 'current' : ''"
        class="c-button c-button--pagination"
      >
        {{ button }}
      </button>
      <button
        (click)="nextPage()"
        *ngIf="hasNextPage()"
        class="c-button c-button--pagination"
      >
        Next
      </button>
      <button
        (click)="lastPage()"
        *ngIf="hasNextPage()"
        class="c-button c-button--pagination"
      >
        Last
      </button>
    </div>
  `,
})
export class PaginationComponent implements OnInit {
  @Input() pagedData$: BehaviorSubject<{
    data: any
    meta: { page: number; limit: number; total: number }
  }>
  @Input() numberOfButtons = 5
  @Output() handlePageChange = new EventEmitter()

  private _data = null
  private _meta = { page: null, limit: null, total: null }

  get meta() {
    return this._meta
  }

  get totalPages() {
    return Math.ceil(this._meta.total / this._meta.limit)
  }

  get pageButtonsData() {
    const buttonData = []
    let start = this._meta.page - Math.floor(this.numberOfButtons / 2)
    let end = this._meta.page + Math.floor(this.numberOfButtons / 2)
    if (start < 1) {
      end = end + Math.abs(1 - start)
      start = 1
    }
    if (end > this.totalPages) {
      const newStart = start - Math.abs(this.totalPages - end)
      start = newStart >= 1 ? newStart : 1
      end = this.totalPages
    }
    let i = start
    if (i > 1) {
      buttonData.push("...")
    }
    for (; i <= end; i++) {
      buttonData.push(i)
    }
    if (end < this.totalPages) {
      buttonData.push("...")
    }

    return buttonData
  }

  ngOnInit(): void {
    this.pagedData$.subscribe((d) => {
      this._meta = d.meta
      this._data = d.data
    })
  }

  prevPage() {
    if (this.hasPrevPage()) {
      this.handlePageChange.emit(this._meta.page - 1)
    }
  }

  nextPage() {
    if (this.hasNextPage()) {
      this.handlePageChange.emit(this._meta.page + 1)
    }
  }

  specificPage(page) {
    this.handlePageChange.emit(page)
  }

  hasNextPage() {
    return this._meta.page * this._meta.limit < this._meta.total
  }

  hasPrevPage() {
    return this._meta.page > 1
  }

  firstPage() {
    this.handlePageChange.emit(1)
  }

  lastPage() {
    this.handlePageChange.emit(this.totalPages)
  }
}
