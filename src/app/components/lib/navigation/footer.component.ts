import { Component, OnInit } from "@angular/core"
import { Router } from "@angular/router"
import { BehaviorSubject } from "rxjs"
import { ProfileService } from "../../../services/profile/profile.service"
import { UserService } from "../../../services/user/user.service"

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
})
export class FooterComponent implements OnInit {
  constructor(
    public profile: ProfileService,
    private _userService: UserService,
    private router: Router
  ) {}

  private _showNewFeatureModal = new BehaviorSubject<boolean>(false)

  get showNewFeatureModal(): BehaviorSubject<boolean> {
    return this._showNewFeatureModal
  }

  ngOnInit() {
    this.handleShowNewFeatureModal()
  }

  handleShowNewFeatureModal() {
    if (
      this.profile.isAuthenticatedAndVerified() &&
      !this.hasDismissedModal() &&
      !this.excludedPage()
    ) {
      this._showNewFeatureModal.next(true)
    }
  }

  handleEditProfileNewFeatureModal() {
    this._userService
      .dismissModal(this.profile.getDismissedModals(), "Profile")
      .subscribe(
        (response) => {
          this._showNewFeatureModal.next(false)
          return this.router.navigate(["manage-account"])
        },
        (error) => {}
      )
  }

  handleDismissNewFeatureModal() {
    this._userService
      .dismissModal(this.profile.getDismissedModals(), "Profile")
      .subscribe(
        (response) => {},
        (error) => {}
      )

    this._showNewFeatureModal.next(false)
  }

  private hasDismissedModal() {
    return this.profile.getDismissedModals().includes("Profile")
  }

  private excludedPage() {
    return (
      global.location.pathname === "/search" ||
      global.location.pathname === "/manage-account"
    )
  }
}
