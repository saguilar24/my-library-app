import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { RouterModule } from "@angular/router"
import { NavigationModule } from "../../lib/navigation/navigation.module"
import { HomeModule } from "../../lib/pages/home/home.module"
import { LayoutModule } from "../../lib/layout/layout.module"
import { TypographyModule } from "../../lib/typography/typography.module"
import { ProfilePageComponent } from "./profile-page.component"
import { ProfilePageHeroPartialComponent } from "../../partials/profile-page-hero-partial/profile-page-hero-partial.component"
import { ProfileCodePageComponent } from "../profile-code-page/profile-code-page.component"
import { EntityCardPartialModule } from "../../partials/entity-card-partial/entity-card-partial.module"
import { FormsModule } from "@angular/forms"
import { CardsModule } from "../../lib/cards/cards.module"

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NavigationModule,
    LayoutModule,
    HomeModule,
    TypographyModule,
    FormsModule,
    EntityCardPartialModule,
    CardsModule,
  ],
  exports: [ProfilePageComponent],
  declarations: [
    ProfilePageComponent,
    ProfileCodePageComponent,
    ProfilePageHeroPartialComponent,
  ],
})
export class ProfilePageModule {}
