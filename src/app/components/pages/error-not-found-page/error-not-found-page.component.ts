import { Component, Inject, OnInit, Optional } from "@angular/core"
import { Meta, Title } from "@angular/platform-browser"
import { ApplicationService } from "../../../services/application/application.service"
import { RESPONSE } from "@nguniversal/express-engine/tokens"

@Component({
  selector: "app-error-not-found-page",
  templateUrl: "./error-not-found-page.component.html",
})
export class ErrorNotFoundPageComponent implements OnInit {
  constructor(
    @Optional() @Inject(RESPONSE) private response,
    private _pageTitle: Title,
    private _meta: Meta,
    private _applicationService: ApplicationService
  ) {}

  ngOnInit() {
    if (this._applicationService.isRunningOnServer()) {
      this.response.status(404)
    }
    this.setPageMetaData()
  }

  private setPageMetaData(): void {
    this._pageTitle.setTitle("404 Page Not Found - Adaptavist Library")
  }
}
