import { Component, OnDestroy, OnInit } from "@angular/core"
import { SearchService } from "../../../services/search/search.service"
import { FilterGroupCollection } from "../../../services/search/filter-group.collection"
import { NavigationEnd, Router } from "@angular/router"
import { EntityModel } from "../../../models/entity.model"
import { SearchResultsModel } from "../../../models/search-results.model"
import { EntityService } from "../../../services/entity/entity.service"
import { BehaviorSubject, combineLatest, Subscription, timer } from "rxjs"
import { makeStateKey, Meta, Title } from "@angular/platform-browser"
import { FilterMenuService } from "../../../services/search/filter-menu.service"
import { debounce, distinctUntilChanged, map, skip, tap } from "rxjs/operators"
import { DataModel } from "../../../models/data.model"
import { Segment } from "../../../services/segment/segment.service"
import { CardModel } from "../../../models/card.model"
import { PagedDataModel } from "../../../models/paged-data.model"
//import { elasticSearchEntityToCardTransformer } from "../../../transformers/entity-transformers"
import { RequestService } from "../../../services/request/request.service"
import makeDataModel from "../../../utilities/make-data-model"
import { UserService } from "../../../services/user/user.service"
import { ProfileService } from "../../../services/profile/profile.service"
import { AuthService } from "../../../services/auth/auth.service"

@Component({
  selector: "app-search-results-page",
  templateUrl: "./search-results-page.component.html",
  providers: [SearchService],
})
export class SearchResultsPageComponent implements OnInit, OnDestroy {
  private _currentPage$ = new BehaviorSubject<number>(1)
  private _resultLimit$ = new BehaviorSubject<number>(12)
  private _filterParam$ = new BehaviorSubject<string>("")
  private _subscriptions = []

  constructor(
    private _filterMenu: FilterMenuService,
    private _search: SearchService,
    private _request: RequestService,
    private _router: Router,
    private _entityService: EntityService,
    private _userService: UserService,
    private _profileService: ProfileService,
    private _pageTitle: Title,
    private _meta: Meta,
    private _authService: AuthService
  ) {}

  // TODO: Check whether this component is required in the new infrastructure.
  get profileService(): ProfileService {
    return this._profileService
  }

  get menuIsReady() {
    return this._filterMenu.isReady
  }

  get filtersGroups(): FilterGroupCollection[] {
    return this.filters.filterGroups
  }

  get topFilterGroups(): FilterGroupCollection[] {
    return this.filters.topFilterGroups
  }

  get moreFilterGroups(): FilterGroupCollection[] {
    return this.filters.moreFilterGroups
  }

  get resultCount() {
    return this._pagedData$.getValue().meta.total
  }

  get page() {
    return this._pagedData$.getValue().meta.page
  }

  get limit() {
    return this._pagedData$.getValue().meta.limit
  }

  get totalPages() {
    return Math.ceil(this.resultCount / this.limit)
  }

  get search() {
    return this._search
  }

  get filters() {
    return this._filterMenu.filterService
  }

  get showMoreFilters(): BehaviorSubject<boolean> {
    return this._filterMenu.showMoreFilters
  }

  private _isMobileFilterMenuOpen: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(false)

  get isMobileFilterMenuOpen(): BehaviorSubject<boolean> {
    return this._isMobileFilterMenuOpen
  }

  private _loading = new BehaviorSubject<boolean>(true)

  get loading(): boolean {
    return this._loading.getValue()
  }

  private _entities: EntityModel[]

  get entities(): EntityModel[] {
    return this._entities
  }

  private _pagedData$ = new BehaviorSubject<PagedDataModel>({
    data: [],
    meta: {
      page: 1,
      total: 0,
      limit: 12,
    },
  })

  get pagedData$(): BehaviorSubject<PagedDataModel> {
    return this._pagedData$
  }

  private _cards: DataModel<CardModel>[] = Array(12).fill(
    makeDataModel(new CardModel())
  )

  get cards(): DataModel<CardModel>[] {
    return this._cards
  }

  private _showSearchAlertModal = new BehaviorSubject<boolean>(false)

  get showSearchAlertModal(): BehaviorSubject<boolean> {
    return this._showSearchAlertModal
  }

  private _showNewFeatureModal = new BehaviorSubject<boolean>(false)

  get showNewFeatureModal(): BehaviorSubject<boolean> {
    return this._showNewFeatureModal
  }

  private _showSearchAlertConfirmation = new BehaviorSubject<boolean>(false)

  get showSearchAlertConfirmation(): boolean {
    return this._showSearchAlertConfirmation.getValue()
  }

  ngOnDestroy() {
    this.filters.resetFilters()
    this._subscriptions.map((s: Subscription) => s.unsubscribe())
  }

  ngOnInit() {
    this._filterMenu.isReady = false
    const params = this.getParamsFromQueryString(
      this.getUrlParamsString(this._router.url)
    )
    this.subscribeToSearchTermAndFilterChanges()
    this.setCurrentPageFromParams(params)
    this.initFilterMenu(params)
    this.subscribeToUrlChangesForFetchingResults()
    //this.subscribeToPagedData()
    this.setPageMetaData()
    this.handleShowNewFeatureModal()
  }

  handleFilterToggle({ filterName, slug }) {
    this.filters.getFilterGroup(filterName).toggleFilter(slug)
    this._filterParam$.next(this.filters.getQueryStringWithNames())
    this._currentPage$.next(1)
  }

  handlePageChange(nextPage) {
    this._pagedData$.next({
      ...this._pagedData$.getValue(),
      meta: { ...this._pagedData$.getValue().meta, page: nextPage },
    })
    this._currentPage$.next(nextPage)
    this.updatePrettyUrl()
  }

  handleReset() {
    this._filterMenu.filterService.clearFilterGroups()
    this._filterParam$.next(this.filters.getQueryStringWithNames())
    this._search.term$.next("")
    this._currentPage$.next(1)
  }

  handleShowSearchAlertModal() {
    this._showSearchAlertModal.next(true)
  }

  handleShowNewFeatureModal() {
    if (
      this.profileService.isAuthenticatedAndVerified() &&
      !this.hasDismissedModal()
    ) {
      this._showNewFeatureModal.next(true)
    }
  }

  handleCreateSearchAlert() {
    this._userService
      .postSearchAlert(
        this.search.term$.getValue(),
        this.filters.getSelectedFilterNames()
      )
      .subscribe(
        (response) => {
          this._showSearchAlertModal.next(false)
          this._showSearchAlertConfirmation.next(true)
        },
        (error) => {
          // Todo display error message to user
          console.log(error)
        }
      )
  }

  handleLoggedOutSavedSearch() {
    this._authService.login(
      global.location.pathname + global.location.search + "&search_alert=true"
    )
  }

  public closeSearchAlertConfirmation() {
    this._showSearchAlertConfirmation.next(false)
  }

  handleDismissNewFeatureModal() {
    this._userService
      .dismissModal(this._profileService.getDismissedModals(), "Search Alert")
      .subscribe(
        (response) => {},
        (error) => {}
      )

    this._showNewFeatureModal.next(false)
  }

  // This method does not make sense in the new infrastructure because the results page will be obtained from Algolia
  // and not from Elastic Search.

  /*private subscribeToPagedData() {
    this._subscriptions.push(
      this.pagedData$.subscribe((response) => {
        this._cards = response.data.map((e) => ({
          isReady: true,
          data: elasticSearchEntityToCardTransformer(e),
        }))
      })
    )
  }*/

  private subscribeToUrlChangesForFetchingResults() {
    this._subscriptions.push(
      this._router.events
        .pipe(
          debounce(() => timer(50)),
          distinctUntilChanged()
        )
        .subscribe((val) => {
          if (val instanceof NavigationEnd) {
            this.performSearchFromUrlParams()
          }
        })
    )
  }

  private initFilterMenu(params) {
    this._subscriptions.push(
      this._filterMenu.init().subscribe(() => {
        this._search.initTerm(params)
        this.filters.selectFiltersFromQueryParams(params)
        this.performSearchFromUrlParams()
      })
    )
  }

  private setCurrentPageFromParams(params) {
    const page = params.find((p) => p.paramName === "page")
    const current_page = page ? +page.value : 1
    this.setCurrentPage(current_page)
  }

  private subscribeToSearchTermAndFilterChanges() {
    this._subscriptions.push(
      combineLatest(
        this._filterParam$,
        this._search.term$.pipe(
          skip(1),
          debounce((str) => (str === "" ? timer(0) : timer(300))),
          distinctUntilChanged(),
          tap(() => this.setCurrentPage(1))
        ),
        this._currentPage$.pipe(skip(1), distinctUntilChanged())
      )
        .pipe(
          skip(1),
          debounce(() => timer(50))
        )
        .subscribe(() => this.updatePrettyUrl())
    )
  }

  private performSearchFromUrlParams() {
    this._showSearchAlertConfirmation.next(false)

    this.fetchSearchResults(this.getQueryString())
  }

  private getQueryString() {
    const filterParams = this.filters.getQueryStringWithNames()
    const searchParam = this._search.getSearchTermQueryString()
    const page = this._currentPage$.getValue()
    const limit = this._resultLimit$.getValue()
    let endpoint = `/v2/search?page=${page}&limit=${limit}`
    endpoint = this.buildFilterString(filterParams, searchParam, endpoint)
    return endpoint
  }

  private buildFilterString(
    filterParams: string,
    searchParam: string,
    endpoint: string
  ) {
    if (filterParams && searchParam) {
      endpoint += `&${filterParams}&${searchParam}`
    } else if (filterParams) {
      endpoint += `&${filterParams}`
    } else if (searchParam) {
      endpoint += `&${searchParam}`
    }
    return endpoint
  }

  private setCurrentPage(current_page) {
    this._currentPage$.next(current_page)
  }

  private updatePrettyUrl() {
    this._router.navigateByUrl(
      this._router.createUrlTree(["search"], {
        queryParams: this.getQueryParams(),
      })
    )
  }

  private fetchSearchResults(query) {
    this._loading.next(true)

    this._cards = this._cards.map(() => ({
      isReady: false,
      data: new CardModel(),
    }))

    this._request
      .getSSR<SearchResultsModel>(makeStateKey(query), query)
      .pipe(
        tap(() => this._loading.next(false)),
        tap((response) => this.trackSearch(response)),
        tap(() => this.setPageMetaData()),
        tap((response) => this._filterMenu.updateCounts(response)),
        map((response) => this.toPagedData(response))
      )
      .subscribe((data) => this.pagedData$.next(data))
  }

  private toPagedData(response): PagedDataModel {
    return {
      data: response.data,
      meta: {
        page: response.current_page,
        limit: response.per_page,
        total: response.total,
      },
    }
  }

  private getParamsFromQueryString(
    url
  ): { paramName: string; value: string }[] {
    return url.split("&").map((p) => {
      const [paramName, value] = p.split("=")
      return { paramName, value }
    })
  }

  private getUrlParamsString(url): string {
    return url.split("?")[1] || ""
  }

  private getQueryParams() {
    return {
      page: this._currentPage$.getValue(),
      ...this.filters.getSelectedFilterSlugs(),
      ...this._search.getParams(),
    }
  }

  private setPageMetaData(): void {
    const filters = this.filtersGroups.reduce(
      (arr, fg) => [
        ...arr,
        ...fg.getSelected().map((f) => ({
          ...f,
          filterName: fg.name,
        })),
      ],
      []
    )

    if (this._search.term$.getValue()) {
      filters.unshift({ name: this._search.term$.getValue() })
    }

    const selectedFilters = filters
      .map(
        (filter) => filter.name.charAt(0).toUpperCase() + filter.name.slice(1)
      )
      .join(", ")
      .replace(/,(?!.*,)/gim, " and")

    if (selectedFilters.length) {
      this._pageTitle.setTitle(
        `Search results for ${selectedFilters} - Adaptavist Library`
      )
      this._meta.updateTag({
        name: "description",
        content: `Search across the Adaptavist Library for ${selectedFilters} code snippets, workflows and collections.`,
      })
    } else {
      this._pageTitle.setTitle(`Search the Adaptavist Library`)
      this._meta.updateTag({
        name: "description",
        content: `Search across the Adaptavist Library for Atlassian Apps code snippets, workflows and collections.`,
      })
    }

    // Show the modal if,
    // the user is logged in and verified,
    // `search_alert=true` param exists in the URL, and
    // a search term or filter has been applied
    if (
      this._profileService.isAuthenticatedAndVerified() &&
      global.location.search.indexOf("search_alert=true") > -1 &&
      (this.search.hasTerm() || this.filters.hasAnySelectedFilters())
    ) {
      this.handleShowSearchAlertModal()
    }
  }

  private trackSearch(response) {
    Segment.track("Search", {
      searchTerm: this._search.term$.getValue(),
      // Todo: FilterGroup 'Types' should be named 'Type'.
      //       Then we can use `filters: this.filters.getSelectedFiltersForSegment()` for segment tracking
      filters: {
        discover: this.filters.getSelectedNamesForFilterGroup("Discover"),
        apps: this.filters.getSelectedNamesForFilterGroup("Apps"),
        tags: this.filters.getSelectedNamesForFilterGroup("Tags"),
        features: this.filters.getSelectedNamesForFilterGroup("Features"),
        type: this.filters.getSelectedNamesForFilterGroup("Types"),
        products: this.filters.getSelectedNamesForFilterGroup("Products"),
        platforms: this.filters.getSelectedNamesForFilterGroup("Platforms"),
        statuses: this.filters.getSelectedNamesForFilterGroup("Status"),
      },
      currentPage: response.current_page,
      totalResults: response.total,
    })
  }

  private hasDismissedModal() {
    return this._profileService.getDismissedModals().includes("Search Alert")
  }
}
