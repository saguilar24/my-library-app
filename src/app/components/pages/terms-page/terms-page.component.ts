import { Component, OnInit } from "@angular/core"
import { Meta, Title } from "@angular/platform-browser"

@Component({
  selector: "app-terms-page",
  templateUrl: "./terms-page.component.html",
})
export class TermsPageComponent implements OnInit {
  constructor(private _pageTitle: Title, private _meta: Meta) {}

  ngOnInit() {
    this.setPageMetaData()
  }

  private setPageMetaData(): void {
    this._pageTitle.setTitle("Terms and Conditions - Adaptavist Library")
    this._meta.updateTag({
      name: "description",
      content: "Adaptavist Library - Terms and Conditions",
    })
  }
}
