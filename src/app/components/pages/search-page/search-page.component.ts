import { Component } from "@angular/core"
import algoliasearch from "algoliasearch/lite"
import { environment } from "../../../../environments/environment"
import {history} from "instantsearch.js/es/lib/routers";
import {FilterMenuService} from "../../../services/search/filter-menu.service";
import {EntityService} from "../../../services/entity/entity.service";
import {CollectionService} from "../../../services/collection/collection.service";
import {RecentEntitiesService} from "../../../services/entity/recent-entities.service";
import {Router} from "@angular/router";
import {makeStateKey, Meta, Title, TransferState} from "@angular/platform-browser";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {createSSRSearchClient} from "angular-instantsearch";

const searchClient = algoliasearch(
  environment.applicationID,
  environment.searchOnlyApiKey
)

@Component({
  selector: "app-search-page",
  templateUrl: "./search-page.component.html",
  styleUrls: ["./search-page.component.scss"],
})
export class SearchPageComponent {

  config: any;

  constructor(
      private httpClient: HttpClient,
      private transferState: TransferState,
  ) {
    this.config = {
      indexName: environment.indexName,
      searchClient: createSSRSearchClient({
        appId: environment.applicationID,
        apiKey: environment.searchOnlyApiKey,
        makeStateKey,
        HttpHeaders,
        transferState: this.transferState,
        httpClient: this.httpClient,
      }),
      routing: {
        router: history({
          getLocation() {
            if (typeof window === 'undefined') {
              const url = environment.appUrl; // retrieved from the server context
              return new URL(url) as unknown as Location;
            }

            return window.location;
          },
        })
      }
    }
  }

  public show: boolean = false
  public buttonName: any = "+ More filters"

  refinementToggle() {
    this.show = !this.show

    // Button text change
    if (this.show) this.buttonName = "Close filters"
    else this.buttonName = "+ More filters"
  }
}
