import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { LayoutModule } from "../../lib/layout/layout.module"
import { HomeModule } from "../../lib/pages/home/home.module"
import { NgAisModule } from "angular-instantsearch"
import { SearchPageComponent } from "./search-page.component"
import { BrowserModule } from "@angular/platform-browser"
import { StatsPartialComponent } from "../../partials/stats-partial/stats-partial.component"
import { PaginationPartialComponent } from "../../partials/pagination-partial/pagination-partial.component"
import { CurrentRefinementsPartial } from "../../partials/current-refinements-partial/current-refinements-partial.component"

@NgModule({
  declarations: [
    SearchPageComponent,
    StatsPartialComponent,
    CurrentRefinementsPartial,
    PaginationPartialComponent,
  ],
  imports: [
    CommonModule,
    NgAisModule.forRoot(),
    BrowserModule,
    LayoutModule,
    HomeModule,
  ],
  exports: [
    SearchPageComponent,
    StatsPartialComponent,
    CurrentRefinementsPartial,
    PaginationPartialComponent,
  ],
})
export class SearchPageModule {}
