import { Component, OnInit } from "@angular/core"
import { Router } from "@angular/router"
import {makeStateKey, Meta, Title, TransferState} from "@angular/platform-browser"
import { BehaviorSubject } from "rxjs"
import { EntityService } from "../../../services/entity/entity.service"
import { CollectionService } from "../../../services/collection/collection.service"
import { FilterMenuService } from "../../../services/search/filter-menu.service"
import { FilterGroupCollection } from "../../../services/search/filter-group.collection"
import { DataModel } from "../../../models/data.model"
import { CardModel } from "../../../models/card.model"
import { CardCollectionModel } from "../../../models/card-collection.model"
import {
  entityToCardTransformerFromAlgolia,
  entityToCardTransformerFromGraphCms,
} from "../../../transformers/entity-transformers"
import { collectionFromGraphCmsToCardCollectionTransformer } from "../../../transformers/collection-transformers"
import { RecentEntitiesService } from "../../../services/entity/recent-entities.service"
import algoliasearch from "algoliasearch/lite"

import makeDataModel from "../../../utilities/make-data-model"
import { environment } from "../../../../environments/environment"
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {createSSRSearchClient} from "angular-instantsearch";

const searchClient = algoliasearch(
  environment.applicationID,
  environment.searchOnlyApiKey
)

@Component({
  selector: "app-home-page",
  templateUrl: "./home-page.component.html",
})
export class HomePageComponent implements OnInit {
  config: any;

  constructor(
    private _filterMenu: FilterMenuService,
    private _entityService: EntityService,
    private _collectionService: CollectionService,
    private _recentEntities: RecentEntitiesService,
    private _router: Router,
    private _pageTitle: Title,
    private _meta: Meta,
    private httpClient: HttpClient,
    private transferState: TransferState,
  ) {
    this.config = {
      searchClient: createSSRSearchClient({
        appId: environment.applicationID,
        apiKey: environment.searchOnlyApiKey,
        makeStateKey,
        HttpHeaders,
        transferState: this.transferState,
        httpClient: this.httpClient,
      }),
    }
  }

  private _isMobileFilterMenuOpen: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(false)

  get isMobileFilterMenuOpen(): BehaviorSubject<boolean> {
    return this._isMobileFilterMenuOpen
  }

  get showMoreFilters(): BehaviorSubject<boolean> {
    return this._filterMenu.showMoreFilters
  }

  get menuIsReady() {
    return this._filterMenu.isReady
  }

  private _popularScriptRunnerCards = new BehaviorSubject<
    DataModel<CardModel>[]
  >(Array(3).fill(makeDataModel(new CardModel())))

  get popularScriptRunnerCards(): DataModel<CardModel>[] {
    return this._popularScriptRunnerCards.getValue()
  }

  private _newestScriptRunnerCards = new BehaviorSubject<
    DataModel<CardModel>[]
  >(Array(3).fill(makeDataModel(new CardModel())))

  get newestScriptRunnerCards(): DataModel<CardModel>[] {
    return this._newestScriptRunnerCards.getValue()
  }

  private _newestJiraCloudCards = new BehaviorSubject<DataModel<CardModel>[]>(
    Array(3).fill(makeDataModel(new CardModel()))
  )

  get newestJiraCloudCards(): DataModel<CardModel>[] {
    return this._newestJiraCloudCards.getValue()
  }

  private _newestAdaptavistCollectionCards = new BehaviorSubject<
    DataModel<CardCollectionModel>[]
  >(Array(3).fill(makeDataModel(new CardCollectionModel())))

  get newestAdaptavistCollectionCards(): DataModel<CardCollectionModel>[] {
    return this._newestAdaptavistCollectionCards.getValue()
  }

  private _trendingPartnerCollectionCards = new BehaviorSubject<
    DataModel<CardCollectionModel>[]
  >(Array(3).fill(makeDataModel(new CardCollectionModel())))

  get trendingPartnerCollectionCards(): DataModel<CardCollectionModel>[] {
    return this._trendingPartnerCollectionCards.getValue()
  }

  get topFilterGroups(): FilterGroupCollection[] {
    return this._filterMenu.filterService.topFilterGroups
  }

  get moreFilterGroups(): FilterGroupCollection[] {
    return this._filterMenu.filterService.moreFilterGroups
  }

  get filters() {
    return this._filterMenu.filterService
  }

  get recentEntities(): DataModel<CardModel>[] {
    return this._recentEntities.getN(3).map((c) => ({ isReady: true, data: c }))
  }

  ngOnInit() {
    this._filterMenu.isReady = false
    //this._filterMenu.init().subscribe()
    this.getNewestAdaptavistCollections()
    this.getTrendingPartnerCollections()
    this.getPopularScriptRunnerEntities()
    this.getNewestScriptRunnerEntities()
    this.getNewestJiraCloudEntities()
    this.setPageMetaData()
  }

  handleSearch($event) {
    if (!$event) {
      return
    }
    this._router.navigateByUrl(
      this._router.createUrlTree(["search"], { queryParams: { term: $event } })
    )
  }

  private getNewestAdaptavistCollections(): void {
    this._collectionService
      .getNewestAdaptavistCollectionsFromGraphCms(3)
      .then((result) => {
        this._newestAdaptavistCollectionCards.next(
          result.collections.map((c) =>
            makeDataModel(
              collectionFromGraphCmsToCardCollectionTransformer(c),
              true
            )
          )
        )
      })
  }

  private getTrendingPartnerCollections(): void {
    this._collectionService
      .getTrendingPartnerCollectionsFromGraphCms(3)
      .then((result) => {
        this._trendingPartnerCollectionCards.next(
          result.collections.map((c) =>
            makeDataModel(
              collectionFromGraphCmsToCardCollectionTransformer(c),
              true
            )
          )
        )
      })
  }

  private getPopularScriptRunnerEntities(): void {
    this.config.searchClient
      .initIndex("library-content_copy_count")
      .search("", {
        attributesToRetrieve: [
          "script_id",
          "script_name",
          "app_pretty_names",
          "app_names",
          "created_at",
          "type",
        ],
        hitsPerPage: 3,
        facetFilters: [
          [
            "app_shortcodes:script-runner-jira",
            "app_shortcodes:script-runner-bitbucket",
            "app_shortcodes:script-runner-confluence",
            "app_shortcodes:script-runner-bamboo",
          ],
        ],
      })
      .then((result) => {
        this._popularScriptRunnerCards.next(
          // @ts-ignore
          result.hits.map((script) =>
            makeDataModel(entityToCardTransformerFromAlgolia(script), true)
          )
        )
      })
  }

  private getNewestScriptRunnerEntities(): void {
    this.config.searchClient
      .initIndex("library-content_created_at")
      .search("", {
        attributesToRetrieve: [
          "script_id",
          "script_name",
          "app_pretty_names",
          "app_names",
          "created_at",
          "type",
        ],
        hitsPerPage: 3,
        facetFilters: [["app_shortcodes:script-runner-jira"]],
      })
      .then((result) => {
        this._newestScriptRunnerCards.next(
          // @ts-ignore
          result.hits.map((script) =>
            makeDataModel(entityToCardTransformerFromAlgolia(script), true)
          )
        )
      })
  }

  private getNewestJiraCloudEntities(): void {
    this.config.searchClient
      .initIndex("library-content_created_at")
      .search("", {
        attributesToRetrieve: [
          "script_id",
          "script_name",
          "app_pretty_names",
          "app_names",
          "created_at",
          "type",
        ],
        hitsPerPage: 3,
        facetFilters: [["product_names:Jira"], ["platforms:cloud"]],
      })
      .then((result) => {
        this._newestJiraCloudCards.next(
          // @ts-ignore
          result.hits.map((script) =>
            makeDataModel(entityToCardTransformerFromAlgolia(script), true)
          )
        )
      })
  }

  private setPageMetaData(): void {
    this._pageTitle.setTitle("Adaptavist Library")
    this._meta.updateTag({
      name: "description",
      content:
        "The Adaptavist Library is a curated collection of tailor-made scripts designed to enable Atlassian users to leverage Adaptavist products to create automation and perform complex customisations.",
    })
  }
}
