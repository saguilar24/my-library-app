import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { RouterModule } from "@angular/router"
import { HomePageComponent } from "./home-page.component"
import { SideBarPartialModule } from "../../partials/side-bar-partial/side-bar-partial.module"
import { EntityCardPartialModule } from "../../partials/entity-card-partial/entity-card-partial.module"
import { NavigationModule } from "../../lib/navigation/navigation.module"
import { HomeModule } from "../../lib/pages/home/home.module"
import { FiltersModule } from "../../lib/filters/filters.module"
import { LayoutModule } from "../../lib/layout/layout.module"
import { CardsModule } from "../../lib/cards/cards.module"
import { TypographyModule } from "../../lib/typography/typography.module"

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NavigationModule,
    LayoutModule,
    CardsModule,
    HomeModule,
    SideBarPartialModule,
    TypographyModule,
    FiltersModule,
    EntityCardPartialModule,
  ],
  exports: [HomePageComponent],
  declarations: [HomePageComponent],
})
export class HomePageModule {}
