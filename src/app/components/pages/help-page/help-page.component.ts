import { Component, OnInit } from "@angular/core"
import { Meta, Title } from "@angular/platform-browser"

@Component({
  selector: "app-help-page",
  templateUrl: "./help-page.component.html",
})
export class HelpPageComponent implements OnInit {
  constructor(private _pageTitle: Title, private _meta: Meta) {}

  ngOnInit() {
    this.setPageMetaData()
  }

  private setPageMetaData(): void {
    this._pageTitle.setTitle("FAQ - Adaptavist Library")
    this._meta.updateTag({
      name: "description",
      content: "What is the Adaptavist Library?",
    })
  }
}
