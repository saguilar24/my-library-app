import { Component, OnInit } from "@angular/core"
import { Location } from "@angular/common"
import { ActivatedRoute } from "@angular/router"
import { CollectionService } from "../../../services/collection/collection.service"
import { EntityService } from "../../../services/entity/entity.service"
import { Meta, Title, TransferState } from "@angular/platform-browser"
import { tap } from "rxjs/operators"
import { BehaviorSubject } from "rxjs"
import { CopyTextService } from "../../../services/copy/copy-text.service"
import { AuthService } from "../../../services/auth/auth.service"
import { ProfileService } from "../../../services/profile/profile.service"
import { DataModel } from "../../../models/data.model"
import { Segment } from "../../../services/segment/segment.service"
import { CardCollectionModel } from "../../../models/card-collection.model"
import { CardModel } from "../../../models/card.model"
import { ApplicationService } from "../../../services/application/application.service"
import { collectionFromGraphCmsToCardCollectionTransformer } from "../../../transformers/collection-transformers"
import { entityToCardTransformerFromGraphCms } from "../../../transformers/entity-transformers"
import { UserService } from "../../../services/user/user.service"
import makeDataModel from "../../../utilities/make-data-model"
import { CollectionModel } from "../../../models/collection.model"

@Component({
  selector: "app-collection-page",
  templateUrl: "./collection-page.component.html",
  providers: [CopyTextService],
})
export class CollectionPageComponent implements OnInit {
  constructor(
    private _applicationService: ApplicationService,
    private _location: Location,
    private _route: ActivatedRoute,
    private _collectionService: CollectionService,
    private _entityService: EntityService,
    private _copy: CopyTextService,
    private _pageTitle: Title,
    private _meta: Meta,
    public _profile: ProfileService,
    public auth: AuthService,
    public profile: ProfileService,
    private _state: TransferState,
    private _userService: UserService
  ) {}

  private _isUpdatingMyLibrary = new BehaviorSubject<boolean>(false)

  get isUpdatingMyLibrary(): BehaviorSubject<boolean> {
    return this._isUpdatingMyLibrary
  }

  private _isInMyLibrary = new BehaviorSubject<boolean>(false)

  get isInMyLibrary(): BehaviorSubject<boolean> {
    return this._isInMyLibrary
  }

  private _collection = makeDataModel(new CardCollectionModel())

  get collection(): DataModel<CollectionModel> {
    // @ts-ignore
    return this._collection
  }

  get collectionData(): CollectionModel {
    return this.hasCollectionData ? this.collection.data : null
  }

  get hasCollectionData(): boolean {
    return this.collection.isReady
  }

  get collectionCards(): DataModel<CardModel>[] {
    return this.collection.data.entities.map((e) => ({
      isReady: true,
      data: e,
    }))
  }

  private _moreCollectionCards: DataModel<CardCollectionModel>[] = []

  get moreCollectionCards(): DataModel<CardCollectionModel>[] {
    return this._moreCollectionCards
  }

  get location(): Location {
    return this._location
  }

  ngOnInit() {
    this._route.params.subscribe(() => {
      const slug = this._route.snapshot.paramMap.get("slug")
      this.resetCollection()
      this.resetIsInMyLibrary()
      this._collectionService.getCollectionItemFromGraphCms(slug).then(
        (result) => {
          const collection = result.collection
          this._collection = makeDataModel(
            {
              ...this._collectionService.graphCmsCollectionToCollectionModel(
                collection
              ),
              // @ts-ignore
              entities: collection.scripts.map((e) =>
                entityToCardTransformerFromGraphCms(e)
              ),
            },
            true
          )

          this._moreCollectionCards = result.more_by_owner
            ? result.more_by_owner.map((moreCollections) =>
                makeDataModel(
                  collectionFromGraphCmsToCardCollectionTransformer(
                    moreCollections
                  ),
                  true
                )
              )
            : []

          this.getIsInMyLibrary(slug)
          this.setPageMetaData()
        },
        (error) => {
          this._applicationService.handleError(error)
        }
      )
    })
  }

  toggleAddedToLibrary() {
    this._isUpdatingMyLibrary.next(true)
    if (!this._isInMyLibrary.getValue()) {
      this._collectionService
        .postAddToMyLibrary(this.collection.data.slug)
        .pipe(
          tap(() => this._isUpdatingMyLibrary.next(false)),
          tap((result: { user_saved_total }) =>
            this.trackAddedToLibrary("Collection Favorited", result)
          )
        )
        .subscribe(() => this._isInMyLibrary.next(true))
    } else {
      this._collectionService
        .deleteAddToMyLibrary(this.collection.data.slug)
        .pipe(
          tap(() => this._isUpdatingMyLibrary.next(false)),
          tap((result: { user_saved_total }) =>
            this.trackAddedToLibrary("Collection Unfavorited", result)
          )
        )
        .subscribe(() => this._isInMyLibrary.next(false))
    }
  }

  handleShare() {
    this._copy.copyUrlToClipboard()
  }

  private resetCollection() {
    this._collection = makeDataModel(new CardCollectionModel())
  }

  private trackAddedToLibrary(name: string, result: { user_saved_total }) {
    Segment.track(name, {
      name: this.collection.data.name,
      slug: this.collection.data.slug,
      user_saved_count: result.user_saved_total,
    })
  }

  private setPageMetaData(): void {
    this._pageTitle.setTitle(
      this.collection.data.name + " - Adaptavist Library"
    )
    this._meta.updateTag({
      name: "description",
      content:
        this.collection.data.meta_description === null
          ? this.collection.data.description.replace(/<[^>]*>/g, "")
          : this.collection.data.meta_description,
    })
  }

  private resetIsInMyLibrary(): void {
    this._isInMyLibrary.next(false)
  }

  private getIsInMyLibrary(slug: string): void {
    if (this._profile.isAuthenticated()) {
      this._userService.getIsInLibrary("collection", slug).subscribe((data) => {
        this._isInMyLibrary.next(data["in-library"])
      })
    }
  }
}
