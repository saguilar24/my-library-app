import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { CollectionPageComponent } from "./collection-page.component"
import { RouterModule } from "@angular/router"
import { EntityCardPartialModule } from "../../partials/entity-card-partial/entity-card-partial.module"
import { NoRenderModule } from "../../../norender.module"
import { NavigationModule } from "../../lib/navigation/navigation.module"
import { TypographyModule } from "../../lib/typography/typography.module"
import { CardsModule } from "../../lib/cards/cards.module"
import { LayoutModule } from "../../lib/layout/layout.module"
import { ScriptModule } from "../../lib/pages/scripts/script.module"
import { CollectionModule } from "../../lib/pages/collection/collection.module"

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NavigationModule,
    TypographyModule,
    CardsModule,
    LayoutModule,
    ScriptModule,
    CollectionModule,
    EntityCardPartialModule,
    NoRenderModule,
  ],
  exports: [CollectionPageComponent],
  declarations: [CollectionPageComponent],
})
export class CollectionPageModule {}
