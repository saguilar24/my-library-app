import { Component, OnInit } from "@angular/core"
import { CollectionService } from "../../../services/collection/collection.service"
import { EntityService } from "../../../services/entity/entity.service"
import { Meta, Title } from "@angular/platform-browser"
import { CardCollectionModel } from "../../../models/card-collection.model"
import { entityToCardTransformerFromGraphCms } from "../../../transformers/entity-transformers"
import { collectionFromGraphCmsToCardCollectionTransformer } from "../../../transformers/collection-transformers"
import { DataModel } from "../../../models/data.model"
import makeDataModel from "../../../utilities/make-data-model"
import { CardModel } from "../../../models/card.model"
import { Router } from "@angular/router"

@Component({
  selector: "app-collections-page",
  templateUrl: "./collections-page.component.html",
  providers: [],
})
export class CollectionsPageComponent implements OnInit {
  constructor(
    private _collectionService: CollectionService,
    private _entityService: EntityService,
    private _pageTitle: Title,
    private _meta: Meta,
    private _router: Router
  ) {}

  private _collections = []

  get collections(): DataModel<CardCollectionModel>[] {
    return this._collections
  }

  private _collectionPageType: string

  get collectionPageType(): string {
    return this._collectionPageType
  }

  ngOnInit() {
    if (this._router.url === "/collections/newest") {
      this.getNewestAdaptavistCollections()
      this._collectionPageType = "newest"
    } else if (this._router.url === "/collections/trending") {
      this.getTrendingCollections()
      this._collectionPageType = "trending"
    }
    this.setPageMetaData()
  }

  getNewestAdaptavistCollections(): void {
    this._collectionService.getNewestAdaptavistCollectionsFromGraphCms(12).then(
      (result) =>
        (this._collections = result.collections.map((collection) =>
          makeDataModel(
            {
              ...collectionFromGraphCmsToCardCollectionTransformer(collection),
              // @ts-ignore
              entities: collection.scripts.map((script) =>
                entityToCardTransformerFromGraphCms(script)
              ),
            },
            true
          )
        ))
    )
  }

  getTrendingCollections(): void {
    this._collectionService.getTrendingPartnerCollectionsFromGraphCms(12).then(
      (result) =>
        (this._collections = result.collections.map((collection) =>
          makeDataModel(
            {
              ...collectionFromGraphCmsToCardCollectionTransformer(collection),
              // @ts-ignore
              entities: collection.scripts.map((script) =>
                entityToCardTransformerFromGraphCms(script)
              ),
            },
            true
          )
        ))
    )
  }

  createCardDataModel(data, isReady): DataModel<CardModel> {
    return { data, isReady }
  }

  private setPageMetaData(): void {
    if (this._collectionPageType === "newest") {
      this._pageTitle.setTitle(
        "Newest Adaptavist Collections - Adaptavist Library"
      )
      this._meta.updateTag({
        name: "description",
        content: "The newest Collections from the Adaptavist Library.",
      })
    } else if (this._collectionPageType === "trending") {
      this._pageTitle.setTitle(
        "Trending Partner Collections - Adaptavist Library"
      )
      this._meta.updateTag({
        name: "description",
        content: "Trending Partner Collections from the Adaptavist Library.",
      })
    }
  }
}
