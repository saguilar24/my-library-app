import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { CollectionsPageComponent } from "./collections-page.component"
import { RouterModule } from "@angular/router"
import { NavigationModule } from "../../lib/navigation/navigation.module"
import { TypographyModule } from "../../lib/typography/typography.module"
import { CardsModule } from "../../lib/cards/cards.module"
import { ScriptModule } from "../../lib/pages/scripts/script.module"
import { LayoutModule } from "../../lib/layout/layout.module"
import { CollectionModule } from "../../lib/pages/collection/collection.module"
import { EntityCardPartialModule } from "../../partials/entity-card-partial/entity-card-partial.module"

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NavigationModule,
    TypographyModule,
    CardsModule,
    LayoutModule,
    ScriptModule,
    CollectionModule,
    EntityCardPartialModule,
  ],
  exports: [CollectionsPageComponent],
  declarations: [CollectionsPageComponent],
})
export class CollectionsPageModule {}
