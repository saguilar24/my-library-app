import { Component, OnInit } from "@angular/core"
import { UserService } from "../../../services/user/user.service"
import { ActivatedRoute } from "@angular/router"
import { SavedSearch } from "../../../models/saved-search.model"
import { ApplicationService } from "../../../services/application/application.service"

@Component({
  selector: "app-saved-search-page",
  templateUrl: "./saved-search-page.component.html",
})
export class SavedSearchPageComponent implements OnInit {
  savedSearches: SavedSearch[]

  constructor(
    private _applicationService: ApplicationService,
    private _route: ActivatedRoute,
    private _userService: UserService
  ) {}

  ngOnInit() {
    this._route.paramMap.subscribe((params) => this.getSavedSearches())
  }

  getSavedSearches(): void {
    this._userService.getSavedSearches().subscribe(
      (savedSearches) => (this.savedSearches = savedSearches),
      (error) => {
        this._applicationService.handleError(error)
      }
    )
  }

  removeSavedSearch(savedSearch: SavedSearch) {
    this._userService.removeSavedSearch(savedSearch).subscribe(
      (savedSearches) => (this.savedSearches = savedSearches),
      (error) => {
        this._applicationService.handleError(error)
      }
    )
  }
}
