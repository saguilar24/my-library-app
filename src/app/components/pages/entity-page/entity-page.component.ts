import { Component, Input, OnInit } from "@angular/core"
import { Location } from "@angular/common"
import { EntityService } from "../../../services/entity/entity.service"
import { ActivatedRoute } from "@angular/router"
import { Meta, Title, TransferState } from "@angular/platform-browser"
import { PrismService } from "../../../services/prism/prism.service"
import { BehaviorSubject } from "rxjs"
import { AttachmentModel } from "../../../models/attachment.model"
import { tap } from "rxjs/operators"
import { CopyTextService } from "../../../services/copy/copy-text.service"
import { ProfileService } from "../../../services/profile/profile.service"
import { AuthService } from "../../../services/auth/auth.service"
import { Segment } from "../../../services/segment/segment.service"
import { DataModel } from "../../../models/data.model"
import { AttachmentTypesModel } from "../../../models/attachment-types.model"
import { CardModel } from "../../../models/card.model"
import { ScriptModel } from "../../../models/script.model"
import { CardCollectionModel } from "../../../models/card-collection.model"
import { ApplicationService } from "../../../services/application/application.service"
import {
  entityToCardTransformerFromAlgolia,
  entityToCardTransformerFromGraphCms,
} from "../../../transformers/entity-transformers"
import { collectionFromGraphCmsToCardCollectionTransformer } from "../../../transformers/collection-transformers"
import { UserService } from "../../../services/user/user.service"
import { RecentEntitiesService } from "../../../services/entity/recent-entities.service"
import makeDataModel from "../../../utilities/make-data-model"

@Component({
  selector: "app-entity-page",
  templateUrl: "./entity-page.component.html",
  providers: [PrismService, CopyTextService],
})
export class EntityPageComponent implements OnInit {
  private _requirements = []
  private _hasUpVotes = false
  private _hasDownVotes = false
  private _upVotes = 0
  private _downVotes = 0

  constructor(
    private _applicationService: ApplicationService,
    private _location: Location,
    private _route: ActivatedRoute,
    private _entityService: EntityService,
    private _pageTitle: Title,
    private _meta: Meta,
    private _prismService: PrismService,
    private _copy: CopyTextService,
    private _recentEntities: RecentEntitiesService,
    private _profileService: ProfileService,
    private _authService: AuthService,
    private _state: TransferState,
    private _userService: UserService
  ) {}

  get profileService() {
    return this._profileService
  }

  get authService(): AuthService {
    return this._authService
  }

  private _isUpdatingMyLibrary = new BehaviorSubject<boolean>(false)

  get isUpdatingMyLibrary(): BehaviorSubject<boolean> {
    return this._isUpdatingMyLibrary
  }

  private _isUpdatingWatchEntity = new BehaviorSubject<boolean>(false)

  get isUpdatingWatchEntity(): BehaviorSubject<boolean> {
    return this._isUpdatingWatchEntity
  }

  private _selectedTab = ""

  get selectedTab(): any {
    return this._selectedTab
  }

  private _selectedTabName = ""

  get selectedTabName(): any {
    return this._selectedTabName
  }

  private _selectedVersion = 0

  get selectedVersion(): number {
    return this._selectedVersion
  }

  private _tabs = []

  get tabs(): any {
    return this._tabs
  }

  private _hideAllFeatures = true

  get hideAllFeatures(): boolean {
    return this._hideAllFeatures
  }

  private _hideAllTags = true

  get hideAllTags(): boolean {
    return this._hideAllTags
  }

  private _codeCopied = false

  get codeCopied(): boolean {
    return this._codeCopied
  }

  private _isInMyLibrary = new BehaviorSubject<boolean>(false)

  get isInMyLibrary(): BehaviorSubject<boolean> {
    return this._isInMyLibrary
  }

  private _isWatching = new BehaviorSubject<boolean>(false)

  get isWatching(): BehaviorSubject<boolean> {
    return this._isWatching
  }

  private _script = makeDataModel(new ScriptModel())

  get script(): DataModel<ScriptModel> {
    // @ts-ignore
    return this._script
  }

  private _relatedEntities: DataModel<CardModel>[]

  get relatedEntities(): DataModel<CardModel>[] {
    return this._relatedEntities
  }

  private _collections: DataModel<CardCollectionModel>[]

  get collections(): DataModel<CardCollectionModel>[] {
    return this._collections
  }

  get location(): Location {
    return this._location
  }

  get recentEntities(): DataModel<CardModel>[] {
    // @ts-ignore
    return this._recentEntities.getN(4).map((c) => makeDataModel(c, true))
  }

  private _currentAttachment$ = new BehaviorSubject<AttachmentModel>({
    data: "",
    id: 0,
    language: "",
    platform_id: 0,
    platform_name: "",
    requirements: [],
    slug: "",
    updated_at: "",
    entity_slug: "",
    verified: false,
  })

  get currentAttachment$(): BehaviorSubject<AttachmentModel> {
    return this._currentAttachment$
  }

  private _currentTabRequirements = []

  get currentTabRequirements() {
    return this._currentTabRequirements
  }

  ngOnInit() {
    this._route.params.subscribe(() => {
      const scriptIdGraphCms = this._route.snapshot.paramMap.get("slug")
      this.resetScript()
      this.resetIsInMyLibrary()
      this.resetIsWatching()

      // This call is made to get the votes from the database script. This information is not found in GraphCMS.
      this._entityService
        .getEntityByGraphCmsIdFromApi(scriptIdGraphCms)
        .subscribe(
          (entity) => {
            this._upVotes = entity.data.upvotes
            this._downVotes = entity.data.downvotes
            this._hasUpVotes = entity.data.has_upvoted_entity
            this._hasDownVotes = entity.data.has_downvoted_entity
          },
          (error) => {
            console.log(
              `There is an error getting the information of a script from the database: ${error}`
            )
          },
          () => {
            this._entityService
              .getEntityByScriptIdFromGraphCms(scriptIdGraphCms)
              .then(
                (result) => {
                  result.script = this.setVotesData(result.script)
                  this._entityService
                    .getLastUpdateDateOfScriptAndSubElementsByScriptIdFromGraphCms(
                      scriptIdGraphCms
                    )
                    .then((date) => {
                      result.script.lastUpdatedDate = date
                      this._script = makeDataModel(
                        this._entityService.graphCmsEntityToScriptModel(
                          result.script
                        ),
                        true
                      )
                      this.setPageMetaData()
                      // @ts-ignore
                      this.setRequirementData(this._script.data.attachments)
                      // @ts-ignore
                      this.setTabData(this._script.data.attachments)
                      this.setIsInMyLibrary(scriptIdGraphCms)
                      this.setupTabsAndAttachmentsFor(
                        "dataCenter",
                        "Data Center"
                      )
                      this.setupTabsAndAttachmentsFor("cloud", "Cloud")
                      this.setupTabsAndAttachmentsFor("server", "Server")
                      this._recentEntities.add(
                        entityToCardTransformerFromGraphCms(result.script)
                      )
                      this._entityService
                        .getRelatedEntitiesFromGraphCms(
                          result.script.scriptId,
                          result.script.appVs,
                          result.script.versionsInfo,
                          4
                        )
                        .then((result) => {
                          // @ts-ignore
                          this._relatedEntities = result.map((script) =>
                            makeDataModel(
                              entityToCardTransformerFromGraphCms(script),
                              true
                            )
                          )
                        })
                      this._collections = result.collections.map((collection) =>
                        makeDataModel(
                          collectionFromGraphCmsToCardCollectionTransformer(
                            collection
                          ),
                          true
                        )
                      )
                    })
                },
                (error) => {
                  this._applicationService.handleError(error)
                }
              )
          }
        )
    })
  }

  toggleAddedToLibrary() {
    this._isUpdatingMyLibrary.next(true)
    if (!this._isInMyLibrary.getValue()) {
      this._entityService
        .postAddToMyLibrary(this._script.data.slug)
        .pipe(
          tap(() => this._isUpdatingMyLibrary.next(false)),
          tap((result: { user_saved_total }) =>
            this.trackSegmentEvent("Entity Favorited", result)
          )
        )
        .subscribe(() => this._isInMyLibrary.next(true))
    } else {
      this._entityService
        .deleteAddToMyLibrary(this._script.data.slug)
        .pipe(
          tap(() => this._isUpdatingMyLibrary.next(false)),
          tap((result: { user_saved_total }) =>
            this.trackSegmentEvent("Entity Unfavorited", result)
          )
        )
        .subscribe(() => this._isInMyLibrary.next(false))
    }
  }

  toggleAddedWatchEntity() {
    this.isUpdatingWatchEntity.next(true)
    if (!this._isWatching.getValue()) {
      this._entityService
        .postWatchEntity(this._script.data.slug)
        .pipe(
          tap(() => this.isUpdatingWatchEntity.next(false)),
          tap((result: { user_saved_total }) =>
            this.trackSegmentEvent("Entity Watched", result)
          )
        )
        .subscribe(() => this._isWatching.next(true))
    } else {
      this._entityService
        .deleteWatchEntity(this._script.data.slug)
        .pipe(
          tap(() => this.isUpdatingWatchEntity.next(false)),
          tap((result: { user_saved_total }) =>
            this.trackSegmentEvent("Entity Un-watched", result)
          )
        )
        .subscribe(() => this._isWatching.next(false))
    }
  }

  handleRequirementsChange(e) {
    this._codeCopied = false
    this._currentAttachment$.next(
      // @ts-ignore
      this._script.data.attachments[this._selectedTab][e.target.value]
    )
    this._selectedVersion = e.target.value
  }

  handleTabChange(selectedTab: string, selectedTabName: string) {
    this._codeCopied = false
    this._selectedTab = selectedTab
    this._selectedTabName = selectedTabName
    this._currentTabRequirements = this._requirements[this._selectedTab]
    this._selectedVersion = this._currentTabRequirements.length - 1
    this._currentAttachment$.next(
      // @ts-ignore
      this._script.data.attachments[this._selectedTab][this._selectedVersion]
    )
  }

  handleShare($event: boolean) {
    this._copy.copyUrlToClipboard()
  }

  // Copy code from CTA button under code box
  copyCode() {
    this._copy.copyTextToClipboard(this._currentAttachment$.getValue().data)
    this._entityService
      .incrementCopyCount(
        this._script.data.slug,
        this.currentAttachment$.getValue().slug
      )
      .subscribe()
    Segment.track("Code Copy", {
      method: "CTA",
      entity_name: this._script.data.name,
      entity_slug: this._currentAttachment$.getValue().slug,
    })
    this._codeCopied = true
    setTimeout(() => (this._codeCopied = false), 1500)
  }

  toggleAllFeatures() {
    this._hideAllFeatures = !this._hideAllFeatures
  }

  toggleAllTags() {
    this._hideAllTags = !this._hideAllTags
  }

  private resetScript() {
    this._script = makeDataModel(new ScriptModel())
  }

  private formatAttachmentsFor(platform) {
    // @ts-ignore
    this._script.data.attachments[platform].map((a) => {
      a.styledData = this._prismService.highlight(a.data)
    })
  }

  private trackSegmentEvent(name: string, result: { user_saved_total }) {
    Segment.track(name, {
      name: this._script.data.name,
      slug: this._script.data.slug,
      user_saved_count: result.user_saved_total,
    })
  }

  private setRequirementData(platformAttachments: AttachmentTypesModel) {
    this.setRequirementDataFor("server", platformAttachments)
    this.setRequirementDataFor("cloud", platformAttachments)
    this.setRequirementDataFor("dataCenter", platformAttachments)

    return this._requirements
  }

  private setRequirementDataFor(platform, platformAttachments) {
    if (platformAttachments[platform].length) {
      this._requirements[platform] = []
      platformAttachments[platform].map((a, aIndex) => {
        this._requirements[platform][aIndex] = a.requirements.reduce(
          (str, r, rIndex) => {
            str +=
              r.version_from && r.version_to
                ? r.product_name +
                  " (" +
                  r.version_from +
                  " - " +
                  r.version_to +
                  ")"
                : r.product_name
            if (rIndex + 1 < a.requirements.length) {
              str += ", "
            }
            return str
          },
          ""
        )
      })
    }
  }

  private setTabData(platformAttachments: AttachmentTypesModel) {
    this._tabs = []

    if (platformAttachments.server.length) {
      this._tabs.push(["server", "Server"])
    }
    if (platformAttachments.cloud.length) {
      this._tabs.push(["cloud", "Cloud"])
    }
    if (platformAttachments.dataCenter.length) {
      this._tabs.push(["dataCenter", "Data Center"])
    }

    return this._tabs
  }

  private setupTabsAndAttachmentsFor(platform, platform_name) {
    // @ts-ignore
    if (this._script.data.attachments[platform].length) {
      this._selectedTab = platform
      this._selectedTabName = platform_name
      // @ts-ignore
      this._selectedVersion = this._script.data.attachments[platform].length - 1
      this._currentAttachment$.next(
        // @ts-ignore
        this._script.data.attachments[platform][
          // @ts-ignore
          this._script.data.attachments[platform].length - 1
        ]
      )
      this.formatAttachmentsFor(platform)
      this._currentTabRequirements = this._requirements[this._selectedTab]
    }
  }

  private setPageMetaData(): void {
    this._pageTitle.setTitle(this._script.data.name + " - Adaptavist Library")
    this._meta.updateTag({
      name: "description",
      content:
      // @ts-ignore
        this._script.data.meta_description === null
          ? this._script.data.description.replace(/<[^>]*>/g, "")
          // @ts-ignore
          : this._script.data.meta_description,
    })
  }

  private resetIsInMyLibrary(): void {
    this._isInMyLibrary.next(false)
  }

  private resetIsWatching(): void {
    this._isWatching.next(false)
  }

  private setIsInMyLibrary(slug: string): void {
    if (this._profileService.isAuthenticated()) {
      this._userService.getIsInLibrary("entity", slug).subscribe((data) => {
        this._isInMyLibrary.next(data["in-library"])
        this._isWatching.next(data["watching"])
      })
    }
  }

  private setVotesData(script) {
    script.upvotes = this._upVotes
    script.downvotes = this._downVotes
    script.has_upvoted_entity = this._hasUpVotes
    script.has_downvoted_entity = this._hasDownVotes
    return script
  }
}
