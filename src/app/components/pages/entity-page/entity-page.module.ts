import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { ModalsModule } from "../../lib/modals/modals.module"
import { EntityPageComponent } from "./entity-page.component"
import { RouterModule } from "@angular/router"
import { AttachmentComponent } from "../../partials/attachment/attachment.component"
import { EntityCardPartialModule } from "../../partials/entity-card-partial/entity-card-partial.module"
import { NoRenderModule } from "../../../norender.module"
import { CardsModule } from "../../lib/cards/cards.module"
import { LayoutModule } from "../../lib/layout/layout.module"
import { ScriptModule } from "../../lib/pages/scripts/script.module"
import { NavigationModule } from "../../lib/navigation/navigation.module"
import { TypographyModule } from "../../lib/typography/typography.module"
import { RequirementsModule } from "../../lib/requirements/requirements.module"
import { FormsModule, ReactiveFormsModule } from "@angular/forms"

@NgModule({
  imports: [
    CommonModule,
    CardsModule,
    LayoutModule,
    ScriptModule,
    RouterModule,
    NavigationModule,
    TypographyModule,
    ModalsModule,
    RequirementsModule,
    EntityCardPartialModule,
    NoRenderModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [EntityPageComponent],
  declarations: [EntityPageComponent, AttachmentComponent],
})
export class EntityPageModule {}
