import { Component, OnInit } from "@angular/core"
import { EntityService } from "../../../../services/entity/entity.service"
import { BehaviorSubject } from "rxjs"
import { ActivatedRoute } from "@angular/router"
import { Meta, Title } from "@angular/platform-browser"
import { tap } from "rxjs/operators"
import { DataModel } from "../../../../models/data.model"
import { CardModel } from "../../../../models/card.model"
import { entityToCardTransformerFromGraphCms } from "../../../../transformers/entity-transformers"
import makeDataModel from "../../../../utilities/make-data-model"

@Component({
  selector: "app-library-entities-page",
  templateUrl: "./library-entities-page.component.html",
})
export class LibraryEntitiesPageComponent implements OnInit {
  constructor(
    private _route: ActivatedRoute,
    private _entityService: EntityService,
    private _pageTitle: Title,
    private _meta: Meta
  ) {}

  private _loading = new BehaviorSubject<boolean>(true)

  get loading(): boolean {
    return this._loading.getValue()
  }

  private _cards = new BehaviorSubject<DataModel<CardModel>[]>(
    Array(3).fill(makeDataModel(new CardModel()))
  )

  get cards(): DataModel<CardModel>[] {
    return this._cards.getValue()
  }

  ngOnInit() {
    this.setPageMetaData()
    this._route.paramMap.subscribe((params) =>
      this.getMyEntities(params.get("type"))
    )
  }

  getMyEntities(type): void {
    this._entityService
      .getMyEntityList(type)
      .pipe(tap(() => this._loading.next(false)))
      .subscribe((myEntities) => {
        const graphcmsIds: string[] = myEntities.map((e) => e.graphcms_id)
        this._entityService
          .getEntityListFromGraphCmsIds(graphcmsIds)
          .then((result) => {
            this._cards.next(
              // @ts-ignore
              result.scripts.map((script) =>
                makeDataModel(entityToCardTransformerFromGraphCms(script), true)
              )
            )
          })
        myEntities.forEach((entity) => {
          this._entityService
            .getEntityByScriptIdFromGraphCms(entity.graphcms_id)
            .then((result) => {
              result.scripts
            })
        })
      })
  }

  private setPageMetaData(): void {
    this._pageTitle.setTitle("Entities - My Library - Adaptavist Library")
    this._meta.updateTag({
      name: "description",
      content: "View the Entities you have saved to your library.",
    })
  }
}
