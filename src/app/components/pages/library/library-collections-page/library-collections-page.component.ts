import { Component, OnInit } from "@angular/core"
import { CollectionService } from "../../../../services/collection/collection.service"
import { BehaviorSubject } from "rxjs"
import { EntityService } from "../../../../services/entity/entity.service"
import { Meta, Title } from "@angular/platform-browser"
import { tap } from "rxjs/operators"
import { DataModel } from "../../../../models/data.model"
import { CardModel } from "../../../../models/card.model"
import { CardCollectionModel } from "../../../../models/card-collection.model"
import { collectionFromGraphCmsToCardCollectionTransformer } from "../../../../transformers/collection-transformers"
import { entityToCardTransformerFromGraphCms } from "../../../../transformers/entity-transformers"
import makeDataModel from "../../../../utilities/make-data-model"

@Component({
  selector: "app-library-collections-page",
  templateUrl: "./library-collections-page.component.html",
})
export class LibraryCollectionsPageComponent implements OnInit {
  constructor(
    private _collectionService: CollectionService,
    private _entityService: EntityService,
    private _pageTitle: Title,
    private _meta: Meta
  ) {}

  private _loading = new BehaviorSubject<boolean>(true)

  get loading(): boolean {
    return this._loading.getValue()
  }


  private _collections = new BehaviorSubject<DataModel<CardCollectionModel>[]>([
    // @ts-ignore
    makeDataModel(new CardCollectionModel()),
  ])

  get collections(): DataModel<CardCollectionModel>[] {
    return this._collections.getValue()
  }

  createCardDataModel(data, isReady): DataModel<CardModel> {
    return { data, isReady }
  }

  ngOnInit() {
    this.getMyCollections()
    this.setPageMetaData()
  }

  getMyCollections(): void {
    this._collectionService
      .getMyCollectionList()
      .pipe(tap(() => this._loading.next(false)))
      .subscribe((myCollections) => {
        const collectionSlugs: string[] = myCollections.map(
          (collection) => collection.slug
        )
        this._collectionService
          .getCollectionListFromGraphCmsSlugs(collectionSlugs)
          .then((result) => {
            this._collections.next(
              // @ts-ignore
              result.collections.map((collection) =>
                makeDataModel(
                  {
                    ...collectionFromGraphCmsToCardCollectionTransformer(
                      collection
                    ),
                    // @ts-ignore
                    entities: collection.scripts.map((script) =>
                      entityToCardTransformerFromGraphCms(script)
                    ),
                    type: "collection",
                  },
                  true
                )
              )
            )
          })
      })
  }

  private setPageMetaData(): void {
    this._pageTitle.setTitle("Collections - My Library - Adaptavist Library")
    this._meta.updateTag({
      name: "description",
      content: "View the Collections you have saved to your library.",
    })
  }
}
