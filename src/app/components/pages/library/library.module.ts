import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { RouterModule } from "@angular/router"
import { LibraryPageComponent } from "./library-page/library-page.component"
import { SideBarPartialModule } from "../../partials/side-bar-partial/side-bar-partial.module"
import { LibraryEntitiesPageComponent } from "./library-entities-page/library-entities-page.component"
import { LibraryCollectionsPageComponent } from "./library-collections-page/library-collections-page.component"
import { EntityCardPartialModule } from "../../partials/entity-card-partial/entity-card-partial.module"
import { CardsModule } from "../../lib/cards/cards.module"
import { LayoutModule } from "../../lib/layout/layout.module"
import { MyLibraryModule } from "../../lib/pages/my-library/my-library.module"
import { TypographyModule } from "../../lib/typography/typography.module"
import { NavigationModule } from "../../lib/navigation/navigation.module"

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    CardsModule,
    LayoutModule,
    SideBarPartialModule,
    MyLibraryModule,
    NavigationModule,
    TypographyModule,
    EntityCardPartialModule,
  ],
  exports: [LibraryPageComponent],
  declarations: [
    LibraryPageComponent,
    LibraryEntitiesPageComponent,
    LibraryCollectionsPageComponent,
  ],
})
export class LibraryModule {}
