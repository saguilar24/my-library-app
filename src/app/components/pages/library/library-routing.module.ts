import { NgModule } from "@angular/core"
import { RouterModule, Routes } from "@angular/router"
import { LibraryCollectionsPageComponent } from "./library-collections-page/library-collections-page.component"
import { LibraryEntitiesPageComponent } from "./library-entities-page/library-entities-page.component"
import { AuthAndVerifiedGuard } from "../../../guards/auth-and-verified.guard"
import { LibraryPageComponent } from "./library-page/library-page.component"

const routes: Routes = [
  {
    path: "library",
    redirectTo: "/library/added-collections",
    pathMatch: "full",
  },
  {
    path: "library",
    canActivate: [AuthAndVerifiedGuard],
    component: LibraryPageComponent,
    children: [
      {
        path: "added-collections",
        component: LibraryCollectionsPageComponent,
      },
      {
        path: ":type",
        component: LibraryEntitiesPageComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LibraryRoutingModule {}
