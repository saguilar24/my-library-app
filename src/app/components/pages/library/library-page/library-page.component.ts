import { Component, OnInit } from "@angular/core"
import { ActivatedRoute } from "@angular/router"
import { Meta, Title } from "@angular/platform-browser"
import { Location } from "@angular/common"
import { UserService } from "../../../../services/user/user.service"

@Component({
  selector: "app-library-page",
  templateUrl: "./library-page.component.html",
})
export class LibraryPageComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private _location: Location,
    private _pageTitle: Title,
    private _meta: Meta,
    private _userService: UserService
  ) {}

  get location(): Location {
    return this._location
  }

  private _tabs: { text; link; count }[] = [
    {
      text: "Collections",
      link: "added-collections",
      count: 0,
    },
    {
      text: "Scripts",
      link: "added-scripts",
      count: 0,
    },
    {
      text: "Snippets",
      link: "added-snippets",
      count: 0,
    },
  ]

  get tabs(): { text: string; link: string; count: number }[] {
    return this._tabs
  }

  ngOnInit() {
    this._userService.getUserMeta().subscribe((d) => {
      this._tabs = this._tabs.map((t) => ({ ...t, count: d[t.link] }))
    })
    this.setPageMetaData()
  }

  private setPageMetaData(): void {
    this._pageTitle.setTitle("My Library - Adaptavist Library")
    this._meta.updateTag({
      name: "description",
      content:
        "View the Scripts, Snippets and Collections you have saved to your library.",
    })
  }
}
