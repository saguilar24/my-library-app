import { Component, OnInit } from "@angular/core"
import { Router } from "@angular/router"
import { AuthService } from "../../../services/auth/auth.service"
import { ProfileService } from "../../../services/profile/profile.service"
import { ApplicationService } from "../../../services/application/application.service"

@Component({
  selector: "app-callback-page",
  templateUrl: "./callback-page.component.html",
  styleUrls: [],
})
export class CallbackPageComponent implements OnInit {
  constructor(
    private auth: AuthService,
    private router: Router,
    private profile: ProfileService,
    private appService: ApplicationService
  ) {}

  ngOnInit() {
    if (!this.appService.isRunningInClientBrowser()) {
      return
    }
    if (this.profile.isAuthenticated()) {
      return this.router.navigate(["/"])
    }
    this.auth
      .handleAuthenticationCallback()
      .then((nextPage) => {
        return this.router.navigateByUrl(nextPage)
      })
      .catch(() => {
        return this.router.navigate(["/"])
      })
  }
}
