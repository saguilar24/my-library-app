import { Component, OnInit } from "@angular/core"
import { UserService } from "../../../services/user/user.service"
import { ActivatedRoute } from "@angular/router"
import { UserModel } from "../../../models/user.model"
import { ApplicationService } from "../../../services/application/application.service"
import { BehaviorSubject } from "rxjs"
import { tap } from "rxjs/operators"
import { DataModel } from "../../../models/data.model"
import { CardModel } from "../../../models/card.model"
//import { entityToCardTransformer } from "../../../transformers/entity-transformers"
import makeDataModel from "../../../utilities/make-data-model"

@Component({
  selector: "app-profile-code-page",
  templateUrl: "./profile-code-page.component.html",
})
export class ProfileCodePageComponent implements OnInit {
  constructor(
    private _route: ActivatedRoute,
    private _userService: UserService
  ) {}

  private _user: UserModel

  private _loading = new BehaviorSubject<boolean>(true)

  get loading(): boolean {
    return this._loading.getValue()
  }

  private _viewingOwnProfile = new BehaviorSubject<boolean>(false)

  get viewingOwnProfile(): boolean {
    return this._viewingOwnProfile.getValue()
  }

  private _cards = new BehaviorSubject<DataModel<CardModel>[]>(
    Array(3).fill(makeDataModel(new CardModel()))
  )

  get cards(): DataModel<CardModel>[] {
    return this._cards.getValue()
  }

  ngOnInit() {
    this._route.params.subscribe(() => {
      const slug = this._route.snapshot.paramMap.get("uuid")

      if (!slug || slug.length === 0) {
        this._viewingOwnProfile.next(true)
      }

      // This method links the script created with the corresponding author, but currently Library does not offer that functionality.
      // This method has been commented on because it does not make sense in the current and new infrastructure.
      /*this._userService
        .getUserAuthoredCode(slug)
        .pipe(tap(() => this._loading.next(false)))
        .subscribe((authoredEntities) =>
          this._cards.next(
            authoredEntities.map((e) =>
              makeDataModel(entityToCardTransformer(e), true)
            )
          )
        )*/
    })
  }
}
