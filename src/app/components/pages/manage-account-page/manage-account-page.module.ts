import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { RouterModule } from "@angular/router"
import { BrowserModule } from "@angular/platform-browser"
import { FormsModule } from "@angular/forms"
import { NavigationModule } from "../../lib/navigation/navigation.module"
import { ManageAccountPageComponent } from "./manage-account-page.component"
import { CardsModule } from "../../lib/cards/cards.module"
import { EntityCardPartialModule } from "../../partials/entity-card-partial/entity-card-partial.module"
import { WatchedEntitiesPageComponent } from "../watched-entities-page/watched-entities-page.component"
import { SavedSearchPageComponent } from "../saved-search-page/saved-search-page.component"
import { ManageAccountHeroPartial } from "../../partials/manage-account-hero-partial/manage-account-hero-partial..component"
import { ModalsModule } from "../../lib/modals/modals.module"

@NgModule({
  imports: [
    EntityCardPartialModule,
    CommonModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    ModalsModule,
    NavigationModule,
    CardsModule,
  ],
  exports: [ManageAccountPageComponent],
  declarations: [
    ManageAccountPageComponent,
    WatchedEntitiesPageComponent,
    SavedSearchPageComponent,
    ManageAccountHeroPartial,
  ],
})
export class ManageAccountPageModule {}
