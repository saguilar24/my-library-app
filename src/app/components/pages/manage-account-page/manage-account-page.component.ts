import { Component, OnInit, Input } from "@angular/core"
import { Router } from "@angular/router"
import { Meta, Title } from "@angular/platform-browser"
import { UserService } from "../../../services/user/user.service"
import { UserModel } from "../../../models/user.model"
import { ApplicationService } from "../../../services/application/application.service"
import { ProfileService } from "../../../services/profile/profile.service"
import { AuthService } from "../../../services/auth/auth.service"
import { BehaviorSubject } from "rxjs"

@Component({
  selector: "app-manage-account-page",
  templateUrl: "./manage-account-page.component.html",
})
export class ManageAccountPageComponent implements OnInit {
  constructor(
    private _router: Router,
    private _pageTitle: Title,
    private _meta: Meta,
    private _userService: UserService,
    private _applicationService: ApplicationService,
    private _profileService: ProfileService,
    private _authService: AuthService
  ) {}

  private _user: UserModel
  private _formLoading = false
  private _emailFormLoading = false
  private _updateEmailLoading = false
  private _updatePasswordLoading = false
  private _deleteAccountLoading = false
  private _displayConfirmation = false
  private _displayUpdatePasswordConfirmation = false
  private _showChangeEmailModal = new BehaviorSubject<boolean>(false)
  private _showDeleteAccountModal = new BehaviorSubject<boolean>(false)
  private _updateUserFormErrorMessages: string
  private _changeEmailFormErrorMessages: string
  private _deleteAccountErrorMessages: string

  get user(): UserModel {
    return this._user
  }

  get formLoading(): boolean {
    return this._formLoading
  }

  get emailFormLoading(): boolean {
    return this._emailFormLoading
  }

  get displayConfirmation(): boolean {
    return this._displayConfirmation
  }

  get updateEmailLoading(): boolean {
    return this._updateEmailLoading
  }

  get updatePasswordLoading(): boolean {
    return this._updatePasswordLoading
  }

  get deleteAccountLoading(): boolean {
    return this._deleteAccountLoading
  }

  get displayUpdatePasswordConfirmation(): boolean {
    return this._displayUpdatePasswordConfirmation
  }

  get showChangeEmailModal(): BehaviorSubject<boolean> {
    return this._showChangeEmailModal
  }

  get showDeleteAccountModal(): BehaviorSubject<boolean> {
    return this._showDeleteAccountModal
  }

  get updateUserFormErrorMessages(): string {
    return this._updateUserFormErrorMessages
  }

  get changeEmailFormErrorMessages(): string {
    return this._changeEmailFormErrorMessages
  }

  get deleteAccountErrorMessages(): string {
    return this._deleteAccountErrorMessages
  }

  ngOnInit() {
    this.setPageMetaData()

    this._userService.getUser().subscribe(
      (u) => {
        this._user = u as UserModel

        this._user.first_name_visible =
          this._user.public.indexOf("first_name") > -1
        this._user.last_name_visible =
          this._user.public.indexOf("last_name") > -1
        this._user.email_visible = this._user.public.indexOf("email") > -1

        this._user.avatar_url = this._user.avatar
        this._user.small_avatar_url = this._user.small_avatar
      },
      (error) => {
        this._applicationService.handleError(error)
      }
    )
  }

  onSave() {
    this._formLoading = true
    this._updateUserFormErrorMessages = ""

    if (this._user.email && this._user.first_name && this._user.last_name) {
      this._userService.updateUser(this._user.uuid, this._user).subscribe(
        (user) => {
          this._profileService.profile.firstName = user.first_name
          this._profileService.profile.lastName = user.last_name
          this._profileService.profile.avatar = user.avatar
          this._profileService.profile.small_avatar = user.small_avatar
          this._profileService.profile.predefined_avatar =
            user.predefined_avatar
          this._formLoading = false
          this._displayConfirmation = true
        },
        (error) => {
          this._formLoading = false
          this._displayConfirmation = false
          this._updateUserFormErrorMessages = error.error.message
        }
      )
    } else {
      this._formLoading = false
      this._displayConfirmation = false
    }
  }

  onUpdateEmail() {
    this._emailFormLoading = true
    this._changeEmailFormErrorMessages = ""
    this._userService.requestEmailReset(this._user).subscribe(
      () => {
        this._authService.logout()
      },
      (error) => {
        console.log(error.status)
        this._emailFormLoading = false
        if (error.status === 409) {
          this._changeEmailFormErrorMessages =
            "It appears you are trying to update your email address to your current one. Please enter a new email address."
        } else {
          this._changeEmailFormErrorMessages =
            "We're sorry, there was an error updating your email address. Please try again."
        }
      }
    )
  }

  onUpdatePassword() {
    this._updatePasswordLoading = true
    this._userService.requestPasswordReset(this._user).subscribe(
      () => {
        this._updatePasswordLoading = false
        this._displayUpdatePasswordConfirmation = true
      },
      (error) => {
        this._updatePasswordLoading = false
        this._displayUpdatePasswordConfirmation = false
        console.log(JSON.stringify(error.error))
        // this.applicationService.handleError(error);
      }
    )
  }

  onDeleteAccount() {
    this._deleteAccountLoading = true
    this._userService.deleteAccount(this._user).subscribe(
      () => {
        this._authService.logout()
      },
      (error) => {
        this._deleteAccountLoading = false
        this._deleteAccountErrorMessages = error.error.message
      }
    )
  }

  onAvatarChange(event) {
    if (event.target.files.length > 0) {
      const mimeType = event.target.files[0].type
      if (mimeType.match(/image\/*/) == null) {
        alert("Only images are supported.")
        return
      }

      const reader = new FileReader()
      reader.readAsDataURL(event.target.files[0])
      reader.onload = (_event) => {
        this._user.avatar_url = reader.result
      }

      this._user.avatar_file = event.target.files[0]
      this._user.predefined_avatar = "false"
    }
  }

  onAvatarSelect(avatar: string) {
    this._user.avatar_url = avatar
    this._user.small_avatar_url = avatar
    this._user.avatar_file = null
    this._user.predefined_avatar = "true"
  }

  handleShowDeleteAccountModal() {
    this._showDeleteAccountModal.next(true)
  }

  handleShowChangeEmailModal() {
    this._showChangeEmailModal.next(true)
  }

  closeDeleteAccountModal() {
    this._showDeleteAccountModal.next(false)
  }

  closeChangeEmailModal() {
    this._showChangeEmailModal.next(false)
  }

  private setPageMetaData(): void {
    this._pageTitle.setTitle("Manage Account - Adaptavist Library")
    this._meta.updateTag({
      name: "description",
      content: "Manage your Adaptavist Library account.",
    })
  }
}
