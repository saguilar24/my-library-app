import { Component, Input, OnInit } from "@angular/core"
import { BehaviorSubject } from "rxjs"
import { FilterGroupCollection } from "../../../services/search/filter-group.collection"

@Component({
  selector: "app-side-bar-partial",
  template: `
    <app-sliding-drawer [isOpen]="isMobileFilterMenuOpen">
      <ng-container *ngIf="isReady">
        <ng-container *ngFor="let filterGroup of filterGroups">
          <app-side-bar-list-partial
            [_filterGroup]="filterGroup"
          ></app-side-bar-list-partial>
        </ng-container>
        <app-more-filters-block [showMoreFilters]="showMoreFilters">
          <ng-container *ngFor="let filterGroup of moreFilterGroups">
            <app-side-bar-list-partial
              [_filterGroup]="filterGroup"
            ></app-side-bar-list-partial>
          </ng-container>
        </app-more-filters-block>
      </ng-container>
      <ng-container *ngIf="!isReady">
        <app-filter-bar-loading-partial></app-filter-bar-loading-partial>
      </ng-container>
    </app-sliding-drawer>
  `,
})
export class SideBarPartialComponent implements OnInit {
  @Input() isReady = false
  @Input() isMobileFilterMenuOpen: BehaviorSubject<boolean>
  @Input() showMoreFilters: BehaviorSubject<boolean>

  constructor() {}

  @Input() private _filterGroups: FilterGroupCollection[]

  get filterGroups(): FilterGroupCollection[] {
    return this._filterGroups
  }

  @Input() private _moreFilterGroups: FilterGroupCollection[]

  get moreFilterGroups(): FilterGroupCollection[] {
    return this._moreFilterGroups
  }

  ngOnInit() {}
}
