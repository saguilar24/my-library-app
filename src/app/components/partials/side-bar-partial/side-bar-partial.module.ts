import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { SideBarPartialComponent } from "./side-bar-partial.component"
import { RouterModule } from "@angular/router"
import { SideBarListPartialComponent } from "./side-bar-list-partial.component"
import { DummyContentModule } from "../dummy-content/dummy-content.module"
import { FiltersModule } from "../../lib/filters/filters.module"
import { NavigationModule } from "../../lib/navigation/navigation.module"
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FiltersModule,
    NavigationModule,
    DummyContentModule,
  ],
  exports: [SideBarPartialComponent],
  declarations: [SideBarPartialComponent, SideBarListPartialComponent],
})
export class SideBarPartialModule {}
