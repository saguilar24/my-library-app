import { Component, Input, OnInit } from "@angular/core"
import { FilterGroupCollection } from "../../../services/search/filter-group.collection"

@Component({
  selector: "app-side-bar-list-partial",
  template: `
    <h3 class="c-filter-menu--title">{{ filterGroup.name }}</h3>
    <ul class="c-filter-menu--list">
      <li
        *ngFor="let filter of filterGroup.filters"
        class="c-filter-menu--item"
      >
        <a
          routerLink="/search"
          [queryParams]="getParams(filterGroup.name, filter.slug)"
          class="c-filter-menu--link"
          [ngClass]="filter.isSelected ? 'active' : ''"
          >{{ filter.name }}</a
        >
      </li>
    </ul>
  `,
})
export class SideBarListPartialComponent implements OnInit {
  constructor() {}

  @Input() private _filterGroup: FilterGroupCollection

  get filterGroup(): FilterGroupCollection {
    return this._filterGroup
  }

  ngOnInit() {}

  getParams(slug: string, link: string) {
    return { [slug.toLowerCase()]: link }
  }
}
