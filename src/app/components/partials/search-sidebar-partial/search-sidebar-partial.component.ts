import { Component, Input } from "@angular/core"
import { environment } from "../../../../environments/environment"
import algoliasearch from "algoliasearch"
import {history} from "instantsearch.js/es/lib/routers";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {makeStateKey, TransferState} from "@angular/platform-browser";
import {createSSRSearchClient} from "angular-instantsearch";

const client = algoliasearch(
  environment.applicationID,
  environment.searchOnlyApiKey
)
const index = client.initIndex("library-content")

@Component({
  selector: "app-search-sidebar-partial",
  templateUrl: "./search-sidebar-partial.component.html",
  styleUrls: ["./search-sidebar-partial.component.scss"],
})
export class SearchSidebarPartialComponent {
  @Input("page") page = "search"

  config: any;

  constructor(
      private httpClient: HttpClient,
      private transferState: TransferState,
  ) {
    this.config = {
      indexName: environment.indexName,
      searchClient: createSSRSearchClient({
        appId: environment.applicationID,
        apiKey: environment.searchOnlyApiKey,
        makeStateKey,
        HttpHeaders,
        transferState: this.transferState,
        httpClient: this.httpClient,
      }),
      routing: {
        router: history({
          getLocation() {
            if (typeof window === 'undefined') {
              const url = environment.appUrl; // retrieved from the server context
              return new URL(url) as unknown as Location;
            }

            return window.location;
          },
        })
      }
    }
  }



  public show: boolean = false
  public buttonName: any = "+ More filters"

  public productNames: any
  public appNames: any
  public platformNames: any
  public typeNames: any
  public status: any
  public featureNames: any
  public tagNames: any

  refinementToggle() {
    this.show = !this.show

    // Button text change
    if (this.show) this.buttonName = "Close filters"
    else this.buttonName = "+ More filters"
  }

  async ngOnInit() {
    //These names are for the relevant fields in algolia, so need to be the same.
    this.productNames = await this.getFacetHits("product_names")
    this.appNames = await this.getFacetHits("app_pretty_names")
    this.platformNames = await this.getFacetHits("platforms")
    this.typeNames = await this.getFacetHits("type")
    this.status = await this.getFacetHits("verified")
    this.featureNames = await this.getFacetHits("feature_names")
    this.tagNames = await this.getFacetHits("tag_names")
  }

  async getFacetHits(attributeName: string) {
    const response = await index.searchForFacetValues(attributeName, "*")
    return response.facetHits
  }
}
