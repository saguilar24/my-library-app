import { ComponentFixture, TestBed } from "@angular/core/testing"

import { SearchSidebarPartialComponent } from "./search-sidebar-partial.component"

describe("SearchSidebarPartialComponent", () => {
  let component: SearchSidebarPartialComponent
  let fixture: ComponentFixture<SearchSidebarPartialComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchSidebarPartialComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSidebarPartialComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it("should create", () => {
    expect(component).toBeTruthy()
  })
})
