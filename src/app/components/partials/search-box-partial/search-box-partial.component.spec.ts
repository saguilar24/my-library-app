import { ComponentFixture, TestBed } from "@angular/core/testing"

import { SearchBoxPartialComponent } from "./search-box-partial.component"

describe("SearchBoxPartialComponent", () => {
  let component: SearchBoxPartialComponent
  let fixture: ComponentFixture<SearchBoxPartialComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchBoxPartialComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBoxPartialComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it("should create", () => {
    expect(component).toBeTruthy()
  })
})
