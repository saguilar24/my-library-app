import { Component, Inject, forwardRef, Optional } from "@angular/core"
import {
  TypedBaseWidget,
  NgAisInstantSearch,
  NgAisIndex,
} from "angular-instantsearch"

import connectSearchBox, {
  SearchBoxWidgetDescription,
  SearchBoxConnectorParams,
} from "instantsearch.js/es/connectors/search-box/connectSearchBox"

@Component({
  selector: "app-search-box-partial",
  template: `
    <div class="c-hero--background">
      <div class="l-page--wrapper">
        <div class="c-hero--search-wrapper">
          <input
            type="text"
            class="c-hero-search--text-input"
            placeholder="Search code and content"
            autocomplete="off"
            #input
            id="search-input"
            (keyup)="this.searchTerm = input.value"
            [value]="this.state ? this.state.query : ''"
            (keyup.enter)="
              this.state ? this.state.refine(this.searchTerm) : null
            "
          />
          <button
            class="c-hero--search-button"
            [ngClass]="textEntered() ? 'active' : ''"
            (click)="this.state.refine(this.searchTerm)"
          >
            <svg
              width="15px"
              height="15px"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 512 512"
            >
              <path
                fill="currentColor"
                d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"
              ></path>
            </svg>
          </button>
        </div>
      </div>
    </div>
  `,
})
export class SearchBoxPartialComponent extends TypedBaseWidget<
  SearchBoxWidgetDescription,
  SearchBoxConnectorParams
> {
  private _textEntered: Boolean = false
  textEntered(): Boolean {
    return this._textEntered
  }
  searchTerm = ""
  public state: SearchBoxWidgetDescription["renderState"] // Rendering options
  constructor(
    @Inject(forwardRef(() => NgAisIndex))
    @Optional()
    public parentIndex: NgAisIndex,
    @Inject(forwardRef(() => NgAisInstantSearch))
    public instantSearchInstance: NgAisInstantSearch
  ) {
    super("SearchBox")
  }
  ngOnInit() {
    this.createWidget(connectSearchBox, {
      // instance options
    })
    super.ngOnInit()
  }
}
