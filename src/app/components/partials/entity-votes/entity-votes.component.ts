import { Component, Input, OnInit } from "@angular/core"
import { EntityModel } from "../../../models/entity.model"
import { EntityService } from "../../../services/entity/entity.service"
import { ProfileService } from "../../../services/profile/profile.service"
import { AuthService } from "../../../services/auth/auth.service"
import { BehaviorSubject } from "rxjs"

@Component({
  selector: "app-entity-votes",
  templateUrl: "./entity-votes.component.html",
})
export class EntityVotesComponent implements OnInit {
  @Input() entity: EntityModel

  private _defaultVotes = 1
  private _showLogInModal = new BehaviorSubject<boolean>(false)
  private _hasUpVotes = false
  private _hasDownVotes = false
  private _upVotes = 0
  private _downVotes = 0

  constructor(
    private entityService: EntityService,
    public profileService: ProfileService,
    private authService: AuthService
  ) {}

  get upVotes(): number {
    return this._upVotes
  }

  get downVotes(): number {
    return this._downVotes
  }

  get hasUpVotes(): boolean {
    return this._hasUpVotes
  }

  get hasDownVotes(): boolean {
    return this._hasDownVotes
  }

  get totalVotes(): number {
    return this.upVotes - this.downVotes + this._defaultVotes
  }

  // In this context, "entity.slug" is equivalent to scriptId from GraphCms. See file "entity.service.ts" line 700
  upVote(entity: EntityModel) {
    if (this.hasDownVotes) {
      this.entityService.downvote(entity.slug).subscribe((e) => {
        this._downVotes = e.downvotes
        this._hasDownVotes = e.has_downvoted_entity
      })
    }
    this.entityService.upvote(entity.slug).subscribe(
      (e) => {
        this._upVotes = e.upvotes
        this._hasUpVotes = e.has_upvoted_entity
      },
      (error) => {
        console.log(JSON.stringify(error.error))
      }
    )
  }

  // In this context, "entity.slug" is equivalent to scriptId from GraphCms. See file "entity.service.ts" line 700
  downVote(entity: EntityModel) {
    if (this.hasUpVotes) {
      this.entityService.upvote(entity.slug).subscribe((e) => {
        this._upVotes = e.upvotes
        this._hasUpVotes = e.has_upvoted_entity
      })
    }
    this.entityService.downvote(entity.slug).subscribe(
      (e) => {
        this._downVotes = e.downvotes
        this._hasDownVotes = e.has_downvoted_entity
      },
      (error) => {
        console.log(JSON.stringify(error.error))
      }
    )
  }

  get showLogInModal(): BehaviorSubject<boolean> {
    return this._showLogInModal
  }

  handleShowLogInModal() {
    this._showLogInModal.next(true)
  }

  ngOnInit() {
    this._upVotes = this.entity.upvotes
    this._downVotes = this.entity.downvotes
    this._hasUpVotes = this.entity.has_upvoted_entity
    this._hasDownVotes = this.entity.has_downvoted_entity
  }
}
