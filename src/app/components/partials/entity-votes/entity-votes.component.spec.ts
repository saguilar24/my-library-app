import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing"

import { EntityVotesComponent } from "./entity-votes.component"

describe("EntityVotesComponent", () => {
  let component: EntityVotesComponent
  let fixture: ComponentFixture<EntityVotesComponent>

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [EntityVotesComponent],
      }).compileComponents()
    })
  )

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityVotesComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it("should create", () => {
    expect(component).toBeTruthy()
  })
})
