import { Component, OnInit } from "@angular/core"
import { Location } from "@angular/common"

@Component({
  selector: "app-manage-account-hero-partial",
  templateUrl: "./manage-account-hero-partial.component.html",
})
export class ManageAccountHeroPartial implements OnInit {
  constructor(private _location: Location) {}

  ngOnInit() {}

  get location(): Location {
    return this._location
  }
}
