import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core"
import { FilterGroupCollection } from "../../../services/search/filter-group.collection"

@Component({
  selector: "app-filter-bar-list-partial",
  template: `
    <ul class="c-filter-menu--list">
      <li
        *ngFor="let filter of filterGroup.filters"
        class="c-filter-menu--item"
      >
        <button
          *ngIf="filter.count !== 0"
          (click)="handleFilterToggle(filterGroup.name, filter.slug)"
          class="c-filter-menu--link"
          [ngClass]="filter.isSelected ? 'active' : ''"
        >
          {{ filter.name }} {{ filter.count ? "(" + filter.count + ")" : "" }}
        </button>
      </li>
    </ul>
  `,
})
export class FilterBarListPartialComponent implements OnInit {
  @Output() toggleFilter: EventEmitter<{ filterName: string; slug: string }> =
    new EventEmitter()

  constructor() {}

  @Input() private _filterGroup: FilterGroupCollection

  get filterGroup(): FilterGroupCollection {
    return this._filterGroup
  }

  ngOnInit() {}

  handleFilterToggle(filterName: string, slug: string) {
    this.toggleFilter.emit({ filterName, slug })
  }
}
