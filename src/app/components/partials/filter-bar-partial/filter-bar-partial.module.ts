import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { FilterBarPartialComponent } from "./filter-bar-partial.component"
import { FilterBarListPartialComponent } from "./filter-bar-list-partial.component"
import { DummyContentModule } from "../dummy-content/dummy-content.module"
import { FiltersModule } from "../../lib/filters/filters.module"
import { NavigationModule } from "../../lib/navigation/navigation.module"

@NgModule({
  imports: [CommonModule, FiltersModule, NavigationModule, DummyContentModule],
  exports: [FilterBarPartialComponent],
  declarations: [FilterBarPartialComponent, FilterBarListPartialComponent],
})
export class FilterBarPartialModule {}
