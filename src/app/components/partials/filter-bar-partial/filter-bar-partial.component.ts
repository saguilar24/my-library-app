import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core"
import { FilterGroupCollection } from "../../../services/search/filter-group.collection"
import { BehaviorSubject } from "rxjs"

@Component({
  selector: "app-filter-bar-partial",
  template: `
    <app-sliding-drawer [isOpen]="isMobileFilterMenuOpen">
      <ng-container *ngIf="isReady">
        <ng-container *ngFor="let filterGroup of filterGroups">
          <h3 class="c-filter-menu--title">{{ filterGroup.name }}</h3>
          <app-filter-bar-list-partial
            [_filterGroup]="filterGroup"
            (toggleFilter)="handleFilterToggle($event)"
          ></app-filter-bar-list-partial>
        </ng-container>
        <app-more-filters-block [showMoreFilters]="showMoreFilters">
          <ng-container *ngFor="let filterGroup of moreFilterGroups">
            <h3 class="c-filter-menu--title">{{ filterGroup.name }}</h3>
            <app-filter-bar-list-partial
              [_filterGroup]="filterGroup"
              (toggleFilter)="handleFilterToggle($event)"
            ></app-filter-bar-list-partial>
          </ng-container>
        </app-more-filters-block>
      </ng-container>
      <ng-container *ngIf="!isReady">
        <app-filter-bar-loading-partial></app-filter-bar-loading-partial>
      </ng-container>
    </app-sliding-drawer>
  `,
})
export class FilterBarPartialComponent implements OnInit {
  @Input() isMobileFilterMenuOpen: BehaviorSubject<boolean>
  @Input() showMoreFilters: BehaviorSubject<boolean>
  @Input() isReady = false
  @Output() toggleFilter: EventEmitter<{ filterName: string; slug: string }> =
    new EventEmitter()

  constructor() {}

  @Input() private _filterGroups: FilterGroupCollection[]

  get filterGroups(): FilterGroupCollection[] {
    return this._filterGroups
  }

  @Input() private _moreFilterGroups: FilterGroupCollection[]

  get moreFilterGroups(): FilterGroupCollection[] {
    return this._moreFilterGroups
  }

  ngOnInit() {}

  handleFilterToggle({ filterName, slug }) {
    this.toggleFilter.emit({ filterName, slug })
  }
}
