import { Component, OnInit } from "@angular/core"
import { UserService } from "../../../services/user/user.service"
import { Router, ActivatedRoute } from "@angular/router"
import { Title, Meta } from "@angular/platform-browser"
import { ApplicationService } from "../../../services/application/application.service"
import { UserModel } from "../../../models/user.model"

@Component({
  selector: "app-profile-page-hero-partial",
  templateUrl: "./profile-page-hero-partial.component.html",
})
export class ProfilePageHeroPartialComponent implements OnInit {
  constructor(
    private _router: Router,
    private _pageTitle: Title,
    private _meta: Meta,
    private _route: ActivatedRoute,
    private _userService: UserService,
    private _applicationService: ApplicationService
  ) {}

  private _user: UserModel
  private _displayCompleteYourProfile = false

  get user(): UserModel {
    return this._user
  }

  get displayCompleteYourProfile(): boolean {
    return this._displayCompleteYourProfile
  }
  public isVisible(attribute: string): boolean {
    if (
      this._user.public !== "" &&
      ((this._user.public && this._user.public.indexOf(attribute) > -1) ||
        (!this._user.public && this._user[attribute]))
    ) {
      return true
    }

    return false
  }

  ngOnInit() {
    this._route.params.subscribe(() => {
      const slug = this._route.snapshot.paramMap.get("uuid")
      this._userService.getUser(slug).subscribe(
        (u) => {
          this._user = u as UserModel
          this._user.avatar_url = this._user.avatar
          if (
            !this.dismissedEditProfilePrompt() &&
            !slug &&
            this.profileFieldsEmpty()
          ) {
            this._displayCompleteYourProfile = true
          }
          this.setPageMetaData()
        },
        (error) => {
          this._applicationService.handleError(error)
        }
      )
    })
  }

  public displayName(): string {
    if (!this.isVisible("first_name") && !this.isVisible("last_name")) {
      return "Anonymous User"
    }

    return this.isVisible("first_name")
      ? this.isVisible("last_name")
        ? `${this._user.first_name} ${this._user.last_name}`
        : `${this._user.first_name}`
      : this._user.last_name
  }

  public dismissEditProfilePrompt() {
    this._displayCompleteYourProfile = false
    localStorage.setItem("dismissedEditProfilePrompt", "true")
  }

  private setPageMetaData(): void {
    this._pageTitle.setTitle(`${this.displayName()} - Adaptavist Library`)
    this._meta.updateTag({
      name: "description",
      content: `View the profile of ${this.displayName()} on Adaptavist Library.`,
    })
  }

  private profileFieldsEmpty() {
    return (
      !this._user.first_name ||
      !this._user.last_name ||
      !this._user.email ||
      !this._user.job_title ||
      !this._user.company ||
      !this._user.location ||
      !this._user.linkedin
    )
  }

  private dismissedEditProfilePrompt() {
    return localStorage.getItem("dismissedEditProfilePrompt") === "true"
  }
}
