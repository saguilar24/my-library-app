import { Component, OnInit } from "@angular/core"

@Component({
  selector: "app-filter-bar-loading-partial",
  template: `
    <ng-container *ngFor="let i of dummyFilterGroups(4)">
      <h3 class="u-dummy-text-bar u-dummy-text-bar--filter-title"></h3>
      <ul class="c-filter-menu--list">
        <li class="c-filter-menu--item">
          <div class="u-dummy-text-bar u-dummy-text-bar--filter-link"></div>
        </li>
        <li class="c-filter-menu--item">
          <div class="u-dummy-text-bar u-dummy-text-bar--filter-link"></div>
        </li>
        <li class="c-filter-menu--item">
          <div class="u-dummy-text-bar u-dummy-text-bar--filter-link"></div>
        </li>
      </ul>
    </ng-container>
  `,
})
export class DummyMenuListComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  dummyFilterGroups(n) {
    return new Array(n)
  }
}
