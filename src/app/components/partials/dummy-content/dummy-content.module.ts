import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { DummyMenuListComponent } from "./dummy-menu-list.component"

@NgModule({
  imports: [CommonModule],
  exports: [DummyMenuListComponent],
  declarations: [DummyMenuListComponent],
})
export class DummyContentModule {}
