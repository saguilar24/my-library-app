import { Component, OnInit } from "@angular/core"
import { ProfileService } from "../../../services/profile/profile.service"
import { AuthService } from "../../../services/auth/auth.service"

@Component({
  selector: "app-announcement-banner-partial",
  templateUrl: "./announcement-banner-partial.component.html",
})
export class AnnouncementBannerPartialComponent implements OnInit {
  isVisible = true

  constructor(public auth: AuthService, public profile: ProfileService) {}

  ngOnInit() {
    const isDismissed = localStorage.getItem("announcementBannerDismissed")
    if (isDismissed) {
      this.isVisible = false
    }
  }

  dismissNotice() {
    this.isVisible = false
    localStorage.setItem("announcementBannerDismissed", "true")
  }
}
