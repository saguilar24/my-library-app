import { Component, OnInit } from "@angular/core"
import { AuthService } from "../../../services/auth/auth.service"
import { ProfileService } from "../../../services/profile/profile.service"

@Component({
  selector: "app-verify-account-message-bar",
  templateUrl: "./verify-account-message-bar.component.html",
})
export class VerifyAccountMessageBarComponent implements OnInit {
  constructor(public auth: AuthService, public profile: ProfileService) {}

  ngOnInit() {}
}
