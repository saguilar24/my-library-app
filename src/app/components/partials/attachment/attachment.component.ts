import {
  Component,
  Input,
  OnInit,
  Renderer2,
  ViewChild,
  ElementRef,
} from "@angular/core"
import { BehaviorSubject } from "rxjs"
import { AttachmentModel } from "../../../models/attachment.model"
import { CopyTextService } from "../../../services/copy/copy-text.service"
import { EntityService } from "../../../services/entity/entity.service"
import { Segment } from "../../../services/segment/segment.service"
import { ProfileService } from "../../../services/profile/profile.service"
import { UserService } from "../../../services/user/user.service"
import { FormGroup, FormControl } from "@angular/forms"

@Component({
  selector: "app-attachment",
  templateUrl: "./attachment.component.html",
})
export class AttachmentComponent implements OnInit {
  @Input() attachment$: BehaviorSubject<AttachmentModel>
  @Input() entityName
  @Input() private _showModal$: BehaviorSubject<boolean>
  @ViewChild("modalclose", { read: ElementRef, static: false })
  modalclose: ElementRef

  constructor(
    private _entityService: EntityService,
    private _copy: CopyTextService,
    private _profileService: ProfileService,
    private _userService: UserService,
    private renderer: Renderer2
  ) {}

  feedbackForm = new FormGroup({
    feedbackText: new FormControl(""),
  })

  private _copiedModal = new BehaviorSubject<boolean>(false)

  get copiedModal(): BehaviorSubject<boolean> {
    return this._copiedModal
  }

  private _codeCopied = false

  get codeCopied(): boolean {
    return this._codeCopied
  }

  private _fullscreen: Boolean = false

  get fullscreen(): Boolean {
    return this._fullscreen
  }

  private _feedbackRating: string

  get feedbackRating(): string {
    return this._feedbackRating
  }

  private _feedbackText: string

  get feedbackText(): string {
    return this._feedbackText
  }

  private _hasSubmittedFeedback: boolean = false

  get hasSubmittedFeedback(): boolean {
    return this._hasSubmittedFeedback
  }

  private _displayCopyPastePrompt: Boolean = false

  get displayCopyPastePrompt(): Boolean {
    return this._displayCopyPastePrompt
  }

  private _attachment: AttachmentModel

  get attachment(): AttachmentModel {
    return this._attachment
  }

  dismissModal() {
    this._copiedModal.next(false)
  }

  ngOnInit() {
    this.attachment$.subscribe((a) => {
      this._attachment = a
      if (!this.hasDismissedCopyPastePrompt()) {
        this._displayCopyPastePrompt = true
      }
    })
  }

  // Copy code from code box (CMD+C/CTRL+C)
  copyCodeSelection() {
    if (global.getSelection) {
      this._copy.copyTextToClipboard(global.getSelection().toString())

      this.displayNPSModal()
      this.incrementCopyCount()

      Segment.track("Code Copy (Selection)", {
        entity_name: this.entityName,
        entity_slug: this.attachment$.getValue().entity_slug,
      })
    }
  }

  // Copy code from Full Screen option
  copyCode() {
    this._copy.copyTextToClipboard(this.attachment$.getValue().data)
    this._codeCopied = true

    this.displayNPSModal()
    this.incrementCopyCount()

    Segment.track("Code Copy (Fullscreen CTA)", {
      entity_name: this.entityName,
      entity_slug: this.attachment$.getValue().slug,
    })

    this.renderer.selectRootElement(".u-attachment--buttons").focus()
    setTimeout(() => (this._codeCopied = false), 1500)
  }

  toggleFullscreen() {
    this._fullscreen = !this._fullscreen
  }

  runFocus(event) {
    let modalDismiss = this.modalclose.nativeElement
    modalDismiss.classList.add("hello")
  }

  checkFeedbackTextContent(event) {
    let textareaValue = event.target.value
    let label = event.target.labels[0]
    if (textareaValue == "") {
      label.classList.remove("c-modal--form-label-animated")
    } else {
      this.animateLabel(event)
    }
  }

  animateLabel(event) {
    let label = event.target.labels[0]
    label.classList.add("c-modal--form-label-animated")
  }

  submitFeedback(event) {
    const feedbackText = this.feedbackForm.get("feedbackText").value
    if (feedbackText !== "") {
      let button = <HTMLElement>document.getElementById("feedback-submit")
      let modalHolder = <HTMLElement>document.querySelector(".c-modal--holder")
      modalHolder.classList.add("c-modal--feedback-submitted")
      button.tabIndex = -1
      button.setAttribute("aria-hidden", "true")

      this._feedbackText = feedbackText
      this.trackFeedbackInSegment()

      setTimeout(() => this._copiedModal.next(false), 1500)
    }
  }

  dismissCopyPastePrompt() {
    localStorage.setItem("dismissedCopyPastePrompt", "true")

    if (this._profileService.isAuthenticated()) {
      this._userService
        .dismissModal(
          this._profileService.getDismissedModals(),
          "Copy Paste Prompt"
        )
        .subscribe(
          (response) => {},
          (error) => {}
        )
    }

    this._displayCopyPastePrompt = false
  }

  hasDismissedCopyPastePrompt() {
    // If the logged in user has dismissed the prompt (synced from Auth0), or a logged out user dismissed the prompt (in localstorage)
    return (
      this._profileService.getDismissedModals().includes("Copy Paste Prompt") ||
      localStorage.getItem("dismissedCopyPastePrompt") === "true"
    )
  }

  displayNPSModal() {
    const numberOfCopies =
      (parseInt(localStorage.getItem("copyCodeInteractionCount")) || 0) + 1
    localStorage.setItem("copyCodeInteractionCount", numberOfCopies.toString())

    // If copying for the first time, or on the 10th interaction
    // and the user hasn't already submitted feedback on this page
    if (
      (numberOfCopies === 1 || numberOfCopies % 10 === 0) &&
      !this._hasSubmittedFeedback
    ) {
      Segment.track("Feedback modal displayed", {})

      this._copiedModal.next(true)
    }
  }

  incrementCopyCount() {
    this._entityService
      .getCopyCountByScriptIdFromGraphCms(this.attachment$.getValue().slug)
      .then((result) =>
        this._entityService
          .incrementCopyCountForGraphCms(
            result.script.counter.id,
            result.script.counter.copyCount + 1
          )
          .then((result) =>
            this._entityService.publishCounterByIdForGraphCms(
              result.updateCounter.id
            )
          )
      )
  }

  closeAndTrack() {
    this.trackFeedbackInSegment()
    this._copiedModal.next(false)
  }

  trackFeedbackInSegment() {
    Segment.track("Feedback", {
      rating: this._feedbackRating,
      text: this._feedbackText,
    })
    this._hasSubmittedFeedback = true
  }

  animateFeedback(event) {
    let blob = <HTMLElement>document.querySelector(".c-modal--blob")
    let feedbackLevel = event.currentTarget.getAttribute("data-feedback-level")
    blob.style.left = event.currentTarget.offsetLeft + "px"
    blob.className = ""
    blob.classList.add("c-modal--blob", feedbackLevel)
  }

  toggleResponse(rating, event) {
    let modalContent = <HTMLElement>document.querySelector(".c-modal--pop-up")
    let textarea = <HTMLElement>document.getElementById("feedback-text")
    let button = <HTMLElement>document.getElementById("feedback-submit")
    modalContent.classList.add("c-modal--feedback-selected")
    textarea.tabIndex = 0
    textarea.setAttribute("aria-hidden", "false")
    button.tabIndex = 0
    button.setAttribute("aria-hidden", "false")

    this._feedbackRating = rating
  }
}
