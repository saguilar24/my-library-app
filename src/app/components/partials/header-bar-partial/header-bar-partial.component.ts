import { Component, OnInit, inject } from "@angular/core"
import { AuthService } from "../../../services/auth/auth.service"
import { ProfileService } from "../../../services/profile/profile.service"
import { ApplicationService } from "../../../services/application/application.service"

@Component({
  selector: "app-header-bar-partial",
  templateUrl: "./header-bar-partial.component.html",
})
export class HeaderBarPartialComponent implements OnInit {
  constructor(
    public appService: ApplicationService,
    public auth: AuthService,
    public profile: ProfileService
  ) {}

  ngOnInit() {}
}
