import { Component, Inject, forwardRef, Optional } from "@angular/core"
import { Router } from "@angular/router"
import {
  TypedBaseWidget,
  NgAisIndex, NgAisInstantSearch,
} from "angular-instantsearch"

import connectStats, {
  StatsWidgetDescription,
  StatsConnectorParams,
} from "instantsearch.js/es/connectors/stats/connectStats"

@Component({
  selector: "app-stats-partial",
  template: `
    <div *ngIf="state && state.query" class="c-filter-menu--results-container">
      <p class="l-margin-right--half-gutter l-margin-bottom--none">
        {{ state.nbHits }} results
      </p>
      <button
        class="c-filter-menu--results-tag l-margin-bottom--half-gutter"
        (click)="onClear()"
        *ngIf="state.query"
      >
        "{{ state.query }}"
      </button>
    </div>
  `,
  styleUrls: ["../stats-partial/stats-partial.component.scss"],
})
export class StatsPartialComponent extends TypedBaseWidget<
  StatsWidgetDescription,
  StatsConnectorParams
> {
  public state: StatsWidgetDescription["renderState"] // Rendering options
  constructor(
    @Inject(forwardRef(() => NgAisIndex))
    @Optional()
    public parentIndex: NgAisIndex,
    private router: Router
  ) {
    super("Stats")
  }
  ngOnInit() {
    this.createWidget(connectStats, {
      // instance options
    })
    super.ngOnInit()
  }

  onClear() {
    this.router.navigate(["/search"]).then(() => global.location.reload())
  }

  instantSearchInstance: NgAisInstantSearch;
}
