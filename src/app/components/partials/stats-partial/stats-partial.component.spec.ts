import { ComponentFixture, TestBed } from "@angular/core/testing"

import { StatsPartialComponent } from "./stats-partial.component"

describe("StatsPartialComponent", () => {
  let component: StatsPartialComponent
  let fixture: ComponentFixture<StatsPartialComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StatsPartialComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsPartialComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it("should create", () => {
    expect(component).toBeTruthy()
  })
})
