import { Component, Inject, forwardRef, Optional } from "@angular/core"
import { Router } from "@angular/router"
import {
  TypedBaseWidget,
  NgAisInstantSearch,
  NgAisIndex,
} from "angular-instantsearch"

import connectCurrentRefinements, {
  CurrentRefinementsWidgetDescription,
  CurrentRefinementsConnectorParams,
} from "instantsearch.js/es/connectors/current-refinements/connectCurrentRefinements"

@Component({
  selector: "app-current-refinements-partial",
  template: `
    <ng-container *ngIf="state">
      <div class="l-flex l-flex--wrap">
        <div class="l-flex" *ngFor="let item of state.items">
          <ng-container *ngFor="let refinement of item.refinements">
            <button
              class="c-filter-menu--results-tag l-margin-bottom--half-gutter"
              (click)="onClear(refinement)"
            >
              {{ refinement.label === "1" ? "Verified" : refinement.label }}
            </button>
          </ng-container>
        </div>
        <button
          *ngIf="state.items.length > 0"
          class="c-filter-menu--reset l-margin-bottom--half-gutter"
          (click)="clearAll()"
        >
          Reset
        </button>
      </div>
    </ng-container>
  `,
  styleUrls: [
    "../current-refinements-partial/current-refinements-partial.component.scss",
  ],
})
export class CurrentRefinementsPartial extends TypedBaseWidget<
  CurrentRefinementsWidgetDescription,
  CurrentRefinementsConnectorParams
> {
  public state: CurrentRefinementsWidgetDescription["renderState"] // Rendering options
  constructor(
    @Inject(forwardRef(() => NgAisIndex))
    @Optional()
    public parentIndex: NgAisIndex,
    @Inject(forwardRef(() => NgAisInstantSearch))
    public instantSearchInstance: NgAisInstantSearch,
    private router: Router
  ) {
    super("CurrentRefinements")
  }
  ngOnInit() {
    this.createWidget(connectCurrentRefinements, {
      // instance options
    })
    super.ngOnInit()
  }

  onClear(refinement) {
    this.state.refine(refinement)
  }

  clearAll() {
    this.router.navigate(["/search"]).then(() => global.location.reload())
  }
}
