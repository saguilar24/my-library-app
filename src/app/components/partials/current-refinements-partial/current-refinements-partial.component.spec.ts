import { ComponentFixture, TestBed } from "@angular/core/testing"

import { CurrentRefinementsPartial } from "./current-refinements-partial.component"

describe("CurrentRefinementsPartialComponent", () => {
  let component: CurrentRefinementsPartial
  let fixture: ComponentFixture<CurrentRefinementsPartial>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CurrentRefinementsPartial],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentRefinementsPartial)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it("should create", () => {
    expect(component).toBeTruthy()
  })
})
