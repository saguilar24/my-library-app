import { Component, Input } from "@angular/core"

@Component({
  selector: "app-hero-partners-apps",
  templateUrl: "./hero-partners-apps.component.html",
  styleUrls: [],
})
export class HeroPartnersAppsComponent {
  @Input() resource

  isAdaptavistUrl(url) {
    return url.includes("adaptavist")
  }
}
