import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { RouterModule } from "@angular/router"
import { HeroPartnersAppsComponent } from "./hero-partners-apps.component"

@NgModule({
  imports: [CommonModule, RouterModule],
  exports: [HeroPartnersAppsComponent],
  declarations: [HeroPartnersAppsComponent],
})
export class HeroPartnersAppsModule {}
