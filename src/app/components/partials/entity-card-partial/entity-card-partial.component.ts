import { Component, Input } from "@angular/core"
import { DataModel } from "../../../models/data.model"
import { CardModel } from "../../../models/card.model"

@Component({
  selector: "app-entity-card-partial",
  templateUrl: "./entity-card-partial.component.html",
})
export class EntityCardPartialComponent {
  @Input() card: DataModel<CardModel>
}
