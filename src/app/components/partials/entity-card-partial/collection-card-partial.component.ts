import { Component, Input } from "@angular/core"
import { DataModel } from "../../../models/data.model"
import { CardCollectionModel } from "../../../models/card-collection.model"

@Component({
  selector: "app-collection-card-partial",
  templateUrl: "./collection-card-partial.component.html",
})
export class CollectionCardPartialComponent {
  @Input() card: DataModel<CardCollectionModel>
}
