import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { EntityCardPartialComponent } from "./entity-card-partial.component"
import { RouterModule } from "@angular/router"
import { CollectionCardPartialComponent } from "./collection-card-partial.component"
import { CardsModule } from "../../lib/cards/cards.module"
import { TypographyModule } from "../../lib/typography/typography.module"

@NgModule({
  imports: [CommonModule, CardsModule, TypographyModule, RouterModule],
  exports: [EntityCardPartialComponent, CollectionCardPartialComponent],
  declarations: [EntityCardPartialComponent, CollectionCardPartialComponent],
})
export class EntityCardPartialModule {}
