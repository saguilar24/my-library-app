import { Component, Inject, forwardRef, Optional } from "@angular/core"
import {
  TypedBaseWidget,
  NgAisInstantSearch,
  NgAisIndex,
} from "angular-instantsearch"

import connectPagination, {
  PaginationWidgetDescription,
  PaginationConnectorParams,
} from "instantsearch.js/es/connectors/pagination/connectPagination"

@Component({
  selector: "app-pagination-partial",
  template: `
    <ng-container *ngIf="state">
      <div class="c-pagination-holder l-display--flex">
        <button
          (click)="state.refine(0)"
          [disabled]="state.isLastPage"
          class="c-button c-button--pagination"
        >
          First
        </button>
        <button
          (click)="state.refine(state.currentRefinement - 1)"
          [disabled]="state.isFirstPage"
          class="c-button c-button--pagination"
        >
          Prev
        </button>
        <button
          class="c-button c-button--pagination"
          [ngClass]="page === state.currentRefinement ? 'current' : ''"
          *ngFor="let page of state.pages"
          (click)="state.refine(page)"
        >
          <strong *ngIf="page === state.currentRefinement">{{
            page + 1
          }}</strong>
          <span *ngIf="page !== state.currentRefinement">{{ page + 1 }}</span>
        </button>
        <button
          (click)="state.refine(state.currentRefinement + 1)"
          [disabled]="state.isLastPage"
          class="c-button c-button--pagination"
        >
          Next
        </button>
        <button
          (click)="state.refine(state.nbPages - 1)"
          [disabled]="state.isLastPage"
          class="c-button c-button--pagination"
        >
          Last
        </button>
      </div>
    </ng-container>
  `,
})
export class PaginationPartialComponent extends TypedBaseWidget<
  PaginationWidgetDescription,
  PaginationConnectorParams
> {
  public state: PaginationWidgetDescription["renderState"] // Rendering options
  constructor(
    @Inject(forwardRef(() => NgAisIndex))
    @Optional()
    public parentIndex: NgAisIndex,
    @Inject(forwardRef(() => NgAisInstantSearch))
    public instantSearchInstance: NgAisInstantSearch
  ) {
    super("Pagination")
  }
  ngOnInit() {
    this.createWidget(connectPagination, {
      // instance options
    })
    super.ngOnInit()
  }
}
