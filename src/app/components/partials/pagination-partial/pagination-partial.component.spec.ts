import { ComponentFixture, TestBed } from "@angular/core/testing"

import { PaginationPartialComponent } from "./pagination-partial.component"

describe("PaginationPartialComponent", () => {
  let component: PaginationPartialComponent
  let fixture: ComponentFixture<PaginationPartialComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaginationPartialComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationPartialComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it("should create", () => {
    expect(component).toBeTruthy()
  })
})
