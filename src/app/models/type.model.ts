export class TypeModel {
  id: number
  name: string
  shortcode: string
}
