import { FilterModel } from "./filter.model"

export class FilterMenuModel {
  title: string
  slug: string
  filters: FilterModel[]
}
