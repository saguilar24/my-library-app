export class PagedDataModel {
  data: any
  meta: {
    page: number
    limit: number
    total: number
  }
}
