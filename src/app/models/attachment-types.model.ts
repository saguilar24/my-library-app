import { AttachmentModel } from "./attachment.model"

export class AttachmentTypesModel {
  server: AttachmentModel[]
  cloud: AttachmentModel[]
  dataCenter: AttachmentModel[]
}
