export class ProfileModel {
  sub: string
  email: string
  nickname: string
  firstName: string
  lastName: string
  roles: string[]
  email_verified: boolean
  avatar: string
  small_avatar: string
  predefined_avatar: string
  dismissed_modals: any
}
