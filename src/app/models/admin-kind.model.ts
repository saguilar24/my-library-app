export class AdminKindModel {
  id: number
  name: string
  shortcode: string
}
