export class AdminProductModel {
  id: number
  name: string
  shortcode: string
  icon: string
  file: string | any
  removeFile: boolean
}
