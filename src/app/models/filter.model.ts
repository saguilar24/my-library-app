export class FilterModel {
  text: string
  link: string
  isSelected?: boolean
}
