import { ProductModel } from "./product.model"
import { PlatformModel } from "./platform.model"
import { AppVersionsModel } from "./app-versions.model"
import { AttachmentTypesModel } from "./attachment-types.model"
import { PartnerModel } from "./partner.model"

export class ScriptModel {
  readonly class: string = "Entity"
  id: number
  name: string
  slug: string
  public?: boolean
  description?: string
  owner?: string
  products: ProductModel[]
  partners?: PartnerModel[]
  platforms: PlatformModel[]
  attachments: AttachmentTypesModel
  app_versions?: AppVersionsModel[]
  feature_names?: string[]
  tag_names?: string[]
  learn_more_url: string
  learn_more_text: string
  kind_name?: string
  type_name?: string
  category_name?: string
  updated_at: string
  attachment_updated_at: string
  created_at: string
  star_count: number
  subscribed?: boolean
  subscribed_at
  upvoted?: boolean
  upvotes: number
  has_upvoted_entity?: boolean
  downvoted?: boolean
  downvotes: number
  has_downvoted_entity?: boolean
}
