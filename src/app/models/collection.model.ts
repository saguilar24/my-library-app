import { EntityModel } from "./entity.model"
import { PartnerModel } from "./partner.model"
import { ProductModel } from "./product.model"
import { PlatformModel } from "./platform.model"
import { AppVersionsModel } from "./app-versions.model"
import { LinkModel } from "./link.model"

export class CollectionModel {
  readonly class: string = "Collection"
  id: number
  name: string
  slug: string
  image?: {
    src: string
    alt: string
  }
  description: string
  meta_description?: string
  partner: boolean
  numberOfEntities: number
  categories: LinkModel[]
  entities: EntityModel[]
  partners: PartnerModel[]
  updated_at?: string
  created_at: string
  more_by_owner?: CollectionModel[]
  removeFile: boolean
  app_versions?: AppVersionsModel[]
  creator: string
  type = "collection"
}
