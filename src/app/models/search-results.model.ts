import { EntityModel } from "./entity.model"

export interface AggModel {
  key: string
  doc_count: number
}

export interface SearchResultsModel {
  product_names: AggModel[]
  platform_names: AggModel[]
  tag_names: AggModel[]
  app_names: any[]
  feature_names: any[]
  kind_names: AggModel[]
  type_names: any[]
  category_names: AggModel[]
  current_page: number
  data: EntityModel[]
  first_page_url: string
  from: number
  last_page: number
  last_page_url: string
  next_page_url: string
  path: string
  per_page: number
  prev_page_url?: any
  to: number
  total: number
}
