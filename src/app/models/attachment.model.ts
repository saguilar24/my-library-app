import { RequirementModel } from "./requirement.model"

export class AttachmentModel {
  id: number
  data: string
  styledData?: string
  platform_id: number
  platform_name: string
  requirements: RequirementModel[]
  language?: string
  slug: string
  updated_at: string
  entity_slug: ""
  verified: boolean
}
