import { ImageModel } from "./image.model"

export class HomeBannerModel {
  title: string
  image: ImageModel
}
