export class SavedSearch {
  search_uuid: number
  publicUrl: string
  filters_and_term: string
  created_at: string
}
