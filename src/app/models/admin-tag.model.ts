export class AdminTagModel {
  id: number
  name: string
  shortcode: string
}
