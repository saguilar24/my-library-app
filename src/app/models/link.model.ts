export class LinkModel {
  text: string
  link: string
  params?: any
  isSelected?: boolean
}
