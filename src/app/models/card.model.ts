import { LinkModel } from "./link.model"
import { PartnerModel } from "./partner.model"
import { AppVersionsModel } from "./app-versions.model"
import { TypeModel } from "./type.model"

export class CardModel {
  id?: number
  scriptId?: string
  name: string
  description?: string
  meta_description?: string
  slug?: string
  categories?: LinkModel[]
  type: string | TypeModel
  created_at: string
}
