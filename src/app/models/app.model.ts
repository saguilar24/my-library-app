export class AppModel {
  id: number
  name: string
  pretty_name: string
  shortcode: string
  company_name: string
  url: string
  product_url: string
  icon: string
  file: string | any
  removeFile: boolean
}
