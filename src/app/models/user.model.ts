export class UserModel {
  uuid: number
  email: string
  new_email: string
  first_name: string
  last_name: string
  job_title?: string
  company?: string
  location?: string
  linkedin?: string
  public?: any

  email_visible?: boolean
  first_name_visible?: boolean
  last_name_visible?: boolean

  avatar: string | any
  small_avatar: string | any
  avatar_file: string | any
  avatar_url: string | any
  small_avatar_url: string | any
  predefined_avatar: string
}
