import { PlatformModel } from "./platform.model"
import { ProductModel } from "./product.model"
import { AppVersionsModel } from "./app-versions.model"
import { CollectionModel } from "./collection.model"
import { TypeModel } from "./type.model"
import { AttachmentTypesModel } from "./attachment-types.model"
import { PartnerModel } from "./partner.model"
import { AdminUserModel } from "./admin-user.model"

export class EntityModel {
  readonly class: string = "Entity"
  id: number
  name: string
  slug: string
  graphcms_id: string
  public = false
  description?: string
  meta_description?: string
  type: TypeModel | string
  products: ProductModel[]
  platforms: PlatformModel[]
  attachments: AttachmentTypesModel
  collections: CollectionModel[]
  partners?: PartnerModel[]
  partner?: boolean
  author?: AdminUserModel
  learn_more_url: string
  learn_more_text: string
  kind_name?: string
  type_name?: string
  app_versions?: AppVersionsModel[]
  category_name?: string
  tag_names?: string[]
  file: string | any
  feature_names?: string[]
  updated_at: string
  attachment_updated_at: string
  created_at: string
  star_count: number
  subscribed = false
  subscribed_at
  upvoted = false
  upvotes: number
  has_upvoted_entity = false
  downvoted = false
  downvotes: number
  has_downvoted_entity = false
  top_pick
}
