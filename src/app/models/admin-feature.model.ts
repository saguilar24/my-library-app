export class AdminFeatureModel {
  id: number
  name: string
  shortcode: string
}
