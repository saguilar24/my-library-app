export class SubscriptionEventModel {
  message: string
  star_count: number
  user_saved_total: number
}
