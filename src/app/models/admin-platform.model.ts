export class AdminPlatformModel {
  id: number
  name: string
  icon: string
  file: string | any
  removeFile: boolean
}
