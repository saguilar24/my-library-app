export class AdminPartnerModel {
  id: number
  name: string
  url: string
  product_name: string
  product_url: string
  icon: string
  file: string | any
  removeFile: boolean
}
