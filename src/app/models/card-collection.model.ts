import { ImageModel } from "./image.model"
import { CardModel } from "./card.model"
import { PartnerModel } from "./partner.model"
import { AppVersionsModel } from "./app-versions.model"

export class CardCollectionModel extends CardModel {
  creator: string
  image: ImageModel
  created_at: string
}
