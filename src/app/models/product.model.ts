export class ProductModel {
  id: number
  name: string
  shortcode: string
  icon: string
}
