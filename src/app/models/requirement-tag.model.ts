import { ImageModel } from "./image.model"

export class RequirementTagModel {
  text: string
  icon: ImageModel
}
