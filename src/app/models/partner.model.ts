export class PartnerModel {
  name: string
  url: string
  product_name: string
  product_url: string
  icon: string
}
