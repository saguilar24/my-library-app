export class PlatformModel {
  id: number
  name: string
  icon: string
}
