/* tslint:disable:max-line-length */
import { Inject, Injectable, PLATFORM_ID } from "@angular/core"
import { isPlatformBrowser } from "@angular/common"
import { HttpErrorResponse } from "@angular/common/http"
import { NavigationEnd, NavigationStart, Router } from "@angular/router"
import { environment } from "../../../environments/environment"
import { ProfileService } from "../profile/profile.service"
import { Segment } from "../segment/segment.service"

@Injectable({
  providedIn: "root",
})
export class ApplicationService {
  applicationWrapperClass: string
  routerOutletWrapperClass: string
  adminTemplate: boolean
  embedTemplate: boolean
  isProfileMenuOpen: boolean

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    public router: Router,
    private profile: ProfileService
  ) {}

  public isRunningOnServer(): boolean {
    return !isPlatformBrowser(this.platformId)
  }

  public isRunningInClientBrowser(): boolean {
    return isPlatformBrowser(this.platformId)
  }

  public primeSegmentAnalytics(): void {
    const body = document.getElementsByTagName("body")[0]

    const segmentJS = `
            !function(){var analytics=global.analytics||[];if(!analytics.initialize)if(analytics.invoked)global.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t,e){var n=document.createElement("script");n.type="text/javascript";n.async=!0;n.src="https://cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(n,a);analytics._loadOptions=e};analytics.SNIPPET_VERSION="4.1.0";
            analytics.page();
            }}();
        `
    const segmentScript = document.createElement("script")
    segmentScript.text = segmentJS
    body.appendChild(segmentScript)

    // Development and Staging Segment key
    let writeKey = "ozq09LoupQAbtxeyVvCOAae206glpxWM"

    if (environment.production) {
      // Production Segment key
      writeKey = "Be382NH8pmWA3haW4dW9tAZJoedotwlR"
    }

    const segmentConsentManagerJS = `
        global.consentManagerConfig = function(exports) {
            var React = exports.React

            // Quick switch for testing EU and non-EU
            //var isInEU = function() {
            //	return true;
            //}

            if (isInEU()) {
                var bannerContent = React.createElement(
                    'div',
                    { class: 'gdpr-cookie-icon' },
                    React.createElement(
                        'img',
                        { src: '/assets/img/cookie-star-grey.svg', class: 'gdpr-cookie-image' },
                    ),
                    React.createElement(
                        'div',
                        { class: 'gdpr-cookie-text' },
                        'This website uses cookies to give the best, most relevant experience and to analyse traffic. You can opt in to our ',
                        React.createElement(
                            'a',
                            { href: 'https://www.adaptavist.com/company/privacy-policy/', target: '_blank' },
                            'use of cookies'
                        ),
                        ' and ',
                        React.createElement(
                            'a',
                            { onClick: (e) => {consentManager.openConsentManager(); e.preventDefault();} },
                            'change your preferences'
                        ),
                        ' at any time.'
                    )
                )
            } else {
                var bannerContent = React.createElement(
                    'div',
                    { class: 'gdpr-cookie-icon' },
                    React.createElement(
                        'img',
                        { src: '/assets/img/cookie-star-grey.svg', class: 'gdpr-cookie-image' },
                    ),
                    React.createElement(
                        'div',
                        { class: 'gdpr-cookie-text' },
                        'This website uses cookies to give the best, most relevant experience and to analyse traffic. If you continue browsing, you consent to our ',
                        React.createElement(
                            'a',
                            { href: 'https://www.adaptavist.com/company/privacy-policy/', target: '_blank' },
                            'use of cookies'
                        ),
                        '. You can ',
                        React.createElement(
                            'a',
                            { onClick: (e) => {consentManager.openConsentManager(); e.preventDefault();} },
                            'change your preferences'
                        ),
                        ' at any time.'
                    )
                )
            }

            var bannerSubContent = 'You can change your preferences at any time.'
            var preferencesDialogTitle = 'Website Data Collection Preferences'
            var preferencesDialogContent =
            'We use data collected by cookies and JavaScript libraries to improve your browsing experience, analyse site traffic, deliver personalized advertisements, and increase the overall performance of our site.'
            var cancelDialogTitle = 'Are you sure you want to cancel?'
            var cancelDialogContent =
            'Your preferences have not been saved. By continuing to use our website, you՚re agreeing to our Website Data Collection Policy.'

            var initialPreferences = isInEU() ? {} : { advertising: true, marketingAndAnalytics: true, functional: true}
            var closeBehavior = isInEU() ? 'deny' : 'accept'

            return {
                container: '#gdpr-cookie-message',
                writeKey: '${writeKey}',
                bannerContent: bannerContent,
                shouldRequireConsent: function() {return true; },
                bannerSubContent: bannerSubContent,
                preferencesDialogTitle: preferencesDialogTitle,
                preferencesDialogContent: preferencesDialogContent,
                cancelDialogTitle: cancelDialogTitle,
                cancelDialogContent: cancelDialogContent,
                closeBehavior: closeBehavior,
                initialPreferences: initialPreferences
            }
        }
        `
    const segmentConsentManagerScript = document.createElement("script")
    segmentConsentManagerScript.text = segmentConsentManagerJS
    body.appendChild(segmentConsentManagerScript)

    const inEUScript = document.createElement("script")
    inEUScript.src = "/assets/js/inEU.min.js"
    body.appendChild(inEUScript)

    const segmentPackageScript = document.createElement("script")
    segmentPackageScript.src =
      "https://unpkg.com/@segment/consent-manager@4.0.0/standalone/consent-manager.js"
    body.appendChild(segmentPackageScript)
  }

  public primeNavigationEventsListener(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.scrollTop()
        this.resetClasses()
      } else if (event instanceof NavigationEnd) {
        // Only send Segment events if running within the Client's Browser and within Production env
        // `event.urlAfterRedirects` can be used to capture final NavigationEnd page URL variable
        if (this.isRunningInClientBrowser()) {
          // We need this check to ensure there's not a double call to Segment upon logging in
          if (event.url !== "/callback") {
            Segment.page({
              logged_in: this.profile.isAuthenticatedAndVerified(),
            })
          }
        }
      }
    })
  }

  public handleError(error: HttpErrorResponse): void {
    if (error.status === 404) {
      this.router.navigateByUrl("/error-404", { skipLocationChange: true })
    } else if (error.status === 401) {
      this.router.navigateByUrl("/error-401", { skipLocationChange: true })
    } else {
      this.router.navigateByUrl("/error-500", { skipLocationChange: true })
    }
  }

  public setAdminTemplate(): void {
    this.adminTemplate = true
  }

  public resetAdminTemplate(): void {
    this.adminTemplate = false
  }

  public isAdminTemplate(): boolean {
    return this.adminTemplate
  }

  public scrollTop(): void {
    // Only attempt to scroll the page via the window object if running within the Client's Browser
    if (this.isRunningInClientBrowser()) {
      document.body.scrollTop = 0
    }
  }

  profileMenuToggle() {
    this.isProfileMenuOpen = !this.isProfileMenuOpen
  }

  private resetClasses(): void {
    this.applicationWrapperClass = ""
    this.routerOutletWrapperClass = ""
    this.isProfileMenuOpen = false
  }
}
