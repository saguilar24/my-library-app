import { Injectable } from "@angular/core"
import { environment } from "../../../environments/environment"
import { Observable } from "rxjs"
import { EntityModel } from "../../models/entity.model"
import { map } from "rxjs/operators"
import { RequestService } from "../request/request.service"
import { ParameterModel } from "../../models/parameter.model"
import { UserModel } from "../../models/user.model"
import { HttpClient } from "@angular/common/http"
import { SavedSearch } from "../../models/saved-search.model"

@Injectable({
  providedIn: "root",
})
export class UserService {
  constructor(private request: RequestService, private _http: HttpClient) {}

  getUser(uuid?) {
    if (uuid) {
      return this._http.get(environment.apiUrl + "/v2/user?user_uuid=" + uuid)
    } else {
      return this._http.get(environment.apiUrl + "/v2/user")
    }
  }

  getUserAuthoredCode(uuid?): Observable<EntityModel[]> {
    if (uuid) {
      return this._http.get<EntityModel[]>(
        environment.apiUrl + "/v2/user/authored-entities?user_uuid=" + uuid
      )
    } else {
      return this._http.get<EntityModel[]>(
        environment.apiUrl + "/v2/user/authored-entities"
      )
    }
  }

  getUserMeta() {
    return this.getUserMetaRequest(`/v2/user/meta`)
  }

  getIsInLibrary(resourceType: string, resourceSlug: string) {
    return this.getUserMetaRequest(
      `/v2/user/in-library/${resourceType}/${resourceSlug}`
    )
  }

  updateUser(id: number, user): Observable<UserModel> {
    const formData = new FormData()

    if (user.email) {
      formData.append("email", user.email)
    }
    if (user.first_name) {
      formData.append("first_name", user.first_name)
    }
    if (user.last_name) {
      formData.append("last_name", user.last_name)
    }
    if (user.job_title) {
      formData.append("job_title", user.job_title)
    }
    if (user.company) {
      formData.append("company", user.company)
    }
    if (user.location) {
      formData.append("location", user.location)
    }
    if (user.linkedin) {
      formData.append("linkedin", user.linkedin)
    }

    formData.append("predefined_avatar", user.predefined_avatar)

    const visibleToPublic = []
    if (user.first_name_visible) {
      visibleToPublic.push("first_name")
    }
    if (user.last_name_visible) {
      visibleToPublic.push("last_name")
    }
    if (user.email_visible) {
      visibleToPublic.push("email")
    }
    formData.append("public", visibleToPublic.join())

    if (user.avatar_file) {
      formData.append("user_avatar", user.avatar_file)
    } else {
      formData.append("user_avatar", user.avatar_url)
      formData.append("user_small_avatar", user.small_avatar_url)
    }

    // Lumen doesn't handle http.put request when sending up formData. Send the data up as http.post and set the _method to PUT
    formData.append("_method", "PUT")

    return this._http.post<UserModel>(environment.apiUrl + "/v2/user", formData)
  }

  requestEmailReset(user: UserModel): Observable<UserModel> {
    const formData = new FormData()

    formData.append("email", user.new_email)

    // Lumen doesn't handle http.patch request when sending up formData. Send the data up as http.post and set the _method to PATCH
    formData.append("_method", "PATCH")

    return this._http.post<UserModel>(
      environment.apiUrl + "/v2/user/email/reset",
      formData
    )
  }

  requestPasswordReset(user: UserModel): Observable<UserModel> {
    return this._http.post<UserModel>(
      environment.apiUrl + "/v2/user/password/reset",
      ""
    )
  }

  deleteAccount(user: UserModel): Observable<UserModel> {
    return this._http.delete<UserModel>(environment.apiUrl + "/v2/user")
  }

  postSearchAlert(searchTerm: string, filters: any) {
    return this._http.post(environment.apiUrl + "/v2/user/search-alert", {
      searchTerm,
      filters,
    })
  }

  getSavedSearches(): Observable<SavedSearch[]> {
    return this._http.get<SavedSearch[]>(
      environment.apiUrl + "/v2/user/search-alert"
    )
  }

  removeSavedSearch(savedSearch: SavedSearch): Observable<SavedSearch[]> {
    return this._http.get<SavedSearch[]>(
      environment.apiUrl + "/user/search-alert/" + savedSearch.search_uuid
    )
  }

  dismissModal(
    dismissedModals: string[],
    modal: string
  ): Observable<UserModel> {
    const formData = new FormData()

    if (dismissedModals) {
      formData.append("dismissed_modals", dismissedModals + ", " + modal)
    } else {
      formData.append("dismissed_modals", modal)
    }

    // Lumen doesn't handle http.patch request when sending up formData. Send the data up as http.post and set the _method to PATCH
    formData.append("_method", "PATCH")

    return this._http.post<UserModel>(
      environment.apiUrl + "/v2/user/dismiss-modal",
      formData
    )
  }

  private getUserMetaRequest(
    endpoint,
    params: ParameterModel[] = null
  ): Observable<EntityModel[]> {
    return this.request
      .get<{ data: any }>(endpoint, params)
      .pipe(map((response) => response.data))
  }
}
