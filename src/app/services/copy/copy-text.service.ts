import { Injectable } from "@angular/core"

@Injectable()
export class CopyTextService {
  constructor() {}

  copyUrlToClipboard() {
    this.copyTextToClipboard(global.location.href)
  }

  copyTextToClipboard(text) {
    const dummy = document.createElement("textarea")

    document.body.appendChild(dummy)
    dummy.value = text
    dummy.select()
    document.execCommand("copy")
    document.body.removeChild(dummy)
  }
}
