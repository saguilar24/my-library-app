interface AuthConfig {
  clientID: string
  client_domain: string
  audience: string
  callbackURL: string
  loginActionFailRoute: string
  logoutActionRoute: string
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: "oeJGsH3q49J9GWBTTuOIKP8FQpdE0bKH",
  client_domain: "adaptavist.eu.auth0.com",
  audience: "https://dex-api.adaptavist.com/",
  callbackURL: "/callback",
  loginActionFailRoute: "/failed-login",
  logoutActionRoute: "/",
}
