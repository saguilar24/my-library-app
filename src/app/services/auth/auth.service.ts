import { Inject, Injectable, PLATFORM_ID } from "@angular/core"
import { isPlatformBrowser } from "@angular/common"
import { environment } from "../../../environments/environment"
import { AUTH_CONFIG } from "./auth0.config"
import * as auth0 from "auth0-js"
import { Segment } from "../segment/segment.service"
import { HttpClient } from "@angular/common/http"
import { ProfileService } from "../profile/profile.service"

@Injectable()
export class AuthService {
  private auth0 = new auth0.WebAuth({
    clientID: AUTH_CONFIG.clientID,
    domain: AUTH_CONFIG.client_domain,
    responseType: "token",
    redirectUri: environment.appUrl + AUTH_CONFIG.callbackURL,
    audience: AUTH_CONFIG.audience,
    scope: "openid profile email",
  })

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private http: HttpClient,
    private profile: ProfileService
  ) {}

  /**
   * Called during Angular's Initialization Process via APP_INITIALIZER within "app.module"
   */
  public init() {
    // If the application is running within the browser and the authService token hasn't expired, refresh it
    if (isPlatformBrowser(this.platformId) && this.profile.isAuthenticated()) {
      return this.renewToken()
    }
  }

  /**
   * Called from the "/callback" Page upon a User being returned to the app having completed the Auth0 authentication process
   */
  public handleAuthenticationCallback(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.auth0.parseHash((err, authResult) => {
        if (authResult && authResult.accessToken) {
          // Clear Auth0 hash from the URL
          global.location.hash = ""
          // Fetch User Profile details
          return resolve(this.authWrappedGetProfile(authResult))
        } else if (err) {
          this.clearSession()
          // @TODO: Improve Error Handling
          console.log(`Error handling authentication: ${err.error}`)
          reject(err)
        }

        resolve(AUTH_CONFIG.loginActionFailRoute)
      })
    })
  }

  public login(redirectTo = null): void {
    if (!this.profile.isAuthenticated()) {
      this.storeCurrentURL(redirectTo)
      this.auth0.authorize()
    }
  }

  public signUp(): void {
    if (!this.profile.isAuthenticated()) {
      this.storeCurrentURL()
      this.auth0.authorize({
        mode: "signUp",
      })
    }
  }

  public logout(): void {
    this.clearSession()

    this.auth0.logout({
      clientId: AUTH_CONFIG.clientID,
      returnTo: environment.appUrl + AUTH_CONFIG.logoutActionRoute,
    })
  }

  private renewToken(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.auth0.checkSession({}, (err, authResult) => {
        if (authResult && authResult.accessToken) {
          resolve(this.renewWrappedGetProfile(authResult))
        } else {
          this.clearSession()
          // @TODO: Improve Error Handling
          console.log(`Error refreshing session: ${err.error}`)
          reject(err)
        }
      })
    })
  }

  private async authWrappedGetProfile(authResult) {
    try {
      const nextPage = await this.getProfile(authResult)
      this.profile.getUserMeta().subscribe(this.identifyUser())
      return nextPage
    } catch (err) {
      console.log(`Error loading user meta: ${err}`)
    }
  }

  private identifyUser() {
    const profile = this.profile.profile
    return (data) =>
      Segment.identify(
        profile.sub,
        {
          identify: {
            userId: profile.sub,
            email: profile.email,
            firstName: profile.firstName,
            lastName: profile.lastName,
            nickname: profile.nickname,
          },
        },
        null,
        this.trackAuth(data)
      )
  }

  private trackAuth(data) {
    return () =>
      Segment.track("Authenticated", {
        script_count: data.entity_count,
        collection_count: data.collection_count,
      })
  }

  private renewWrappedGetProfile(authResult) {
    return new Promise((resolve, reject) => {
      this.getProfile(authResult)
        .then((nextPage) => resolve(nextPage))
        .catch((err) => reject(err))
    })
  }

  private getProfile(authResult): Promise<any> {
    return new Promise((resolve, reject) => {
      // Use Access Token to retrieve User's Auth0 Profile and then configure the User's session
      this.auth0.client.userInfo(authResult.accessToken, (err, profile) => {
        if (profile) {
          // After storing the User's profileService against the current session
          this.setSession(authResult)
          this.setProfileData(authResult, profile)
          // Resolve with the "Redirect To" URL when available for use as the "nextPage" within
          // the "/callback" route handler as appropriate
          const nextPage = localStorage.getItem("redirect_to")
          localStorage.removeItem("redirect_to") // Don't allow the value to persist beyond its first use
          resolve(nextPage)
        } else if (err) {
          // @TODO: Improve Error Handling
          console.log(`Error retrieving profile: ${err.error}`)
          reject(err)
        }
      })
    })
  }

  private setSession(authResult): void {
    const expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    )

    // Persist the Auth0 Access Token's "session expiry" timestamp within local storage
    localStorage.setItem("expires_at", expiresAt)
  }

  private setProfileData(authResult, profile) {
    this.profile.accessToken = authResult.accessToken
    this.profile.profile = profile
  }

  private clearSession(): void {
    localStorage.removeItem("expires_at")
    localStorage.removeItem("redirect_to")
  }

  private storeCurrentURL(redirectTo = null): void {
    if (redirectTo !== null) {
      // Retain the redirectTo URL so that the User may be redirected to the same page they left upon returning
      // from the Auth0 Authentication process
      localStorage.setItem("redirect_to", redirectTo)
    } else {
      // Retain the current URL so that the User may be redirected to the same page they left upon returning
      // from the Auth0 Authentication process
      let currentURLPath = global.location.pathname + global.location.search

      // However, ensure that a User is never returned to any of the following routes
      const blacklist = ["/callback", AUTH_CONFIG.loginActionFailRoute]

      if (blacklist.indexOf(currentURLPath) > -1) {
        currentURLPath = "/" // In these cases, simply return the User to the homepage route instead
      }

      localStorage.setItem("redirect_to", currentURLPath)
    }
  }
}
