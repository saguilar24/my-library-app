interface AuthConfig {
  clientID: string
  client_domain: string
  audience: string
  callbackURL: string
  loginActionFailRoute: string
  logoutActionRoute: string
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: "5dfXv5focjdSRmqQAagY9iRlxItKBVHL",
  client_domain: "adaptavist-id.eu.auth0.com",
  audience: "https://library-api.adaptavist.com/",
  callbackURL: "/callback",
  loginActionFailRoute: "/failed-login",
  logoutActionRoute: "/",
}
