import { Injectable } from "@angular/core"
import { environment } from "../../../../environments/environment"
import { Observable } from "rxjs"
import { HttpClient } from "@angular/common/http"
import { AppModel } from "../../../models/app.model"

@Injectable({
  providedIn: "root",
})
export class AdminAppService {
  constructor(private http: HttpClient) {}

  getApps(): Observable<AppModel[]> {
    return this.http.get<AppModel[]>(environment.apiUrl + "/apps")
  }

  createApp(data): Observable<AppModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("shortcode", data.shortcode)
    formData.append("removeFile", data.removeFile)
    formData.append("url", data.url)
    formData.append("company_name", data.company_name)
    formData.append("product_url", data.product_url)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    return this.http.post<AppModel>(environment.apiUrl + "/apps", formData)
  }

  updateApp(id: number, data): Observable<AppModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("pretty_name", data.pretty_name)
    formData.append("shortcode", data.shortcode)
    formData.append("removeFile", data.removeFile)
    formData.append("url", data.url)
    formData.append("company_name", data.company_name)
    formData.append("product_url", data.product_url)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    // Lumen doesn't handle http.put request when sending up formData. Send the data up as http.post and set the _method to PUT
    formData.append("_method", "PUT")

    return this.http.post<AppModel>(
      environment.apiUrl + "/apps/" + id + "?use-id=true",
      formData
    )
  }

  deleteApp(id: number): Observable<any> {
    return this.http.delete(environment.apiUrl + "/apps/" + id + "?use-id=true")
  }
}
