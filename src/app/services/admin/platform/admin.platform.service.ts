import { Injectable } from "@angular/core"
import { environment } from "../../../../environments/environment"
import { Observable } from "rxjs"
import { HttpClient } from "@angular/common/http"
import { AdminPlatformModel } from "../../../models/admin-platform.model"

@Injectable({
  providedIn: "root",
})
export class AdminPlatformService {
  constructor(private http: HttpClient) {}

  getPlatforms(): Observable<AdminPlatformModel[]> {
    return this.http.get<AdminPlatformModel[]>(
      environment.apiUrl + "/platforms"
    )
  }

  updatePlatform(id: number, data): Observable<AdminPlatformModel> {
    const formData = new FormData()

    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    // Lumen doesn't handle http.put request when sending up formData. Send the data up as http.post and set the _method to PUT
    formData.append("_method", "PUT")

    return this.http.post<AdminPlatformModel>(
      environment.apiUrl + "/platforms/" + id + "?use-id=true",
      formData
    )
  }

  deletePlatform(id: number): Observable<any> {
    return this.http.delete(
      environment.apiUrl + "/platforms/" + id + "?use-id=true"
    )
  }
}
