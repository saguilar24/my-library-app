import { Injectable } from "@angular/core"
import { environment } from "../../../../environments/environment"
import { Observable } from "rxjs"
import { HttpClient } from "@angular/common/http"
import { EntityModel } from "../../../models/entity.model"

@Injectable({
  providedIn: "root",
})
export class AdminEntityService {
  constructor(private http: HttpClient) {}

  getEntities(): Observable<EntityModel[]> {
    return this.http.get<EntityModel[]>(
      environment.apiUrl + "/v2/entity?limit=150"
    )
  }
}
