import { Injectable } from "@angular/core"
import { environment } from "../../../../environments/environment"
import { Observable } from "rxjs"
import { HttpClient } from "@angular/common/http"
import { AdminFeatureModel } from "../../../models/admin-feature.model"

@Injectable({
  providedIn: "root",
})
export class AdminFeatureService {
  constructor(private http: HttpClient) {}

  getFeatures(): Observable<AdminFeatureModel[]> {
    return this.http.get<AdminFeatureModel[]>(environment.apiUrl + "/features")
  }

  createFeature(data): Observable<AdminFeatureModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("shortcode", data.shortcode)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    return this.http.post<AdminFeatureModel>(
      environment.apiUrl + "/features",
      formData
    )
  }

  updateFeature(id: number, data): Observable<AdminFeatureModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("shortcode", data.shortcode)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    // Lumen doesn't handle http.put request when sending up formData. Send the data up as http.post and set the _method to PUT
    formData.append("_method", "PUT")

    return this.http.post<AdminFeatureModel>(
      environment.apiUrl + "/features/" + id + "?use-id=true",
      formData
    )
  }

  deleteFeature(id: number): Observable<any> {
    return this.http.delete(
      environment.apiUrl + "/features/" + id + "?use-id=true"
    )
  }
}
