import { Injectable } from "@angular/core"
import { environment } from "../../../../environments/environment"
import { Observable } from "rxjs"
import { HttpClient } from "@angular/common/http"
import { AdminKindModel } from "../../../models/admin-kind.model"

@Injectable({
  providedIn: "root",
})
export class AdminKindService {
  constructor(private http: HttpClient) {}

  getKinds(): Observable<AdminKindModel[]> {
    return this.http.get<AdminKindModel[]>(environment.apiUrl + "/kinds")
  }

  createKind(data): Observable<AdminKindModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("shortcode", data.shortcode)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    return this.http.post<AdminKindModel>(
      environment.apiUrl + "/kinds",
      formData
    )
  }

  updateKind(id: number, data): Observable<AdminKindModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("shortcode", data.shortcode)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    // Lumen doesn't handle http.put request when sending up formData. Send the data up as http.post and set the _method to PUT
    formData.append("_method", "PUT")

    return this.http.post<AdminKindModel>(
      environment.apiUrl + "/kinds/" + id + "?use-id=true",
      formData
    )
  }

  deleteKind(id: number): Observable<any> {
    return this.http.delete(
      environment.apiUrl + "/kinds/" + id + "?use-id=true"
    )
  }
}
