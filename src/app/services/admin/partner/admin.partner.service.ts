import { Injectable } from "@angular/core"
import { environment } from "../../../../environments/environment"
import { Observable } from "rxjs"
import { HttpClient } from "@angular/common/http"
import { AdminPartnerModel } from "../../../models/admin-partner.model"

@Injectable({
  providedIn: "root",
})
export class AdminPartnerService {
  constructor(private http: HttpClient) {}

  getPartners(): Observable<AdminPartnerModel[]> {
    return this.http.get<AdminPartnerModel[]>(environment.apiUrl + "/partners")
  }

  createPartner(data): Observable<AdminPartnerModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("url", data.url)
    formData.append("product_name", data.product_name)
    formData.append("product_url", data.product_url)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    return this.http.post<AdminPartnerModel>(
      environment.apiUrl + "/partners",
      formData
    )
  }

  updatePartner(id: number, data): Observable<AdminPartnerModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("url", data.url)
    formData.append("product_name", data.product_name)
    formData.append("product_url", data.product_url)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    // Lumen doesn't handle http.put request when sending up formData. Send the data up as http.post and set the _method to PUT
    formData.append("_method", "PUT")

    return this.http.post<AdminPartnerModel>(
      environment.apiUrl + "/partners/" + id + "?use-id=true",
      formData
    )
  }

  deletePartner(id: number): Observable<any> {
    return this.http.delete(
      environment.apiUrl + "/partners/" + id + "?use-id=true"
    )
  }
}
