import { Injectable } from "@angular/core"
import { environment } from "../../../../environments/environment"
import { Observable } from "rxjs"
import { EntityModel } from "../../../models/entity.model"
import { map } from "rxjs/operators"
import { RequestService } from "../../request/request.service"
import { ParameterModel } from "../../../models/parameter.model"
import { UserModel } from "../../../models/user.model"
import { HttpClient } from "@angular/common/http"
import { SavedSearch } from "../../../models/saved-search.model"
import { AdminUserModel } from "../../../models/admin-user.model"

@Injectable({
  providedIn: "root",
})
export class AdminUserService {
  constructor(private request: RequestService, private _http: HttpClient) {}

  getVerifiedUsers(): Observable<AdminUserModel[]> {
    return this._http.get<AdminUserModel[]>(
      environment.apiUrl + "/v2/user/verified"
    )
  }
}
