import { Injectable } from "@angular/core"
import { environment } from "../../../../environments/environment"
import { Observable } from "rxjs"
import { HttpClient } from "@angular/common/http"
import { AdminTagModel } from "../../../models/admin-tag.model"

@Injectable({
  providedIn: "root",
})
export class AdminTagService {
  constructor(private http: HttpClient) {}

  getTags(): Observable<AdminTagModel[]> {
    return this.http.get<AdminTagModel[]>(environment.apiUrl + "/tags")
  }

  createTag(data): Observable<AdminTagModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("shortcode", data.shortcode)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    return this.http.post<AdminTagModel>(environment.apiUrl + "/tags", formData)
  }

  updateTag(id: number, data): Observable<AdminTagModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("shortcode", data.shortcode)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    // Lumen doesn't handle http.put request when sending up formData. Send the data up as http.post and set the _method to PUT
    formData.append("_method", "PUT")

    return this.http.post<AdminTagModel>(
      environment.apiUrl + "/tags/" + id + "?use-id=true",
      formData
    )
  }

  deleteTag(id: number): Observable<any> {
    return this.http.delete(environment.apiUrl + "/tags/" + id + "?use-id=true")
  }
}
