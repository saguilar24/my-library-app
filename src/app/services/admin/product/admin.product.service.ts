import { Injectable } from "@angular/core"
import { environment } from "../../../../environments/environment"
import { Observable } from "rxjs"
import { HttpClient } from "@angular/common/http"
import { AdminProductModel } from "../../../models/admin-product.model"

@Injectable({
  providedIn: "root",
})
export class AdminProductService {
  constructor(private http: HttpClient) {}

  getProducts(): Observable<AdminProductModel[]> {
    return this.http.get<AdminProductModel[]>(environment.apiUrl + "/products")
  }

  createProduct(data): Observable<AdminProductModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("shortcode", data.shortcode)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    return this.http.post<AdminProductModel>(
      environment.apiUrl + "/products",
      formData
    )
  }

  updateProduct(id: number, data): Observable<AdminProductModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("shortcode", data.shortcode)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    // Lumen doesn't handle http.put request when sending up formData. Send the data up as http.post and set the _method to PUT
    formData.append("_method", "PUT")

    return this.http.post<AdminProductModel>(
      environment.apiUrl + "/products/" + id + "?use-id=true",
      formData
    )
  }

  deleteProduct(id: number): Observable<any> {
    return this.http.delete(
      environment.apiUrl + "/products/" + id + "?use-id=true"
    )
  }
}
