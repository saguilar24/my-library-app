import { HttpClient } from "@angular/common/http"
import { Observable, of } from "rxjs"
import { environment } from "../../../environments/environment"
import { Injectable } from "@angular/core"
import { TransferState } from "@angular/platform-browser"
import { tap } from "rxjs/operators"
import { ParameterModel } from "../../models/parameter.model"

@Injectable({ providedIn: "root" })
export class RequestService {
  constructor(protected http: HttpClient, private _state: TransferState) {}

  get<T>(endpoint, parameters: ParameterModel[] = null): Observable<T> {
    return this.http.get<T>(
      `${environment.apiUrl}${endpoint}${this.createParamString(parameters)}`
    )
  }

  post<T>(
    endpoint,
    data: any,
    parameters: ParameterModel[] = null
  ): Observable<T> {
    return this.http.post<T>(
      `${environment.apiUrl}${endpoint}${this.createParamString(parameters)}`,
      data
    )
  }

  getSSR<T>(
    stateKey,
    endpoint,
    parameters: ParameterModel[] = null
  ): Observable<T> {
    const data = this._state.get(stateKey, null as any)
    return data
      ? of(data)
      : this.get<T>(endpoint, parameters).pipe(
          tap((response) => this._state.set(stateKey, response))
        )
  }

  createParamString(params: ParameterModel[]): string {
    return params
      ? params.reduce((str, p, i) => {
          str += i !== 0 ? "&" : ""
          str += p.value ? `${p.key}=${p.value}` : p.key
          return str
        }, "?")
      : ""
  }
}
