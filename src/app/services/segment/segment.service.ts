export class Segment {
  static identify(id, data, options = null, callback = null) {
    return (window as any).analytics.identify(id, data, options, callback)
  }

  static track(name, data) {
    return (window as any).analytics.track(name, data)
  }

  static page(data) {
    return (window as any).analytics.page(data)
  }
}
