import { Injectable } from "@angular/core"
import { SearchResultsModel } from "../../models/search-results.model"
import { FilterService } from "./filter.service"
import { slugify } from "../../utilities/slugify"
import { tap } from "rxjs/operators"
import { BehaviorSubject, of } from "rxjs"
import { RequestService } from "../request/request.service"
import { makeStateKey } from "@angular/platform-browser"

@Injectable({
  providedIn: "root",
})
export class FilterMenuService {
  private _discoverFilterNames = [
    "Most Popular",
    "Most Viewed",
    "Top Picks",
    "Recently Updated",
    "Recently Created",
  ]
  private _filterList = [
    { name: "Apps", key: "app_names" },
    { name: "Products", key: "product_names" },
    { name: "Platforms", key: "platform_names" },
    { name: "Types", key: "type_names" },
    { name: "Status", key: "status_names" },
    { name: "Features", key: "feature_names" },
    { name: "Tags", key: "tag_names" },
  ]

  constructor(
    private _filterService: FilterService,
    private _request: RequestService
  ) {}

  private readonly _showMoreFilters: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(false)

  get showMoreFilters(): BehaviorSubject<boolean> {
    return this._showMoreFilters
  }

  private _isReady = false

  get isReady(): boolean {
    return this._isReady
  }

  set isReady(value: boolean) {
    this._isReady = value
  }

  get filterService(): FilterService {
    return this._filterService
  }

  public init() {
    if (this._filterService.filterGroups.length) {
      return of({}).pipe(tap(() => (this._isReady = true)))
    }
    return this.getAvailableFiltersFromElastic()
  }

  updateCounts(response) {
    if (!this._filterService.filterGroups.length) {
      return
    }
    this._filterList.map((fl) =>
      this._filterService.getFilterGroup(fl.name).filters.map((f) => {
        const filterRow = response[fl.key].find(
          (filter) => filter.key === f.name
        )
        f.count = filterRow ? filterRow.doc_count : 0
      })
    )
  }

  private getAvailableFiltersFromElastic() {
    return this._request
      .getSSR<SearchResultsModel>(makeStateKey("FILTERS_KEY"), "/search")
      .pipe(
        tap(() => (this._isReady = true)),
        tap((response) => this.initSearchFilterService(response))
      )
  }

  private initSearchFilterService(response) {
    this.addDiscoverFilterGroup()
    this.addFilterGroups(response)
  }

  private addDiscoverFilterGroup() {
    this._filterService.addFilterGroup(
      "Discover",
      this._discoverFilterNames.map((name) => this.makeFilterItem(name)),
      true
    )
  }

  private addFilterGroups(response) {
    this._filterList.map((filter) => {
      if (response.hasOwnProperty(filter.key) && response[filter.key].length) {
        this._filterService.addFilterGroup(
          filter.name,
          this.getFilterData(response, filter.key),
          false
        )
      }
    })
  }

  private makeFilterItem(name, count = null) {
    return {
      name,
      count,
      slug: slugify(name),
    }
  }

  private getFilterData(response, filterKey) {
    return response[filterKey].map((menuItems) =>
      this.makeFilterItem(menuItems.key, menuItems.doc_count)
    )
  }
}
