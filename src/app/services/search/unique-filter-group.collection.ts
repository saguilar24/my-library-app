import { FilterGroupCollection } from "./filter-group.collection"

export class UniqueFilterGroupCollection extends FilterGroupCollection {
  selectFilter(slug: string): boolean {
    try {
      this._filters.map((f) => (f.isSelected = f.slug === slug))
    } catch (e) {
      return false
    }
    return true
  }

  deselectFilter(slug: string): boolean {
    try {
      this._filters.map((f) => (f.isSelected = false))
    } catch (e) {
      return false
    }
    return true
  }

  toggleFilter(slug: string): boolean {
    try {
      this._filters.map(
        (f) => (f.isSelected = f.slug === slug ? !f.isSelected : false)
      )
    } catch (e) {
      return false
    }
    return true
  }
}
