export class FilterGroupCollection {
  constructor(name) {
    this._name = name
    this._filters = []
  }

  protected _filters: {
    name: string
    slug: string
    isSelected: boolean
    count?: number
  }[]

  get filters(): {
    name: string
    slug: string
    isSelected: boolean
    count?: number
  }[] {
    return this._filters
  }

  private _name: string

  get name(): string {
    return this._name
  }

  set name(value: string) {
    this._name = value
  }

  hasSelectedFilters(): boolean {
    return typeof this.filters.find((f) => f.isSelected) !== "undefined"
  }

  addFilter(filter: { name: string; slug: string; count?: number }) {
    if (!this.hasFilter(filter.name)) {
      this._filters.push({ ...filter, isSelected: false })
    }
  }

  hasFilter(name: string): boolean {
    return this._filters.some((f) => f.name === name)
  }

  selectFilter(slug: string): boolean {
    try {
      const filter = this._filters.find((f) => f.slug === slug)
      filter.isSelected = true
    } catch (e) {
      return false
    }
    return true
  }

  deselectFilter(slug: string): boolean {
    try {
      const filter = this._filters.find((f) => f.slug === slug)
      filter.isSelected = false
    } catch (e) {
      return false
    }
    return true
  }

  toggleFilter(slug: string): boolean {
    try {
      const filter = this._filters.find((f) => f.slug === slug)
      filter.isSelected = !filter.isSelected
    } catch (e) {
      return false
    }
    return true
  }

  // Todo: Could move parameter handlers somewhere else
  getParameterStringWithNames() {
    return `${this._name.toLowerCase()}=${this.createSelectedFilterStringFromNames()}`
  }

  createSelectedFilterString(key) {
    return this._filters
      .filter((f) => f.isSelected)
      .reduce((str, f, i) => str + (i === 0 ? "" : ",") + f[key], "")
  }

  createSelectedFilterStringFromSlugs() {
    return this.createSelectedFilterString("slug")
  }

  createSelectedFilterStringFromNames() {
    return this.createSelectedFilterString("name")
  }

  getSelected() {
    return this._filters.filter((f) => f.isSelected)
  }

  getSelectedFilterNamesForSegment() {
    if (!this.hasSelectedFilters()) {
      return null
    }
    const selected = this.getSelected().map((f) => f.name)
    return selected.length === 1 ? selected[0] : selected
  }

  clearFilters() {
    return (this._filters = this._filters.map((f) => ({
      ...f,
      isSelected: false,
    })))
  }
}
