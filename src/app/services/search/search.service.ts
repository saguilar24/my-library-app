import { Injectable } from "@angular/core"
import { BehaviorSubject } from "rxjs"

@Injectable()
export class SearchService {
  private _term$: BehaviorSubject<string> = new BehaviorSubject<string>("")

  get term$(): BehaviorSubject<string> {
    return this._term$
  }

  get term(): string {
    return this.term$.getValue() || ""
  }

  hasTerm(): boolean {
    return !!this.term$.getValue()
  }

  initTerm(params: { paramName: string; value: string }[]): void {
    this.updateTerm(this.getTermFromParams(params))
  }

  getSearchTermQueryString(): string {
    return this.term$.getValue() ? `term=${this.term$.getValue()}` : ""
  }

  clearSearchTerm(): void {
    this.term$.next("")
  }

  getTermFromParams(params: { paramName: string; value: string }[]): string {
    const searchTerm = params.find((p) => p.paramName === "term")
    if (typeof searchTerm !== "undefined") {
      return searchTerm.value
    }
    return ""
  }

  updateTerm(term): void {
    this._term$.next(decodeURIComponent(term))
  }

  getParams(): { term: string } {
    return this.term$.getValue() ? { term: this.term$.getValue() } : null
  }
}
