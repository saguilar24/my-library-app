import { Injectable } from "@angular/core"
import { FilterGroupCollection } from "./filter-group.collection"
import { UniqueFilterGroupCollection } from "./unique-filter-group.collection"

@Injectable({
  providedIn: "root",
})
export class FilterService {
  private _filterGroups: FilterGroupCollection[] = []

  get filterGroups(): FilterGroupCollection[] {
    return this._filterGroups
  }

  get topFilterGroups(): FilterGroupCollection[] {
    return this._filterGroups.filter((fg, i) => i <= 3)
  }

  get moreFilterGroups(): FilterGroupCollection[] {
    return this._filterGroups.filter((fg, i) => i > 3)
  }

  addFilterGroup(
    name,
    filters: { name: string; slug: string; count?: number }[],
    isUnique
  ) {
    let filterGroup = this.getFilterGroup(name)
    if (!filterGroup) {
      filterGroup = isUnique
        ? new UniqueFilterGroupCollection(name)
        : new FilterGroupCollection(name)
      this._filterGroups.push(filterGroup)
    }
    filters.map((f) => filterGroup.addFilter(f))

    return this
  }

  hasAnySelectedFilters() {
    // Todo: This loop can be avoided if we keep count of the selected filters when they are toggled/handleReset.
    return (
      typeof this.filterGroups.find((fg) => fg.hasSelectedFilters()) !==
      "undefined"
    )
  }

  resetFilters() {
    this._filterGroups = []
  }

  getFilterGroup(name: string): FilterGroupCollection {
    return this._filterGroups.find((f) => f.name === name)
  }

  clearFilterGroups(): void {
    this._filterGroups.map((fg) => fg.clearFilters())
  }

  hasFilterGroup(name: string): boolean {
    return this._filterGroups.some((f) => f.name === name)
  }

  getQueryStringWithNames(): string {
    return this._filterGroups.reduce(
      (str, fg) =>
        fg.hasSelectedFilters()
          ? str + (str === "" ? "" : "&") + fg.getParameterStringWithNames()
          : str,
      ""
    )
  }

  getSelectedFilterSlugs() {
    return this._filterGroups
      .filter((fg) => fg.hasSelectedFilters())
      .reduce(
        (obj, fg) => ({
          ...obj,
          [fg.name.toLowerCase()]: fg.createSelectedFilterStringFromSlugs(),
        }),
        {}
      )
  }

  getSelectedFilterNames() {
    return this._filterGroups
      .filter((fg) => fg.hasSelectedFilters())
      .reduce(
        (obj, fg) => ({
          ...obj,
          [fg.name.toLowerCase()]: fg.createSelectedFilterStringFromNames(),
        }),
        {}
      )
  }

  getSelectedFilterNamesAndSlugs(): { name: string; slug: string }[] {
    return this.filterGroups.reduce(
      (arr, fg) => [
        ...arr,
        ...fg.getSelected().map((f) => ({ name: f.name, slug: f.slug })),
      ],
      []
    )
  }

  selectFiltersFromQueryParams(params) {
    this.filterGroups.map((fg) => {
      params
        .filter((p) => fg.name.toLowerCase() === p.paramName)
        .map((p) => p.value.split(",").map((f) => fg.selectFilter(f)))
    })
  }

  getSelectedNamesForFilterGroup(name) {
    const filterGroup = this.getFilterGroup(name)
    return filterGroup ? filterGroup.getSelectedFilterNamesForSegment() : null
  }

  // Todo: FilterGroup 'Types' should be 'Type'. Then we can use this for method for segment tracking
  getSelectedFiltersForSegment() {
    return this._filterGroups.reduce(
      (obj, fg) => ({
        ...obj,
        [fg.name.toLowerCase()]: this.getSelectedNamesForFilterGroup(fg.name),
      }),
      {}
    )
  }
}
