import { Inject, Injectable, PLATFORM_ID } from "@angular/core"
import { isPlatformBrowser } from "@angular/common"
import { CardModel } from "../../models/card.model"

@Injectable({
  providedIn: "root",
})
export class RecentEntitiesService {
  private recentEntities: CardModel[] = []
  private maxEntities = 5

  constructor(@Inject(PLATFORM_ID) private platformId) {
    this.getFromLocalStorage()
  }

  add(entity: CardModel) {
    const existingCardIndex = this.recentEntities.findIndex(
      (re) => re.id === entity.id
    )
    if (existingCardIndex !== -1) {
      this.recentEntities.splice(existingCardIndex, 1)
    }
    this.recentEntities.unshift(entity)
    if (this.recentEntities.length > this.maxEntities) {
      this.recentEntities.pop()
    }
    this.saveToLocalStorage()
  }

  getN(n: number) {
    n = n || 4
    return this.recentEntities.slice(1, 1 + n)
  }

  private saveToLocalStorage() {
    if (!isPlatformBrowser(this.platformId)) {
      return
    }

    localStorage.setItem("recentEntities", JSON.stringify(this.recentEntities))
  }

  private getFromLocalStorage() {
    if (!isPlatformBrowser(this.platformId)) {
      return
    }

    this.recentEntities =
      (JSON.parse(localStorage.getItem("recentEntities")) as CardModel[]) || []
  }
}
