import { Injectable } from "@angular/core"
import { HttpClient, HttpHeaders } from "@angular/common/http"
import { Observable } from "rxjs"
import { map } from "rxjs/operators"
import { EntityModel } from "../../models/entity.model"
import { RequestService } from "../request/request.service"
import { environment } from "../../../environments/environment"
import { ParameterModel } from "../../models/parameter.model"
import { ScriptModel } from "../../models/script.model"
import { makeStateKey } from "@angular/platform-browser"
import { gql, GraphQLClient } from "graphql-request"
import dayjs from "dayjs"
import relativeTime from "dayjs/plugin/relativeTime"
import { markdown } from "markdown"
import { AppVersionsModel } from "../../models/app-versions.model"
import { PlatformModel } from "../../models/platform.model"
import { AttachmentTypesModel } from "../../models/attachment-types.model"
import { RequirementModel } from "../../models/requirement.model"
import _ from "lodash"
import flatten from "flat"

const headers = { authorization: `Bearer ${environment.graphcmsToken}` }
const graphqlClient = new GraphQLClient(environment.graphcmsEndpoint, {
  headers,
})

@Injectable({
  providedIn: "root",
})
export class EntityService {
  constructor(private _request: RequestService, private _http: HttpClient) {}

  getEntityItem(slug: string): Observable<EntityModel> {
    return this.getEntityItemRequest(`/v2/entity/${slug}`, [
      { key: "with", value: "type,collections,partners" },
      {
        key: "merge",
        value:
          "platforms,products,attachments,feature_names,tag_names,app_versions",
      },
    ])
  }

  getEntityList(
    limit: number = 5,
    orderBy: string = "updated_at",
    direction: string = "DESC"
  ): Observable<EntityModel[]> {
    return this.getEntityListRequest("/v2/entity", [
      { key: "limit", value: limit.toString() },
      { key: "order", value: orderBy },
      { key: "direction", value: direction },
    ])
  }

  getEntityListWithPartnersAndAuthor(
    limit: number = 5,
    orderBy: string = "name",
    direction: string = "asc"
  ): Observable<EntityModel[]> {
    return this.getEntityListRequest("/v2/entity", [
      { key: "with", value: "type,collections,partners,author" },
      { key: "order", value: orderBy },
      { key: "direction", value: direction },
      { key: "limit", value: limit.toString() },
    ])
  }

  getNewestProductsAndPlatformEntityList(
    products: string[],
    platform: string,
    limit: number = 3
  ): Observable<EntityModel[]> {
    return this.getSSREntityListRequest(
      makeStateKey(
        `newest_product_platform_entity_list_${products.join(
          "_"
        )}_${platform}_${limit}`
      ),
      `/v2/entity/newest`,
      [
        { key: "products", value: products.join(",") },
        { key: "platform", value: platform },
        { key: "limit", value: limit.toString() },
      ]
    )
  }

  getNewestInAppsEntityList(
    apps: string[],
    limit: number = 3
  ): Observable<EntityModel[]> {
    return this.getSSREntityListRequest(
      makeStateKey(`newest_in_apps_entity_list_${apps.join("_")}_${limit}`),
      `/v2/entity/newest`,
      [
        { key: "apps", value: apps.join(",") },
        { key: "limit", value: limit.toString() },
      ]
    )
  }

  createEntity(data): Observable<EntityModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("slug", data.slug)
    formData.append("description", data.description)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    return this._http.post<EntityModel>(
      environment.apiUrl + "/entity",
      formData
    )
  }

  deleteEntity(id: number): Observable<any> {
    return this._http.delete(
      environment.apiUrl + "/entity/" + id + "?use-id=true"
    )
  }

  getPopularInAppEntityList(app: string): Observable<EntityModel[]> {
    return this.getSSREntityListRequest(
      makeStateKey(`popular_in_app_entity_list_${app}`),
      `/v2/entity/popular/${app}`,
      [{ key: "with", value: "type" }]
    )
  }

  getRelatedEntityList(slug: string): Observable<EntityModel[]> {
    return this.getSSREntityListRequest(
      makeStateKey(`related_entities_for_${slug}`),
      `/v2/entity/related/${slug}`,
      [{ key: "with", value: "type" }]
    )
  }

  getMyEntityList(type: string): Observable<EntityModel[]> {
    return this.getEntityListRequest("/v2/user/entity", [
      { key: "type", value: type },
    ])
  }

  getWatchedEntityList(): Observable<EntityModel[]> {
    return this.getEntityListRequest("/v2/user/watch/entity", [])
  }

  updateEntity(id: number, data): Observable<EntityModel> {
    const formData = new FormData()

    formData.append("top_pick", data.top_pick ? "1" : "0")
    formData.append("meta_description", data.meta_description)

    // Lumen doesn't handle http.put request when sending up formData. Send the data up as http.post and set the _method to PUT
    formData.append("_method", "PUT")

    return this._http.post<EntityModel>(
      environment.apiUrl + "/v2/entity/" + id + "?use-id=true",
      formData
    )
  }

  postAddToMyLibrary(slug) {
    return this._http.post(
      `${environment.apiUrl}/v2/user/entity/${slug}`,
      null,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      }
    )
  }

  deleteAddToMyLibrary(slug) {
    return this._http.delete(`${environment.apiUrl}/v2/user/entity/${slug}`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
    })
  }

  postWatchEntity(slug) {
    return this._http.post(
      `${environment.apiUrl}/v2/user/watch/entity/${slug}`,
      null,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      }
    )
  }

  deleteWatchEntity(slug) {
    return this._http.delete(
      `${environment.apiUrl}/v2/user/watch/entity/${slug}`,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      }
    )
  }

  incrementCopyCount(slug, attachmentSlug) {
    return this._http.post(
      `${environment.apiUrl}/entities/${slug}/attachments/${attachmentSlug}/copy`,
      null,
      {}
    )
  }

  entityToScriptModel(e: EntityModel): ScriptModel {
    return {
      ...e,
      type_name: "Script",
    }
  }

  private getEntityListRequest(
    endpoint,
    params: ParameterModel[] = null
  ): Observable<EntityModel[]> {
    return this._request.get<{ data: EntityModel[] }>(endpoint, params).pipe(
      map((response) => response.data),
      map((entities) =>
        entities.map((e) => Object.assign(new EntityModel(), e))
      )
    )
  }

  private getEntityItemRequest(
    endpoint = "",
    params: ParameterModel[] = null
  ): Observable<EntityModel> {
    return this._request.get<{ data: EntityModel }>(endpoint, params).pipe(
      map((response) => response.data),
      map((entity) => Object.assign(new EntityModel(), entity))
    )
  }

  private getSSREntityListRequest(
    stateKey,
    endpoint,
    params: ParameterModel[] = null
  ): Observable<EntityModel[]> {
    return this._request
      .getSSR<{ data: EntityModel[] }>(stateKey, endpoint, params)
      .pipe(
        map((response) => response.data),
        map((entities) =>
          entities.map((e) => Object.assign(new EntityModel(), e))
        )
      )
  }

  private getSSREntityItemRequest(
    stateKey,
    endpoint = "",
    params: ParameterModel[] = null
  ): Observable<EntityModel> {
    return this._request
      .getSSR<{ data: EntityModel }>(stateKey, endpoint, params)
      .pipe(
        map((response) => response.data),
        map((entity) => Object.assign(new EntityModel(), entity))
      )
  }

  assignPartner(id: number, partnerID: number) {
    return this._http.post<EntityModel>(
      environment.apiUrl + "/entities/" + id + "/partners?use-id=true",
      { partnerID }
    )
  }

  unAssignPartner(id: number, partnerID: number) {
    return this._http.delete<EntityModel>(
      environment.apiUrl +
        "/entities/" +
        id +
        "/partners/" +
        partnerID +
        "?use-id=true"
    )
  }

  assignAuthor(id: number, authorID: number) {
    const formData = new FormData()

    return this._http.put<EntityModel>(
      environment.apiUrl + "/v2/entities/" + id + "/author/" + authorID,
      formData
    )
  }

  unAssignAuthor(id: number) {
    const formData = new FormData()

    return this._http.delete<EntityModel>(
      environment.apiUrl + "/v2/entities/" + id
    )
  }

  /* GraphCms methods */
  /**
   * This method returns a list of GraphCMS scripts using a list of strings with the scriptIds of the scripts we want to get.
   * The order in which the scriptIds are input is not taken into account when returning the result scripts.
   * @param graphCmsIds
   * @return List of scripts in json format
   */
  async getEntityListFromGraphCmsIds(
    graphCmsIds: string[]
  ): Promise<{ scripts: any[] }> {
    const getScriptListQuery = gql`
      {
        scripts(
          where: {scriptId_in: ${JSON.stringify(graphCmsIds)}}
        ) {
          id
          scriptId
          name
          description
          appVs {
            app {
              name
              shortcode
              prettyName
            }
            version
          }
          type
          createdDate
        }
      }
    `

    return await graphqlClient.request<{ scripts: any[] }>(getScriptListQuery)
  }

  async getEntityByScriptIdFromGraphCms(scriptId: string): Promise<any> {
    this.incrementViewCount(scriptId)
    const getScriptByScriptIdQuery = gql`{
      script(where: { scriptId: "${scriptId}"}) {
        scriptId
        name
        kind
        description
        versionsInfo {
          platform
          language
          statuses
          code
          codeURL
          requirements {
            product {
              shortcode
              name
              icon {
                url
              }
            }
            minMajorVersion
            minMinorVersion
            maxMajorVersion
            maxMinorVersion
          }
        }
        appVs {
          app {
            id
            shortcode
            name
            prettyName
            companyName
            companyURL
            productURL
            icon {
              url
            }
          }
          version
        }
        type
        feature {
          types
        }
        tags
        createdDate
      }
      collections(where: {scripts_some: {scriptId_in: "${scriptId}"}}) {
        name
        id
        description
        metaDescription
        slug
        scripts{
          appVs {
            app {
              name
              prettyName
            }
          }
        }
        originalUpdateDate
        originalCreationDate
        icon {
          url
        }
      }
    }`

    return graphqlClient.request<{ script: any[]; collections: any[] }>(
      getScriptByScriptIdQuery
    )
  }

  graphCmsEntityToScriptModel(entity): ScriptModel {
    // We use the plugin relativeTime to get the relative time. That is, to get results like 2 years ago.
    // For more information see https://day.js.org/docs/en/plugin/relative-time
    dayjs.extend(relativeTime)
    const appVersions: AppVersionsModel[] = []

    entity.appVs.forEach((data) => {
      const appInfo = data.app
      const version: AppVersionsModel = {
        company_name: appInfo.companyName,
        file: appInfo.icon.url,
        icon: appInfo.icon.url,
        id: appInfo.id,
        name: appInfo.name,
        pretty_name: appInfo.prettyName,
        product_url: appInfo.productURL,
        removeFile: false,
        shortcode: appInfo.shortcode,
        url: appInfo.companyURL,
        version_from: data.version,
        version_to: null,
      }
      appVersions.push(version)
    })

    const platformList: PlatformModel[] = []
    const attachmentType: AttachmentTypesModel = {
      server: [],
      cloud: [],
      dataCenter: [],
    }

    entity.versionsInfo.forEach((version, index) => {
      const requirementsList: RequirementModel[] = []
      platformList.push(version.platform)
      version.requirements.forEach((req) => {
        const newReq: RequirementModel = {
          product_icon: req.product.icon.url,
          product_name: req.product.name,
          version_from:
            (req.minMajorVersion && req.minMinorVersion) != null
              ? `${req.minMajorVersion}.${req.minMinorVersion}`
              : null,
          version_to:
            (req.maxMajorVersion && req.maxMinorVersion) != null
              ? `${req.maxMajorVersion}.${req.maxMinorVersion}`
              : null,
        }

        requirementsList.push(newReq)
      })

      attachmentType[version.platform] = [
        {
          id: index,
          data: version.code,
          styledData: "",
          platform_id: index,
          platform_name: version.platform,
          requirements: requirementsList,
          language: version.language,
          slug: entity.scriptId,
          updated_at: entity.lastUpdatedDate,
          entity_slug: entity.scriptId,
          verified: version.statuses === "verified",
        },
      ]
    })

    return {
      downvotes: entity.downvotes,
      app_versions: appVersions,
      attachment_updated_at: "",
      attachments: attachmentType,
      category_name: "",
      class: "ScriptModel",
      created_at: dayjs(entity.createdDate).fromNow(),
      description: markdown.toHTML(entity.description),
      feature_names: entity.feature.types,
      has_upvoted_entity: entity.has_upvoted_entity,
      has_downvoted_entity: entity.has_downvoted_entity,
      id: entity.scriptId,
      kind_name: entity.kind,
      learn_more_text: "",
      learn_more_url: "",
      name: entity.name,
      owner: "",
      partners: [],
      platforms: platformList,
      products: [],
      public: entity.kind === "free",
      slug: entity.scriptId,
      star_count: 0,
      subscribed: false,
      subscribed_at: undefined,
      tag_names: entity.tags,
      type_name: entity.type,
      updated_at: dayjs(entity.lastUpdatedDate).fromNow(),
      upvoted: false,
      upvotes: entity.upvotes,
    }
  }

  async getCopyCountByScriptIdFromGraphCms(scriptId): Promise<{ script }> {
    const getCopyCountByScriptIdQuery = gql`
    {
      script(where: {scriptId: "${scriptId}"}) {
        counter {
          id
          copyCount
        }
      }
    }`

    return await graphqlClient.request<{ script }>(getCopyCountByScriptIdQuery)
  }

  async getViewCountByScriptIdFromGraphCms(scriptId): Promise<{ script }> {
    const getCopyCountByScriptIdQuery = gql`
    {
      script(where: {scriptId: "${scriptId}"}) {
        counter {
          id
          viewCount
        }
      }
    }`

    return await graphqlClient.request<{ script }>(getCopyCountByScriptIdQuery)
  }

  async incrementCopyCountForGraphCms(counterId, newCopyCountValue) {
    const incrementCopyCountMutation = gql`mutation updateCopyCounter
    {
      __typename
      updateCounter(data: {copyCount: ${newCopyCountValue}}, where: {id: "${counterId}"}) {
        id
        copyCount
      }
    }`

    return graphqlClient.request<{ updateCounter }>(incrementCopyCountMutation)
  }

  async incrementViewCountForGraphCms(counterId, newViewCountValue) {
    const incrementViewCountMutation = gql`mutation updateViewCounter
    {
      __typename
      updateCounter(data: {viewCount: ${newViewCountValue}}, where: {id: "${counterId}"}) {
        id
        viewCount
      }
    }`

    return graphqlClient.request<{ updateCounter }>(incrementViewCountMutation)
  }

  publishCounterByIdForGraphCms(counterId: string) {
    const publishCounterMutation = gql`mutation MyMutation
    {
      __typename
      publishCounter(where: {id: "${counterId}"}) {
        copyCount
        viewCount
        id
      }
    }`

    graphqlClient.request<{ publishCounter }>(publishCounterMutation)
  }

  incrementViewCount(scriptId: string) {
    this.getViewCountByScriptIdFromGraphCms(scriptId).then((response) =>
      this.incrementViewCountForGraphCms(
        response.script.counter.id,
        response.script.counter.viewCount + 1
      ).then((response) =>
        this.publishCounterByIdForGraphCms(response.updateCounter.id)
      )
    )
  }

  upvote(scriptIdGraphCms: string) {
    return this._http.put<EntityModel>(
      environment.apiUrl + "/users/entities/upvote",
      { scriptIdGraphCms }
    )
  }

  downvote(scriptIdGraphCms: string) {
    return this._http.put<EntityModel>(
      environment.apiUrl + "/users/entities/downvote",
      { scriptIdGraphCms }
    )
  }

  getEntityByGraphCmsIdFromApi(scriptIdGraphCms: string) {
    return this._http.get<any>(
      environment.apiUrl + "/v2/entity/" + scriptIdGraphCms
    )
  }

  async getRelatedEntitiesFromGraphCms(
    scriptId,
    appVs,
    versionsInfo,
    limit: number
  ): Promise<any[]> {
    const platforms = versionsInfo.map((version) => version.platform)
    const appVersions = JSON.stringify(
      appVs.map((appVersion) => appVersion.app.shortcode)
    )
    const body = `
        scriptId
        name
        kind
        description
        counter {
          copyCount
        }
        versionsInfo {
          platform
          language
          statuses
          code
          codeURL
          requirements {
            product {
              shortcode
              name
              icon {
                url
              }
            }
            minMajorVersion
            minMinorVersion
            maxMajorVersion
            maxMinorVersion
          }
        }
        appVs {
          app {
            id
            shortcode
            name
            prettyName
            companyName
            companyURL
            productURL
            icon {
              url
            }
          }
          version
        }
        type
        feature {
          types
        }
        tags
        createdDate
      `
    let dataCenterWhereQuery
    let cloudWhereQuery

    if (platforms.includes("dataCenter")) {
      const dataCenterRequirement = _.compact(
        versionsInfo.map((version) => {
          if (version.platform === "dataCenter") return version.requirements[0]
        })
      )[0]
      dataCenterWhereQuery = `dataCenterScripts: scripts(
      where: {scriptId_not: "${scriptId}", appVs_every: {app: {shortcode_in: ${appVersions}}}, versionsInfo_some: {platform: dataCenter, requirements_every:
       {
        minMajorVersion: ${dataCenterRequirement.minMajorVersion},
        minMinorVersion: ${dataCenterRequirement.minMinorVersion},
        product: {
          shortcode: "${dataCenterRequirement.product.shortcode}"
        }
       }
      }}
      ) {
        ${body}
      }`
    }

    if (platforms.includes("cloud")) {
      const cloudRequirement = _.compact(
        versionsInfo.map((version) => {
          if (version.platform === "cloud") return version.requirements[0]
        })
      )[0]
      cloudWhereQuery = `cloudScripts: scripts(
          where: {scriptId_not: "${scriptId}", appVs_every: {app: {shortcode_in: ${appVersions}}}, versionsInfo_some: {platform: cloud, requirements_every: {product: {shortcode: "${cloudRequirement.product.shortcode}"}}}}){${body}}`
    }

    const getRelatedEntitiesQuery = gql`
      {
        ${dataCenterWhereQuery ? dataCenterWhereQuery : ""}
        ${cloudWhereQuery ? cloudWhereQuery : ""}
      }
    `
    const queryResult: { dataCenterScripts: any; cloudScripts: any } =
      await graphqlClient.request<{
        dataCenterScripts: any
        cloudScripts: any
      }>(getRelatedEntitiesQuery)

    const scripts = _.union(
      queryResult.dataCenterScripts,
      queryResult.cloudScripts
    )

    let result = []
    let numbers = []

    if (scripts.length < limit) {
      return scripts
    }

    while (numbers.length != limit) {
      let position = _.random(0, scripts.length - 1)
      if (!numbers.includes(position)) {
        numbers.push(position)
      }
    }
    numbers.forEach((number) => {
      result.push(scripts[number])
    })

    return result
  }

  /**
   * This method calculates the most current last update date among the last update dates of the GraphCMS system for the
   * script and its sub-elements. This date will be displayed as the last update date of the script in the front end.
   * @param scriptId - Id of the script in GraphCMS from which you want to get the last update date
   * @return Promise<{ string }> - date of last update for the script
   *
   * @example:
   * "script": {
      "updatedAt": "2021-10-25T06:52:00.907601+00:00",
      "appVs": [
        {
          "updatedAt": "2021-10-08T08:16:13.793757+00:00"
        }
      ],
      "feature": {
        "updatedAt": "2021-10-08T08:16:13.793757+00:00"
      },
      "versionsInfo": [
        {
          "updatedAt": "2021-10-21T08:26:11.420415+00:00",
          "requirements": [
            {
              "updatedAt": "2021-10-08T08:16:13.793757+00:00"
            }
          ]
        },
        {
          "updatedAt": "2021-10-08T08:16:13.793757+00:00",
          "requirements": [
            {
              "updatedAt": "2021-10-08T08:16:13.793757+00:00"
            }
          ]
        }
      ]
    }
   *
   * @Result: "2021-10-25T06:52:00.907601+00:00"
   */
  async getLastUpdateDateOfScriptAndSubElementsByScriptIdFromGraphCms(
    scriptId
  ): Promise<{ string }> {
    const getUpdateDateOfScriptAndSubElementsByScriptIdQuery = gql`{
    script (where: {scriptId: "${scriptId}"}) {
      updatedAt
      appVs {
        updatedAt
      }
      feature {
        updatedAt
      }
      versionsInfo {
        updatedAt
        requirements {
          updatedAt
        }
      }
    }
  }`

    return await graphqlClient
      .request<{ script }>(getUpdateDateOfScriptAndSubElementsByScriptIdQuery)
      .then((result) => {
        return _.max(
          _.map(_.values(flatten(result.script)), function applyDayJs(x) {
            return dayjs(x)
          })
        ).toISOString()
      })
  }
}
