import { Inject, Injectable, PLATFORM_ID } from "@angular/core"
import { HttpClient } from "@angular/common/http"
import { throwError } from "rxjs/internal/observable/throwError"
import { environment } from "../../../environments/environment"

import { UserMetaModel } from "../../models/user-meta.model"
import { ProfileModel } from "../../models/profile.model"
import { isPlatformServer } from "@angular/common"

@Injectable({
  providedIn: "root",
})
export class ProfileService {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private http: HttpClient
  ) {}

  private _profile: ProfileModel

  get profile() {
    return this._profile
  }

  set profile(profile) {
    this._profile = {
      sub: profile.sub,
      roles: profile["https://adaptavist:eu:auth0:com/roles"],
      firstName: profile["https://adaptavist:eu:auth0:com/first_name"],
      lastName: profile["https://adaptavist:eu:auth0:com/last_name"],
      email: profile.email,
      nickname: profile.nickname,
      email_verified: profile.email_verified,
      avatar: profile["https://adaptavist:eu:auth0:com/avatar"],
      small_avatar: profile["https://adaptavist:eu:auth0:com/small_avatar"],
      predefined_avatar:
        profile["https://adaptavist:eu:auth0:com/predefined_avatar"],
      dismissed_modals:
        profile["https://adaptavist:eu:auth0:com/dismissed_modals"],
    }
  }

  get email() {
    return this._profile ? this._profile.email : null
  }

  private _admin = null

  get admin(): boolean {
    if (this._admin === null) {
      this._admin = this.hasRole("Library Admin")
    }

    return this._admin
  }

  private _accessToken = null

  get accessToken() {
    return this._accessToken
  }

  set accessToken(accessToken) {
    this._accessToken = accessToken
  }

  public isAuthenticated(): boolean {
    // If the application is running on the server, a User may never be authenticated
    if (isPlatformServer(this.platformId)) {
      return false
    }

    const expiresAt = JSON.parse(localStorage.getItem("expires_at") || "{}")
    return new Date().getTime() < expiresAt
  }

  public isVerified(): boolean {
    return typeof this._profile !== "undefined" && this._profile.email_verified
  }

  public isAuthenticatedAndVerified(): boolean {
    return this.isAuthenticated() && this.isVerified()
  }

  public isAdmin(): boolean {
    return this.admin
  }

  public getUserMeta() {
    if (!this.isAuthenticatedAndVerified()) {
      return throwError(new Error("Unauthenticated"))
    }

    return this.http.get<UserMetaModel>(environment.apiUrl + "/users/meta")
  }

  public getName() {
    return `${this._profile.firstName} ${this._profile.lastName}`
  }

  public getAvatarSmallThumbURL() {
    if (this._profile.small_avatar) {
      return this._profile.small_avatar
    } else {
      return "https://library.adaptavist.com/assets/images/avatars/chameleon.png"
    }
  }

  public getAvatarThumbURL() {
    return this._profile.avatar
  }

  public hasPredefinedAvatar(): boolean {
    return this._profile.predefined_avatar === "false"
  }

  public getDismissedModals() {
    if (this._profile && this._profile.dismissed_modals) {
      return this._profile.dismissed_modals
    }

    return ""
  }

  private hasRole(role): boolean {
    if (this._profile) {
      const roles = this._profile.roles || []
      return roles.includes(role)
    }

    return false
  }
}
