import { Injectable } from "@angular/core"
import { Observable } from "rxjs"
import { map } from "rxjs/operators"
import { HttpClient, HttpHeaders } from "@angular/common/http"
import { environment } from "../../../environments/environment"
import { CollectionModel } from "../../models/collection.model"
import { RequestService } from "../request/request.service"
import { slugify } from "../../utilities/slugify"
import { LinkModel } from "../../models/link.model"
import { ParameterModel } from "../../models/parameter.model"
import { makeStateKey } from "@angular/platform-browser"
import { gql, GraphQLClient } from "graphql-request"
import {
  getAppVersion,
  getPartners,
  makeGraphCmsLinkData,
} from "../../transformers/category-transformers"
import dayjs from "dayjs"
import { markdown } from "markdown"

const headers = { authorization: `Bearer ${environment.graphcmsToken}` }
const graphqlClient = new GraphQLClient(environment.graphcmsEndpoint, {
  headers,
})

@Injectable({
  providedIn: "root",
})
export class CollectionService {
  constructor(private _request: RequestService, private _http: HttpClient) {}

  getCollectionItem(slug: string): Observable<CollectionModel> {
    return this.getSSRCollectionItemRequest(
      makeStateKey(`collection_item_${slug}`),
      `/v2/collection/${slug}`,
      [
        { key: "with", value: "entities,partners" },
        { key: "merge", value: "more_by_owner,app_versions" },
      ]
    )
  }

  getCollectionList(): Observable<CollectionModel[]> {
    return this.getCollectionListRequest("/v2/collection")
  }

  getCollectionListWithEntitiesAndPartners(): Observable<CollectionModel[]> {
    return this.getCollectionListRequest("/v2/collection", [
      { key: "with", value: "entities,partners" },
      { key: "order", value: "name" },
      { key: "direction", value: "asc" },
      { key: "limit", value: "100" },
    ])
  }

  getMyCollectionList(): Observable<CollectionModel[]> {
    return this.getCollectionListRequest("/v2/user/collection", [
      { key: "with", value: "entities" },
      { key: "merge", value: "products" },
    ])
  }

  getCollectionsBy(owner: string): Observable<CollectionModel[]> {
    return this.getCollectionListRequest(`/v2/collection/user/${owner}`, [
      { key: "limit", value: "4" },
    ])
  }

  getNewestAdaptavistCollections(
    limit: number = 5
  ): Observable<CollectionModel[]> {
    return this.getSSRCollectionListRequest(
      makeStateKey(`newest_adaptavist_collections_${limit}`),
      "/v2/collection",
      [
        { key: "order", value: "created_at" },
        { key: "scope", value: "adaptavist" },
        { key: "merge", value: "products,app_versions" },
        { key: "limit", value: limit.toString() },
      ]
    )
  }

  getTrendingPartnerCollections(
    limit: number = 5
  ): Observable<CollectionModel[]> {
    return this.getSSRCollectionListRequest(
      makeStateKey(`trending_partner_collections_${limit}`),
      "/v2/collection",
      [
        { key: "order", value: "updated_at" },
        { key: "scope", value: "partner" },
        { key: "merge", value: "products,app_versions" },
        { key: "limit", value: limit.toString() },
      ]
    )
  }

  getNewestAdaptavistCollectionsWithEntities(
    limit: number = 5
  ): Observable<CollectionModel[]> {
    return this.getSSRCollectionListRequest(
      makeStateKey(`newest_adaptavist_collections_with_entities_${limit}`),
      "/v2/collection",
      [
        { key: "order", value: "created_at" },
        { key: "scope", value: "adaptavist" },
        { key: "merge", value: "products,app_versions" },
        { key: "limit", value: limit.toString() },
        { key: "with", value: "entities" },
      ]
    )
  }

  getTrendingPartnerCollectionsWithEntities(
    limit: number = 5
  ): Observable<CollectionModel[]> {
    return this.getSSRCollectionListRequest(
      makeStateKey(`trending_partner_collections_with_entities_${limit}`),
      "/v2/collection",
      [
        { key: "order", value: "created_at" },
        { key: "scope", value: "partner" },
        { key: "merge", value: "products,app_versions" },
        { key: "limit", value: limit.toString() },
        { key: "with", value: "entities" },
      ]
    )
  }

  createCollection(data): Observable<CollectionModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("slug", data.slug)
    formData.append("description", data.description)
    formData.append("meta_description", data.meta_description)
    formData.append("removeFile", data.removeFile)

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    return this._http.post<CollectionModel>(
      environment.apiUrl + "/collections",
      formData
    )
  }

  updateCollection(id: number, data): Observable<CollectionModel> {
    const formData = new FormData()

    formData.append("name", data.name)
    formData.append("slug", data.slug)
    formData.append("description", data.description)
    formData.append("meta_description", data.meta_description)
    formData.append("removeFile", data.removeFile)
    formData.append("shouldUpdateTimestamps", "false")

    // Lumen doesn't handle http.put request when sending up formData. Send the data up as http.post and set the _method to PUT
    formData.append("_method", "PUT")

    if (data.file && data.file.name) {
      formData.append("file", data.file, data.file.name)
    }

    return this._http.post<CollectionModel>(
      environment.apiUrl + "/collections/" + id + "?use-id=true",
      formData
    )
  }

  deleteCollection(id: number): Observable<any> {
    return this._http.delete(
      environment.apiUrl + "/collections/" + id + "?use-id=true"
    )
  }

  assignEntity(id: number, entityID: number) {
    return this._http.post<CollectionModel>(
      environment.apiUrl + "/collections/" + id + "/entities?use-id=true",
      { entityID }
    )
  }

  unAssignEntity(id: number, entityID: number) {
    return this._http.delete(
      environment.apiUrl +
        "/collections/" +
        id +
        "/entities/" +
        entityID +
        "?use-id=true"
    )
  }

  assignPartner(id: number, partnerID: number) {
    return this._http.post<CollectionModel>(
      environment.apiUrl + "/collections/" + id + "/partners?use-id=true",
      { partnerID }
    )
  }

  unAssignPartner(id: number, partnerID: number) {
    return this._http.delete<CollectionModel>(
      environment.apiUrl +
        "/collections/" +
        id +
        "/partners/" +
        partnerID +
        "?use-id=true"
    )
  }

  postAddToMyLibrary(slug) {
    return this._http.post(
      `${environment.apiUrl}/v2/user/collection/${slug}`,
      null,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      }
    )
  }

  deleteAddToMyLibrary(slug) {
    return this._http.delete(
      `${environment.apiUrl}/v2/user/collection/${slug}`,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      }
    )
  }

  extractCategories(collection: CollectionModel): LinkModel[] {
    return ["app_versions", "products"].reduce(
      (arr, category) => [
        ...arr,
        ...collection[category].map((ec) => this.makeLinkData(category, ec)),
      ],
      []
    )
  }

  private makeLinkData(category, entityCategory) {
    return {
      link: "/search",
      text:
        entityCategory.name.charAt(0).toUpperCase() +
        entityCategory.name.slice(1),
      params: { [category]: slugify(entityCategory.name) },
    }
  }

  private getCollectionListRequest(
    endpoint,
    params: ParameterModel[] = null
  ): Observable<CollectionModel[]> {
    return this._request
      .get<{ data: CollectionModel[] }>(endpoint, params)
      .pipe(
        map((response) => response.data),
        map((collections) =>
          collections.map((c) => Object.assign(new CollectionModel(), c))
        )
      )
  }

  private getCollectionItemRequest(
    endpoint = "",
    params: ParameterModel[] = null
  ): Observable<CollectionModel> {
    return this._request.get<{ data: CollectionModel }>(endpoint, params).pipe(
      map((response) => response.data),
      map((collection) => Object.assign(new CollectionModel(), collection))
    )
  }

  private getSSRCollectionListRequest(
    stateKey: string,
    endpoint,
    params: ParameterModel[] = null
  ): Observable<CollectionModel[]> {
    return this._request
      .getSSR<{ data: CollectionModel[] }>(stateKey, endpoint, params)
      .pipe(
        map((response) => response.data),
        map((collections) =>
          collections.map((c) => Object.assign(new CollectionModel(), c))
        )
      )
  }

  private getSSRCollectionItemRequest(
    stateKey: string,
    endpoint = "",
    params: ParameterModel[] = null
  ): Observable<CollectionModel> {
    return this._request
      .getSSR<{ data: CollectionModel }>(stateKey, endpoint, params)
      .pipe(
        map((response) => response.data),
        map((collection) => Object.assign(new CollectionModel(), collection))
      )
  }

  /* GraphCms methods*/
  async getNewestAdaptavistCollectionsFromGraphCms(
    limit: number
  ): Promise<any> {
    const getNewestAdaptavistCollectionsQuery = gql`{
      collections(orderBy: originalCreationDate_DESC, first: ${limit}, where: {partners_none: {}}) {
        name
        id
        description
        metaDescription
        slug
        originalUpdateDate
        originalCreationDate
        partners {
          partnerName
          partnerProductName
          partnerProductURL
          partnerURL
          partnerLogo {
            url
          }
        }
        scripts {
          id
          scriptId
          name
          description
          appVs {
            app {
              id
              shortcode
              name
              prettyName
              companyName
              companyURL
              productURL
              companyURL
              icon {
                url
              }
            }
          }
        }
        icon {
          url
        }
      }
    }`

    return graphqlClient.request<{ collections: any[] }>(
      getNewestAdaptavistCollectionsQuery
    )
  }

  async getTrendingPartnerCollectionsFromGraphCms(
    limit: number = 5
  ): Promise<any> {
    const getTrendingPartnerCollectionsQuery = gql`{
      collections(orderBy: originalUpdateDate_DESC, first: ${limit}, where: {partners_some: {}}) {
        name
        id
        description
        metaDescription
        slug
        originalUpdateDate
        originalCreationDate
        partners {
          partnerName
          partnerProductName
          partnerProductURL
          partnerURL
          partnerLogo {
            url
          }
        }
        scripts {
          id
          scriptId
          name
          description
          appVs {
            app {
              id
              shortcode
              name
              prettyName
              companyName
              companyURL
              productURL
              companyURL
              icon {
                url
              }
            }
          }
        }
        icon {
          url
        }
      }
    }`

    return graphqlClient.request<{ collections: any[] }>(
      getTrendingPartnerCollectionsQuery
    )
  }

  async getCollectionItemFromGraphCms(slug: string): Promise<any> {
    const getCollectionItemQuery = gql`{
      collection(where: {slug: "${slug}"}) {
        name
        id
        description
        metaDescription
        slug
        originalUpdateDate
        originalCreationDate
        partners {
          partnerName
          partnerProductName
          partnerProductURL
          partnerURL
          partnerLogo {
            url
          }
        }
        icon {
          url
        }
        scripts {
          id
          scriptId
          name
          description
          appVs {
            app {
              id
              shortcode
              name
              prettyName
              companyName
              companyURL
              productURL
              companyURL
              icon {
                url
              }
            }
            version
          }
          type
          createdDate
        }
      }
      more_by_owner: collections(where: {slug_not: "${slug}", AND: {partners_none: {}}}) {
        name
        id
        description
        metaDescription
        slug
        originalUpdateDate
        originalCreationDate
        partners {
          partnerName
          partnerProductName
          partnerProductURL
          partnerURL
          partnerLogo {
            url
          }
        }
        icon {
          url
        }
        scripts {
          id
          scriptId
          name
          description
          appVs {
            app {
              id
              shortcode
              name
              prettyName
              companyName
              companyURL
              productURL
              companyURL
              icon {
                url
              }
            }
            version
          }
          type
          createdDate
        }
      }
    }`

    return graphqlClient.request<{ collection: any; more_by_owner: any[] }>(
      getCollectionItemQuery
    )
  }

  async getCollectionListFromGraphCmsSlugs(
    collectionSlugs: string[]
  ): Promise<{ collections: any[] }> {
    const getCollectionListQuery = gql`{
      collections(
      where: {slug_in: ${JSON.stringify(collectionSlugs)}}
      ) {
        name
        id
        description
        metaDescription
        slug
        originalUpdateDate
        originalCreationDate
        partners {
          partnerName
          partnerProductName
          partnerProductURL
          partnerURL
          partnerLogo {
            url
          }
        }
        scripts {
          id
          scriptId
          name
          description
          type
          appVs {
            app {
              id
              shortcode
              name
              prettyName
              companyName
              companyURL
              productURL
              companyURL
              icon {
                url
              }
            }
          }
        }
        icon {
          url
        }
      }
    }`
    return await graphqlClient.request<{ collections: any[] }>(
      getCollectionListQuery
    )
  }

  graphCmsCollectionToCollectionModel(collection): CollectionModel {
    return {
      app_versions:
        collection.scripts.length > 0
          ? getAppVersion(collection.scripts[0].appVs)
          : [],
      class: "CollectionModel",
      created_at: dayjs(collection.originalCreationDate).fromNow(),
      description: markdown.toHTML(collection.description),
      entities: [],
      id: collection.id,
      image: {
        src: collection.icon.url,
        alt: collection.icon.url,
      },
      removeFile: false,
      more_by_owner: [],
      name: collection.name,
      categories:
        collection.scripts.length !== 0
          ? makeGraphCmsLinkData(collection.scripts[0].appVs)
          : null,
      numberOfEntities: 0,
      partner: false,
      partners: getPartners(collection.partners),
      slug: collection.slug,
      updated_at: dayjs(collection.originalUpdateDate).fromNow(),
      creator: "Adaptavist",
      type: "collection",
    }
  }
}
