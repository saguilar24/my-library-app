import { Component, Inject, forwardRef, Input } from "@angular/core"
import {
  BaseWidget,
  NgAisIndex,
  NgAisInstantSearch,
} from "angular-instantsearch"
import { connectRefinementList } from "instantsearch.js/es/connectors"

@Component({
  selector: "app-refinement-list",
  template: `
    <ul class="l-padding--none l-margin-left--none">
      <label *ngFor="let item of state.items">
        <li class="u-link-list">
          <button
            class="c-filter-menu--link"
            (click)="state.refine(item.value)"
          >
            {{ item.label }} ({{ item.count }})
          </button>
        </li>
      </label>
    </ul>
  `,
})
export class RefinementList extends BaseWidget {
  instantSearchInstance: NgAisInstantSearch
  parentIndex?: NgAisIndex
  @Input("attribute") attribute = ""
  public state: {
    items: object[]
    refine: Function
    createURL: Function
    isFromSearch: boolean
    searchForItems: Function
    isShowingMore: boolean
    canToggleShowMore: boolean
    toggleShowMore: Function
    widgetParams: object
  }
  constructor(
    @Inject(forwardRef(() => NgAisInstantSearch))
    public instantSearchParent
  ) {
    super("RefinementList")
  }

  ngOnInit() {
    this.createWidget(connectRefinementList, {
      // instance options
      attribute: this.attribute,
    })
    super.ngOnInit()
  }
}
