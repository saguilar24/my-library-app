import { Injectable } from "@angular/core"
import { HttpClient } from "@angular/common/http"
import { Observable } from "rxjs"
import { environment } from "../../../environments/environment"
import { AttachmentModel } from "../../models/attachment.model"

@Injectable({
  providedIn: "root",
})
export class AttachmentService {
  constructor(private http: HttpClient) {}

  getAttachment(
    entitySlug: string,
    attachmentSlug: string
  ): Observable<AttachmentModel> {
    return this.http.get<AttachmentModel>(
      environment.apiUrl +
        "/entities/" +
        entitySlug +
        "/attachments/" +
        attachmentSlug
    )
  }
}
