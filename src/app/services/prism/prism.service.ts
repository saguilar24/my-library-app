import { Injectable } from "@angular/core"

import "prismjs"
import "prismjs/components/prism-javascript"
import "prismjs/components/prism-java"
import "prismjs/components/prism-groovy"
import { ApplicationService } from "../application/application.service"

declare var Prism: any

@Injectable()
export class PrismService {
  constructor(private appService: ApplicationService) {}

  highlightAll() {
    if (this.appService.isRunningInClientBrowser()) {
      Prism.highlightAll()
    }
  }

  highlight(code: string, language = "groovy") {
    if (this.appService.isRunningInClientBrowser()) {
      return Prism.highlight(code, Prism.languages.groovy)
    } else {
      return code
    }
  }
}
