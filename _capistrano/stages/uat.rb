set :deploy_to, '/var/www/library-uat.brewdigital.uk'
set :branch, 'master'

set :ssh_options, {
    user: "dex",
    keys: "~/.ssh/dex-staging.brewdigital.uk_do",
    forward_agent: true
}

set :npm_target_path, -> { release_path }
set :npm_flags, ''
set :npm_env_variables, { 'NG_CLI_ANALYTICS' => 'false' }

server '46.101.23.218', roles: [:app], :primary => true

# ---------------------------------------------------------------------------------------------------------------------
# Deploy tasks
# ---------------------------------------------------------------------------------------------------------------------

namespace :angular do

    desc 'Create/move specific configuration files based on deployment stage'
    task :configure_stage do
        on roles(:app) do
            info "Move env file"
            execute "mv -f #{release_path}/_env-files/uat/.env.uat #{release_path}/.env"

            info 'Remove environment files directory afterwards'
            execute "rm -rf #{release_path}/_env-files"
        end
    end

    desc 'Build distribution files'
    task :build_dist do
        on roles(:app) do
            within release_path do
                info 'Building application distribution files'
                 execute *%w[ npm run build:ssr ]
            end
        end
    end

    desc 'Reload application'
    task :boot_app do
        on roles(:app) do
            within current_path do
                info 'Kill pm2'
                execute *%w[ pm2 kill ]
                info 'Prevent pm2 hanging...'
                execute *%w[ sleep 5 ]
                info 'Start application'
                execute *%w[ pm2 start ecosystem.config.js --env uat ]
                info 'Persist application'
                execute *%w[ pm2 cleardump ]
                execute *%w[ pm2 save ]
            end
        end
    end

    task :deploy_application do
        before :updating,  'angular:configure_stage'
        after :updated,   'angular:build_dist'
        after :published, 'angular:boot_app'
        after :finishing, 'deploy:cleanup'
    end
end

namespace :deploy do
    SSHKit.config.command_map[:npm] = '/home/dex/.nvm/versions/node/v16.10.0/bin/npm'
    invoke 'angular:deploy_application'
end
