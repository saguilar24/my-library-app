set :deploy_to, '/opt/www_adaptavist_library'
set :branch, 'master'

set :ssh_options, {
    user: 'brew',
    password: 'eTW%epzHeN;m>u#RCv2U',
    forward_agent: true
}

set :npm_target_path, -> { release_path }
set :npm_flags, '--silent --no-progress'

server 'library.adaptavist.com', roles: [:app], :primary => true

# ---------------------------------------------------------------------------------------------------------------------
# Deploy tasks
# ---------------------------------------------------------------------------------------------------------------------

namespace :angular do

    desc 'Create/move specific configuration files based on deployment stage'
    task :configure_stage do
        on roles(:app) do
            info "Move env file"
            execute "mv -f #{release_path}/_env-files/production/.env.production #{release_path}/.env"

            info 'Remove environment files directory afterwards'
            execute "rm -rf #{release_path}/_env-files"
        end
    end

    desc 'Build distribution files'
    task :build_dist do
        on roles(:app) do
            within release_path do
                info 'Building application distribution files'
                execute *%w[ npm run build:ssr:production ]
            end
        end
    end

    desc 'Reload application as `hosting` user'
    task :boot_app do
        on roles(:app) do
            within current_path do
                info 'Kill pm2'
                execute! :sudo, *%w[ -u hosting pm2 kill ]
                info 'Prevent pm2 hanging...'
                execute *%w[ sleep 5 ]
                info 'Start application'
                execute! :sudo, *%w[ -u hosting pm2 start ecosystem.config.js ]
                info 'Persist application'
                execute! :sudo, *%w[ -u hosting pm2 cleardump ]
                execute! :sudo, *%w[ -u hosting pm2 save ]
            end
        end
    end

    task :deploy_application do
        before :updating, 'angular:configure_stage'
        after :updated,   'angular:build_dist'
        after :published, 'angular:boot_app'
        after :finishing, 'deploy:cleanup'
    end
end

namespace :deploy do
    invoke 'angular:deploy_application'
end
