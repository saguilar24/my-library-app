# config valid only for specific version of Capistrano
lock '3.11.0'

set :application, 'library.adaptavist.com'
set :repo_url, 'git@bitbucket.org:saguilar24/my-library-app.git'

# Only pull commits remote repo instead of clone
set :deploy_via, :remote_cache

# Make pseudo-terminal for interactive terminal
set :pty, true

# Set Slack Webhook
set :slackistrano, {
  channel: '#adapt-library-dev',
  webhook: 'https://hooks.slack.com/services/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'
}

# Having manually entered the password once, persist it for the remainder of the session
class SSHKit::Sudo::InteractionHandler
  use_same_password!
end
